/**MERGE VS SWITCH */

import { fromEvent, interval } from "rxjs";
import { mergeMap, switchMap } from "rxjs/operators";

const click$ = fromEvent(document, "click");
const interval$ = interval(1000);

click$
  .pipe(
    // De esta manera por cada click emitido
    // se crea una nueva linea del tiempo del interval
    // pero termina cuando se emite un nuevo click
    // Solo mantiene una subscripcion interna activa
    switchMap(() => interval$)
    // De esta manera por cada click emitido
    // se crea una nueva linea del tiempo del interval
    // que sigue emitiendo valores indefinidamente
    // En otras palabras aqui se mantienen
    // las subscripciones internas activas.
    //mergeMap(() => interval$) **
  )
  .subscribe(console.log);
