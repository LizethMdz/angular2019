/**MERGEALL */

/**Converts a higher-order Observable into
 * a first-order Observable which concurrently
 * delivers all
 * values that are emitted on the inner Observables. */

import { fromEvent, Observable } from "rxjs";
import { ajax } from "rxjs/ajax";
import { debounceTime, map, mergeAll, pluck } from "rxjs/operators";

//Interfaces
import { GithubUser } from "../interfaces/github-user.interface";
import { GithubUsersResp } from "../interfaces/github-users.interfaces";

// Referencias
const body = document.querySelector("body");
const textInput = document.createElement("input");
const orderList = document.createElement("ol");

body.append(textInput, orderList);

// Streams
const input$ = fromEvent<KeyboardEvent>(textInput, "keyup");

/**De esta manera encadenamos varios observables, y no es lo
 * más recomendable
 */

// input$
//   .pipe(
//     debounceTime<KeyboardEvent>(500),
//     map((event) => {
//       const texto = event.target["value"];

//       return ajax.getJSON(`https://api.github.com/search/users?q=${texto}`);
//     })
//   )
//   .subscribe((resp) => resp.pipe(pluck("url")).subscribe(console.log));

// Helpers
const mostrarUsuarios = (usuarios: GithubUser[]) => {
  console.log(usuarios);
  orderList.innerHTML = "";

  for (const usuario of usuarios) {
    const li = document.createElement("li");
    const img = document.createElement("img");
    img.src = usuario.avatar_url;

    const anchor = document.createElement("a");
    anchor.href = usuario.html_url;
    anchor.text = "Ver página";
    anchor.target = "_blank";

    li.append(img);
    li.append(usuario.login + " ");
    li.append(anchor);

    orderList.append(li);
  }
};

// Procesamiento
input$
  .pipe(
    debounceTime<KeyboardEvent>(500),
    pluck<KeyboardEvent, string>("target", "value"),
    map<string, Observable<GithubUsersResp>>((texto) =>
      ajax.getJSON(`https://api.github.com/search/users?q=${texto}`)
    ),
    mergeAll<GithubUsersResp>(),
    pluck<GithubUsersResp, GithubUser[]>("items")
  )
  .subscribe(
    //(resp) => console.log(resp[0].login)
    mostrarUsuarios
  );
