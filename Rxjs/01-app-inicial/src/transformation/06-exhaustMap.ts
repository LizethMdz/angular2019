/**EXHAUSTMAP */

/**
 * Projects each source value to an Observable
 * which is merged in the output Observable only
 * if the previous projected Observable has completed.
 */

import { interval, fromEvent } from "rxjs";
import { take, exhaustMap } from "rxjs/operators";

const interval$ = interval(500).pipe(take(3));
const click$ = fromEvent(document, "click");

// Aqui el primer valor emitido
// por el click crea un observable(intervalo)
// que emitirá valores hasta que finalice (permanece suscrito).
// Sin embargo, si se emite otro click antes
// de que el anterior(intervalo) termine. Lo ignnora.
// Si se vuelve a emitir otro click después,
// de que termine la anterior subscripcion.
// Si se vulve a generar otra subscripción. (Emitiendo valores
// hasta que termine).

click$.pipe(exhaustMap(() => interval$)).subscribe(console.log);
