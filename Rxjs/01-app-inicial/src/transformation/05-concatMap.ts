/**CONCATMAP */

/**
 * Projects each source value to an Observable
 * which is merged in the output Observable,
 * in a serialized fashion waiting
 * for each one to complete before merging the next
 */

import { interval, fromEvent } from "rxjs";
import { take, switchMap, concatMap } from "rxjs/operators";

const interval$ = interval(500).pipe(take(3));
const click$ = fromEvent(document, "click");

// utilidad en formularios con submnits

click$
  .pipe(
    //switchMap(() => interval$)
    // De esta manera, cada valor emitido por
    // el click cre una subscripcion,
    // sin embargo, espera hasta que el intervalo
    // de cada uno sea completado para concatenar
    // los valores.
    concatMap(() => interval$)
  )
  .subscribe(console.log);
