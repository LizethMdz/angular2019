/**CONCAT */

/**
 * Creates an output Observable which sequentially
 * emits all values from given Observable and then
 * moves on to the next.
 */

// Sample

// // results in:
// 0 -1000ms-> 1 -1000ms-> 2 -1000ms-> 3 -immediate-> 1 ... 10

import { take } from "rxjs/operators";
import { concat, interval } from "rxjs";

const interval$ = interval(1000);

concat(interval$.pipe(take(3)), interval$.pipe(take(2)), [
  1,
  2,
  3,
  4,
  5,
]).subscribe(console.log);
