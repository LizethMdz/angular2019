/**STARTWITH - ENDWITH */

//StartWuith
/**Returns an Observable that emits the
 * items you specify as arguments before it begins
 * to emit items emitted by the source Observable. */

// Sample
// results:
//   "first"
//   "second"
//   "from source"

//EndWith
/**Returns an Observable that emits the
 * items you specify as arguments after it finishes
 * emitting items emitted by the source Observable. */

//Sample
// result:
// 'hi'
// 'how are you?'
// 'sorry, I have to go now'
// 'goodbye!

import { of } from "rxjs";
import { endWith, startWith } from "rxjs/operators";

const numeros$ = of(1, 2, 3).pipe(
  startWith("a", "b", "c"),
  endWith("x", "y", "z")
);

numeros$.subscribe(console.log);
