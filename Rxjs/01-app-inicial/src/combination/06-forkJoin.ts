/**FORKJOIN */

/**
 * Accepts an Array of ObservableInput or a dictionary
 * Object of ObservableInput and returns an Observable
 * that emits either an array of values in the
 * exact same order as the passed array, or a dictionary
 * of values in the same shape as the passed dictionary.
 */

import { forkJoin, interval, of } from "rxjs";
import { delay, take } from "rxjs/operators";

const numeros$ = of(1, 2, 3, 4);
const intervalo$ = interval(1000).pipe(take(3)); //0..1..2
const letras$ = of("a", "b", "c").pipe(delay(3500));

forkJoin(numeros$, intervalo$, letras$).subscribe(console.log); //0:4, 1:2, 2:'c'

/**Como arreglo */
forkJoin(numeros$, intervalo$, letras$).subscribe((resp) => {
  console.log("numeros", resp[0]), console.log("intérvalo: ", resp[1]);
  console.log("letras: ", resp[2]);
}); //0:4, 1:2, 2:'c'

/**Con si fuera un objeto */
forkJoin({
  numeros$,
  intervalo$,
  letras$,
}).subscribe((resp) => {
  console.log(resp);
});

/**Con llaves diferente */
forkJoin({
  num: numeros$,
  int: intervalo$,
  let: letras$,
}).subscribe((resp) => {
  console.log(resp);
});
