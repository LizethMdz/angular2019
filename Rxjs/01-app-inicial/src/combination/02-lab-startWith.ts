import { of } from "rxjs";
import { ajax } from "rxjs/ajax";
import { endWith, startWith } from "rxjs/operators";

const loadingDiv = document.createElement("div");
loadingDiv.classList.add("loading");
loadingDiv.textContent = "Cargando...";

const body = document.querySelector("body");

//Stream
ajax
  .getJSON("https://reqres.in/api/users/2?delay=3")
  .pipe(startWith(true))
  .subscribe((resp) => {
    if (resp === true) {
      body.appendChild(loadingDiv);
    } else {
      document.querySelector(".loading").remove();
    }
    console.log(resp);
  });
