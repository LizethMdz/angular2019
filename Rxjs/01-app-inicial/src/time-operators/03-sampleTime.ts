/** SAMPLE - TIME */

/**Emits the most recently emitted
 * value from the source Observable within periodic time intervals. */

import { fromEvent } from "rxjs";
import { map, sampleTime } from "rxjs/operators";

const click$ = fromEvent<MouseEvent>(document, "click");

click$
  .pipe(
    sampleTime(2000),
    // Si lo colocamos abajo, lo que realmente procesa
    // Es la salida del map, y no tanto del evento
    map(({ x, y }) => ({ x, y }))
  )
  .subscribe(console.log);
