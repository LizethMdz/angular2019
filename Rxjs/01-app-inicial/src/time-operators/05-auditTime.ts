/** AUDITTIME */

/**Ignores source values for
 *  duration milliseconds, then emits the most
 * recent value from the source Observable, then repeats this process. */

import { fromEvent, interval } from "rxjs";
import { auditTime, map, sample, sampleTime, tap } from "rxjs/operators";

const click$ = fromEvent<MouseEvent>(document, "click");

/**
 * El valor emitido por el operador auditTime
 * ocurre una vez transcurrido el lapso de tiempo,
 * lo que emite es el ultimo valor(el evento click).
 *
 */

click$
  .pipe(
    tap(({ screenX, screenY }) => console.log("tap:", screenX, screenY)),
    auditTime(2000)
  )
  .subscribe(console.log);
