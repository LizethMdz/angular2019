import { asyncScheduler } from "rxjs";
/** THROTTLETIME */

/**Emits a value from the source Observable,
 * then ignores subsequent source values
 * for duration milliseconds, then repeats this process. */

import { fromEvent } from "rxjs";
import { distinctUntilChanged, pluck, throttleTime } from "rxjs/operators";

const click$ = fromEvent(document, "click");

/**Controlar los momentos de emision
 * Ya que se pudieran emitir muchos eventos
 * antes de que el valor sea mostrado por el Obsv
 */

click$.pipe(throttleTime(3000)).subscribe(console.log);

const input = document.createElement("input");

document.querySelector("body").appendChild(input);

const input$ = fromEvent(input, "keyup");

/**De esta manera el operador emite cada tecla siempre y cuando
 * haya pasado 1s.
 */
input$
  .pipe(
    // De esta manera nos aseguramos de recibir
    // los primeros y ultimos valores
    throttleTime(1000, asyncScheduler, {
      leading: true,
      trailing: true,
    }),
    pluck("target", "value"),
    distinctUntilChanged()
  )
  .subscribe(console.log);
