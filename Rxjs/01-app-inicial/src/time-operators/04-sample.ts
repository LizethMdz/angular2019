/** SAMPLE */

/**Emits the most recently
 * emitted value from the source Observable
 * whenever another Observable, the notifier, emits */

import { fromEvent, interval } from "rxjs";
import { map, sample, sampleTime } from "rxjs/operators";

const interval$ = interval(500);

const click$ = fromEvent<MouseEvent>(document, "click");

/**El intervalo sucede cada 5ms, pero
 * no es mostrado hasta que ocurra
 * el evento click, y asi sucesivamente
 */

interval$.pipe(sample(click$)).subscribe(console.log);
