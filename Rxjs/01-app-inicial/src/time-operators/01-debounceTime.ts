/** DEBOUNCETIME */

/**Emits a value from the source Observable only after
 * a particular time span has passed without
 * another source emission. */

import { fromEvent } from "rxjs";
import {
  debounce,
  debounceTime,
  distinctUntilChanged,
  map,
  pluck,
} from "rxjs/operators";

const click$ = fromEvent(document, "click");

/**Controla los momentos de emsion
 * Ya que se pudieran emitir muchos eventos
 * antes de que el valor sea mostrado por el Obsv
 */

click$.pipe(debounceTime(3000)).subscribe(console.log);

const input = document.createElement("input");

document.querySelector("body").appendChild(input);

const input$ = fromEvent(input, "keyup");

input$
  .pipe(debounceTime(1000), pluck("target", "value"), distinctUntilChanged())
  .subscribe(console.log);
