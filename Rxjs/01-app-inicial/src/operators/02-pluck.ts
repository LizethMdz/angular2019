/** Maps each source value (an object) to its specified nested property. */
import { fromEvent, pipe, range } from "rxjs";
import { map, pluck } from "rxjs/operators";

const keyUp$ = fromEvent<KeyboardEvent>(document, "keyup");

const keyUpCode$ = keyUp$.pipe(map(({ code }) => code));
// Si solo quiero una propiedad
//const keyUpPluck$ = keyUp$.pipe(pluck("key"));
// Si queremos una propiedad dentro de otro objeto  | ejem: target.baseURI
const keyUpPluck$ = keyUp$.pipe(pluck("target", "baseURI"));

keyUp$.subscribe(console.log);
keyUpCode$.subscribe((val) => console.log("map", val));
keyUpPluck$.subscribe((val) => console.log("pluck", val));
