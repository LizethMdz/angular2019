/**SCAN */

import { map } from "rxjs/operators";
import { reduce, scan } from "rxjs/operators";
import { from } from "rxjs";

const numbers = [1, 2, 3, 4, 5];

// const totalReducer = (acumulador: number, valorActual: number) => {
//   return acumulador + valorActual;
// };

const totalReducer = (acumulador: number, valorActual: number) =>
  acumulador + valorActual;

/// Reduce

from(numbers)
  .pipe(
    // Una unica emision
    reduce(totalReducer, 0)
  )
  .subscribe(console.log);

// Scann

from(numbers).pipe(scan(totalReducer, 0)).subscribe(console.log);

// Redux
interface Usuario {
  id?: string;
  autenticado?: boolean;
  token?: string;
  edad?: number;
}

const user: Usuario[] = [
  { id: "fher", autenticado: false, token: null },
  { id: "fher", autenticado: true, token: "ABC" },
  { id: "fher", autenticado: true, token: "ABC123" },
];

const state$ = from(user).pipe(
  scan<Usuario>(
    (acc, cur) => {
      return { ...acc, ...cur };
    },
    { edad: 33 }
  )
);

const id$ = state$.pipe(map((state) => state.id));

id$.subscribe(console.log);
