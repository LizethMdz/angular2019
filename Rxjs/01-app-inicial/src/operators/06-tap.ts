/** TAP */
/**Perform a side effect for every emission on the source Observable, but return an Observable that is identical to the source. */

import { range } from "rxjs";
import { tap, map } from "rxjs/operators";

const numeros$ = range(1, 5);
let arregloAntes: number[] = [];
numeros$
  .pipe(
    tap((x) => {
      console.log("antes:", x);
      arregloAntes.push(x);
      return true;
    }),
    map((val) => val * 10),
    tap((x) => {
      console.log("despues:", x);
    }),
    tap({
      next: (valor) => console.log("next-after", valor),
      complete: () => console.log("Se terminó todo"),
    })
  ) // Si se agrega un return dentro de un tap - no cambia el flujo de informacion
  .subscribe((val) => console.log("subs:", val));

console.log(arregloAntes);
