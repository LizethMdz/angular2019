/**Emits the given constant value on the output Observable every time the source Observable emits a value. */
import { fromEvent, pipe, range } from "rxjs";
import { map } from "rxjs/operators";

// range(1, 5)
//   .pipe(
//     // valor in, out
//     map<number, number>((value) => {
//       return value * 10;
//     }),
//   )
//   .subscribe(console.log);

const keyUp$ = fromEvent<KeyboardEvent>(document, "keyup").pipe(
  map(({ code }) => code)
);

keyUp$.subscribe((val) => console.log("map", val));
