/**Emits the given constant value on the output Observable every time the source Observable emits a value. */
/**Emitimos una misma salida para cada valor emitido por el Obs */

import { fromEvent, pipe, range } from "rxjs";
import { map, pluck, mapTo } from "rxjs/operators";

const keyUp$ = fromEvent<KeyboardEvent>(document, "keyup");

const keyUpCode$ = keyUp$.pipe(map(({ code }) => code));
// Si solo quiero una propiedad
//const keyUpPluck$ = keyUp$.pipe(pluck("key"));
// Si queremos una propiedad dentro de otro objeto  | ejem: target.baseURI
const keyUpPluck$ = keyUp$.pipe(pluck("target", "baseURI"));

const keyUpMapTo$ = keyUp$.pipe(mapTo("Tecla presionada"));

keyUp$.subscribe(console.log);
keyUpCode$.subscribe((val) => console.log("map", val));
keyUpPluck$.subscribe((val) => console.log("pluck", val));
keyUpMapTo$.subscribe((val) => console.log("mapTo", val));
