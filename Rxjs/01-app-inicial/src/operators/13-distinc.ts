/**DISTINC */
/**
 * Returns an Observable that emits all items
 * emitted by the source Observable that are
 * distinct by comparison from previous items.
 *
 */
import { from, of } from "rxjs";
import { distinct, tap } from "rxjs/operators";

const numeros$ = of(1, 1, 1, 2, 2, 3, 6, 5, 5, 6, 9, 8, 7, 4, 2);

/**Emite solo los valores que no
 * hayan sido previamente mostrados.
 */

numeros$
  .pipe(
    distinct() // ===
  )
  .subscribe(console.log);

interface Personaje {
  nombre: string;
}

const personajes: Personaje[] = [
  {
    nombre: "yuki",
  },
  {
    nombre: "hakiman",
  },
  {
    nombre: "yukinon",
  },
  {
    nombre: "alice",
  },
  {
    nombre: "zero-two",
  },
  {
    nombre: "yuki",
  },
];

from(personajes)
  .pipe(
    //De esta manera todos los obj son emitidos,
    // ya que cada uno apunta a una direccion en memoria
    //distinct(),
    distinct((p) => p.nombre)
  )
  .subscribe(console.log);
