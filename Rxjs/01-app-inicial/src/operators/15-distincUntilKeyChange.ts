/**DISTINC UNTIL KEY CHANGE */
/**
 * Returns an Observable that emits all items emitted
 * by the source Observable that are distinct by
 * comparison from the previous item, using a
 * property accessed by using
 * the key provided to check if the two items are distinct.
 *
 */
import { from, of } from "rxjs";
import { distinctUntilKeyChanged } from "rxjs/operators";

interface Personaje {
  nombre: string;
}

const personajes: Personaje[] = [
  {
    nombre: "yuki",
  },
  {
    nombre: "hakiman",
  },
  {
    nombre: "yukinon",
  },
  {
    nombre: "alice",
  },
  {
    nombre: "alice",
  },
  {
    nombre: "yuki",
  },
];

from(personajes)
  .pipe(
    //De esta manera todos los obj son emitidos,
    // ya que cada uno apunta a una direccion en memoria
    //distinct(),
    // Aqui si hace la distintincion
    // distinct((p) => p.nombre)

    // Evalua el valor anterior emitido,
    // Si es distinto lo emite, pero si es
    // igual no lo muestra.
    //De esta manera todos los obj son emitidos por igual,
    //distinctUntilChanged()
    // Aqui si evalúa en base ala propiedad nombre
    //distinctUntilChanged((ant, act) => ant.nombre === act.nombre)

    // Evalua la propiedad anterior emitida, si es diferente
    // lo emite, sino no lo muestra.
    distinctUntilKeyChanged("nombre")
  )
  .subscribe(console.log);
