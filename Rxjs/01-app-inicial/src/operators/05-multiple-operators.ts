/**Filter items emitted by the source Observable by only emitting those that satisfy a specified predicate. */
import { of, from, range, fromEvent } from "rxjs";
import { map, filter } from "rxjs/operators";

// range(1, 10)
//   .pipe(filter((val) => val % 2 === 1))
//   .subscribe(console.log);

range(1, 10).pipe(
  filter((val, index) => {
    console.log("index; ", index);
    return val % 2 === 1;
  })
);
//.subscribe(console.log);

interface Personaje {
  tipo: string;
  nombre: string;
}

const personajes: Personaje[] = [
  { tipo: "heroe", nombre: "Batman" },
  { tipo: "heroe", nombre: "Superman" },
  { tipo: "villano", nombre: "Jocker" },
];

const personajes$ = from(personajes).pipe(
  filter((val) => val.tipo === "villano")
);

personajes$.subscribe(console.log);

// Otro ejemplo para la cadena de operadores
// Se disparan de arriba hacia abajo
const keyUp$ = fromEvent<KeyboardEvent>(document, "keyup").pipe(
  map((event) => event.code), // KeyboardEvent -> string
  filter((key) => key === "Enter") // String -> String
);

keyUp$.subscribe(console.log);
