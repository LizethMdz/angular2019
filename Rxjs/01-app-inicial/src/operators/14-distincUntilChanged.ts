/**DISTINC UNTIL CHANGE */
/**
Returns an Observable that emits all items emitted
 by the source Observable that 
are distinct by comparison from the previous item.
 *
 */
import { from, of } from "rxjs";
import { distinct, distinctUntilChanged, tap } from "rxjs/operators";

const numeros$ = of(1, 1, 1, 2, 2, 3, 6, 5, 5, 6, 9, 8, 7, 4, 2);

numeros$
  .pipe(
    /**Emite solo los valores que no
     * hayan sido emitidos en la emision anterior
     * */
    distinctUntilChanged() // ===
  )
  .subscribe(console.log);

interface Personaje {
  nombre: string;
}

const personajes: Personaje[] = [
  {
    nombre: "yuki",
  },
  {
    nombre: "hakiman",
  },
  {
    nombre: "yukinon",
  },
  {
    nombre: "alice",
  },
  {
    nombre: "alice",
  },
  {
    nombre: "yuki",
  },
];

from(personajes)
  .pipe(
    //De esta manera todos los obj son emitidos,
    // ya que cada uno apunta a una direccion en memoria
    //distinct(),
    // Aqui si hace la distintincion
    // distinct((p) => p.nombre)

    // Evalua el valor anterior emitido,
    // Si es distinto lo emite, pero si es
    // igual no lo muestra.
    //De esta manera todos los obj son emitidos por igual,
    //distinctUntilChanged()
    // Aqui si evalúa en base ala propiedad nombre
    distinctUntilChanged((ant, act) => ant.nombre === act.nombre)
  )
  .subscribe(console.log);
