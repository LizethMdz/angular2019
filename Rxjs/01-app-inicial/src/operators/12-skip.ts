/**SKIP */
/** Returns an Observable that skips the first count items emitted by the source Observable. */
import { fromEvent, interval } from "rxjs";
import { skip, takeUntil, tap } from "rxjs/operators";

const boton = document.createElement("button");

boton.innerHTML = "Detener Timer";

document.querySelector("body").append(boton);

const counter$ = interval(1000);

const clickBtn$ = fromEvent(boton, "click").pipe(
  tap(() => console.log("Tap antes del skip")),
  // Emite el evento una vez que se haya cumplido
  // el no. de saltos dados como parametro
  skip(1),
  tap(() => console.log("Tap despues del skip"))
);

counter$
  .pipe(
    // Emite valores hasta que otro Obsv emita su primer valor
    // En este caso hasta que ocurra el evento click del boton
    takeUntil(clickBtn$)
  )
  .subscribe({
    next: (val) => console.log("next:", val),
    complete: () => console.log("complete"),
  });
