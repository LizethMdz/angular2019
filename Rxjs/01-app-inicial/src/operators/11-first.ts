/**FIRST */
/**
 * Emits only the first value (or the first value that meets some condition) emitted by the source Observable.
 */
import { first, map, take, tap } from "rxjs/operators";
import { fromEvent, of } from "rxjs";

const click$ = fromEvent<MouseEvent>(document, "click");

click$
  .pipe(
    // Solo toma el primer valor emitido y termina
    //take(1),
    tap<MouseEvent>(console.log),
    map(({ clientX, clientY }) => ({ clientY, clientX })),
    // Segurá emitiendo eventos, pero hasta que no se cumpla el
    // predicado, no emitirá el primer valor y se completará.
    // También funcionará similar
    first((event) => event.clientY >= 150)
  )
  .subscribe({
    next: (val) => console.log("next:", val),
    complete: () => console.log("completed"),
  });
