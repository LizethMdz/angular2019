/**Interval y timer */

import { asyncScheduler } from "rxjs";

const observer = {
  next: (val) => console.log("next", val),
  complete: () => console.log("complete"),
};

const saludar = () => console.log("hola mundo");
const saludar2 = (nombre) => console.log("hola mundo ", nombre);

// Establecer un timeout
// Params (function, time)
//asyncScheduler.schedule(saludar, 2000);
// Establecer un timeout
// Params (function, time, state)
//asyncScheduler.schedule(saludar2, 2000, "lizeth");

// Establecer un interval
// No puede ser un a funcion de flecha
// Params (function, interval, state)
const subs = asyncScheduler.schedule(
  function (state) {
    console.log("state", state);
    // Aqui se crea el intervalo
    this.schedule(state + 1, 1000);
  }, // Donde inicia el timer
  3000,
  0
);

//Cancelar un asyncScheduler

// setTimeout(() => {
//   subs.unsubscribe();
// }, 6000);

// O bien cancelarlo con un scheduler
asyncScheduler.schedule(() => subs.unsubscribe(), 6000);
