/**Event target */
/**FROM EVENT */

//fromEvent<Event>

import { fromEvent } from "rxjs";

/**Eventos del dom */

const src$ = fromEvent<KeyboardEvent>(document, "keyup");
const src2$ = fromEvent<MouseEvent>(document, "click");

const observer = {
  next: (val) => console.log("next", val),
};

src2$.subscribe(({ x, y }) => {
  console.log(x, y);
});
src$.subscribe((evento) => {
  console.log(evento.key);
});
