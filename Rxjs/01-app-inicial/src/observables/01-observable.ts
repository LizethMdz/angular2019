import { Observable, Observer } from "rxjs";

//const obs$ = Observable.create();

const observer: Observer<any> = {
  next: (value) => console.log("siguiente [next]:", value),
  error: (error) => console.log("error [obs]:", error),
  complete: () => console.log("completed"),
};

const obs$ = new Observable<string>((subscriber) => {
  subscriber.next("Hola");
  subscriber.next("Hola");
  subscriber.next("Hola");

  subscriber.complete();
});

// obs$.subscribe(
//   (resp) => console.log(resp),
//   (error) => console.log(error),
//   () => console.log("Completado")
// );

obs$.subscribe(observer);

//obs$.subscribe()
