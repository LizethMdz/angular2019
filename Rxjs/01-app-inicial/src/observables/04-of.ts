/** OF */
// Emite los valores de manera sincrona (por el Observable)

import { of } from "rxjs";

const obs$ = of(1, 2, 3, 4, 5, 6);
const obs2$ = of(...[1, 2, 3, 4, 5, 6], 2, 3, 4, 5);
const obs3$ = of(
  [1, 2],
  { a: 1, b: 2 },
  function () {},
  true,
  Promise.resolve(true)
);

console.log("Inicio del obs");

obs2$.subscribe(
  (next) => console.log("next ", next),
  null,
  () => console.log("terminamos")
);

console.log("Fin del obs");
