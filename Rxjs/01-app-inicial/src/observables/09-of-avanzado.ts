import { of, from } from "rxjs";

// of - toma argumentos y genera una secuencia
// from - array, promise, iterable, observable

const observer = {
  next: (val) => console.log("next", val),
  complete: () => console.log("complete"),
};

const source$ = from([1, 2, 4, 5, 6]);
const source2$ = of([1, 2, 4, 5, 6]);

const source3$ = from("lizeth");

const source4$ = from(fetch("https://api.github.com/users/klerith"));

source4$.subscribe(async (resp) => {
  const info = await resp.json();
  console.log(info);
});

// Subscribe de una funcion generadora de un iterador
const miGenerador = function* () {
  yield 1;
  yield 2;
  yield 3;
  yield 4;
  yield 5;
};

const miGenOutput = miGenerador();

//formas de iterarlo

// for (const numero of miGenOutput) {
//   console.log(numero);
// }

// O bien con un from

from(miGenOutput).subscribe(observer);
