import { Observable, Observer } from "rxjs";

//const obs$ = Observable.create();

const observer: Observer<any> = {
  next: (value) => console.log("[next]:", value),
  error: (error) => console.log("[error]:", error),
  complete: () => console.log("completado"),
};

const intervalos = new Observable<number>((subscriber) => {
  // Crear un contador 1.2.3,4,5,6.....
  let cont = 0;
  const interval = setInterval(() => {
    // Cada segundo
    cont = cont + 1;
    subscriber.next(cont);
    console.log(cont);
  }, 1000);

  setTimeout(() => {
    subscriber.complete();
  }, 2500);

  return () => {
    clearInterval(interval);
    console.log("Destruido...");
  };
});

// const contador = intervalos.subscribe((num) => {
//   console.log("Num: ", num);
// });

const contador = intervalos.subscribe(observer);
const contador2 = intervalos.subscribe(observer);
const contador3 = intervalos.subscribe(observer);

// Añadir una subs hija
// Solo ejecuta el complete una vez
contador.add(contador2).add(contador3);

// contador.unsubscribe();

setTimeout(() => {
  // De esta manera limpia los subs hijos
  contador.unsubscribe();
  //   contador2.unsubscribe();
  //   contador3.unsubscribe();
  console.log("Completado");
}, 4000);
