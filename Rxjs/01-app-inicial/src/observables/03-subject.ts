import { Observable, Observer, Subject } from "rxjs";

//const obs$ = Observable.create();

const observer: Observer<any> = {
  next: (value) => console.log("[next]:", value),
  error: (error) => console.log("[error]:", error),
  complete: () => console.log("completado"),
};

const intervalo$ = new Observable<number>((subs) => {
  const interval = setInterval(() => subs.next(Math.random()), 1000);

  return () => {
    clearInterval(interval);
    console.log("Unsubscribe");
  };
});

// De esta manera nos suscribimos, pero recibimos
// distintos valores cada 3s.

//const subs1 = intervalo$.subscribe(rnd => console.log('Subs1: ', rnd));
//const subs2 = intervalo$.subscribe(rnd => console.log('Subs2: ', rnd))

// Para subscribirnos a un unico valor en cualquier subs
/** 1- Casteo multiple
 *  2- Tambien es un observer
 *  3- Next, error y complete
 */
const subject$ = new Subject();
const subscription = intervalo$.subscribe(subject$);
// De esta manera ambos subs imprimen el mismo valor
//const subs1 = subject$.subscribe((rnd) => console.log("Subs1: ", rnd));
//const subs2 = subject$.subscribe((rnd) => console.log("Subs2: ", rnd));

const subs1 = subject$.subscribe(observer);
const subs2 = subject$.subscribe(observer);

/**Nota =========================== */

/**
 * OBSERVABLES FRÍOS
Los Observables "fríos" son aquellos que no emiten 
valores hasta que haya una suscripción activa, ya que la 
información es producida dentro del Observable y por tanto 
solo emiten valores en el momento en que se establece una 
nueva subscripción, por eso, el ejemplo previo que hemos 
visto, math.random() devuelve valores diferentes.
 */

/**
  * OBSERVABLES CALIENTES
Por contra, los Observables "calientes" son aquellos que 
pueden emitir valores sin que haya ninguna subscripción 
activa, porque la información del stream se produce 
fuera del propio Observable. RxJs dispone de
 algunos Observables ¨calientes¨ y el mejor ejemplo 
 de éstos, es fromEvent que nos permite establecer 
 un Observable sobre 
cualquier tipo de evento como el click del ratón
  */

/** ================================= */

// El subject nos permite transformar un
// Cold Observable a un Hot Observable

/** ============================================= */

// CO - Data emitida por si misma dentro del Obs.
// HO - Data emitida al exterior del Obs.

setTimeout(() => {
  subject$.next(10);
  subject$.complete();
  subscription.unsubscribe();
}, 3500);
