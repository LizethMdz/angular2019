// Rango de numeros
/**RANGE - Emite una secuencia de numeros dentro de
 * un rango especifico
 */

import { asyncScheduler, of, range } from "rxjs";

//const src$ = of(1,2,3,4,5,6);
// No estable como tal un rango
// Sino son emisiones
// asyncScheduler - Se vuelve asincrona
const src$ = range(1, 5, asyncScheduler);
console.log("inicio");
src$.subscribe(console.log);
console.log("fin");
