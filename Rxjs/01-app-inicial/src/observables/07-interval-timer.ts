/**Interval y timer */

import { interval, timer } from "rxjs";

const observer = {
  next: (val) => console.log("next", val),
  complete: () => console.log("complete"),
};

// Para programar la emision de un evento en una fecha
// determinada
const hoy5 = new Date(); //ahora
hoy5.setSeconds(hoy5.getMinutes() + 5);

const interval$ = interval(1000);
//const timer$ = timer(1000);
//const timer$ = timer(2000, 1000);
const timer$ = timer(hoy5);

// Es asincrono
//interval$.subscribe(observer);
// Tambien es asincrono
console.log("inicio");
timer$.subscribe(observer);
console.log("fin");
