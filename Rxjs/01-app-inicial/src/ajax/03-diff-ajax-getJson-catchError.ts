import { ajax, AjaxError } from "rxjs/ajax";
import { map, pluck, catchError } from "rxjs/operators";
import { of } from "rxjs";

const url = "https://httpbin.org/delay/1";
//const url = "https://api.github.com/users?per_page=5";

const manejaError = (err: AjaxError) => {
  console.warn("error: ", err.message);
  return of({
    ok: false,
    usuarios: [],
  });
};

// const obs$ = ajax
//   .getJSON(url, {
//     "Content-Type": "application/json",
//     "mi-token": "ABC123",
//   })
//   .pipe(catchError(manejaError));

// const obs2$ = ajax(url).pipe(catchError(manejaError));

const obs$ = ajax
  .getJSON(url, {
    "Content-Type": "application/json",
    "mi-token": "ABC123",
  })
  .pipe();

//const obs2$ = ajax(url).pipe();

// El error en esta peticion es atrapado por el cathError (imprime msm)
// Sin embargo, el valor emitido es el Obs retornado por
// catchError ya que interpreta que no es un valor no válido

obs$.pipe(catchError(manejaError)).subscribe({
  next: (val) => console.log("valor", val),
  error: (err) => console.log("error", err),
  complete: () => console.log("completado"),
});
//obs2$.subscribe((data) => console.log("data:", data));
