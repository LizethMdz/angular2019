import { ajax, AjaxError } from "rxjs/ajax";
import { map, pluck, catchError } from "rxjs/operators";
import { of } from "rxjs";

const url = "https://api.github.com/users?per_page=5";

const fetchPromesa = fetch(url);

const manejaErrores = (response: Response) => {
  if (!response.ok) {
    throw new Error(response.statusText);
  }

  return response;
};

const atrapaError = (err: AjaxError) => {
  console.warn("error en:", err.message);
  return of([]);
};

/**El sucede pero no es procesado */
// fetchPromesa
//   .then((resp) => resp.json())
//   .then((data) => console.log("data:", data))
//   .catch((err) => console.warn("error en usuarios", err));

/**Aqui el error si sucede, pero observamos
 * bastantes pasos
 */
// fetchPromesa
//   .then(manejaErrores)
//   .then((resp) => resp.json())
//   .then((data) => console.log("data:", data))
//   .catch((err) => console.warn("error en usuarios", err));

/**
 * Atrapamos y procesamos el error con el uso de
 * funciones reactivas
 */
ajax(url)
  .pipe(
    pluck("response"),
    // Recibe el error
    catchError(atrapaError)
  )
  .subscribe((users) => console.log("usuarios:", users));
