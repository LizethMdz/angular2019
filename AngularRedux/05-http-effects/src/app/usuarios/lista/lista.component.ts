import { cargarUsuarios } from './../../store/actions/usuarios.actions';
import { AppState } from './../../store/app.reducer';
import { Usuario } from './../../models/usuario.models';
import { UsuarioService } from './../../services/usuario.service';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css'],
})
export class ListaComponent implements OnInit {
  usuarios: Usuario[] = [];
  loading: boolean = false;
  error: any;
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    // this.usuarioS.getListOfUsers().subscribe((usuarios: Usuario[]) => {
    //   console.log(usuarios);
    //   this.usuarios = usuarios;
    // });
    this.store.select('usuarios').subscribe(({ users, loading, error }) => {
      this.usuarios = users;
      this.loading = loading;
      this.error = error;
    });
    this.store.dispatch(cargarUsuarios());
  }
}
