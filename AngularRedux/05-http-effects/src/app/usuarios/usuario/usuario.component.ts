import { Usuario } from './../../models/usuario.models';
import { cargarUsuario } from './../../store/actions/usuario.action';
import { Store } from '@ngrx/store';
import { AppState } from './../../store/app.reducer';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css'],
})
export class UsuarioComponent implements OnInit {
  usuario: Usuario;
  constructor(private router: ActivatedRoute, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.select('usuario').subscribe(({ user, loading, error }) => {
      this.usuario = user;
    });

    this.router.params.subscribe(({ id }) => {
      console.log(id);
      this.store.dispatch(cargarUsuario({ id }));
    });
  }
}
