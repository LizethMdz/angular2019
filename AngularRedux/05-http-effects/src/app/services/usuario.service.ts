import { UsuariosResponse } from './../models/usuario.models';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UsuarioService {
  private url = 'https://reqres.in/api';
  constructor(private http: HttpClient) {}

  getListOfUsers() {
    return this.http
      .get(`${this.url}/users?per_page=6`)
      .pipe(map((usuarios: UsuariosResponse) => usuarios.data));
  }
  getUserById(id: string) {
    return this.http
      .get(`${this.url}/users/${id}?`)
      .pipe(map((usuario) => usuario['data']));
  }
}
