export interface UsuariosResponse {
  page?: number;
  per_page?: number;
  total?: number;
  total_pages?: number;
  data?: Usuario[] | null;
  support?: Support;
}
export interface Usuario {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}

export class Usuario {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;

  constructor(
    id: number,
    email: string,
    first_name: string,
    last_name: string,
    avatar: string
  ) {
    this.id = id;
    this.email = email;
    this.first_name = first_name;
    this.last_name = last_name;
    this.avatar = avatar;
  }
}

export interface Support {
  url: string;
  text: string;
}
