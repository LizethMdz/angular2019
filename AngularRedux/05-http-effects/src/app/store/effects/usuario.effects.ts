import { UsuarioService } from './../../services/usuario.service';
import * as usuariosActions from './../actions';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UsuarioEffects {
  // 1. Inyeccion de módulos
  // 2. Constructor
  constructor(private actions$: Actions, private usuariosS: UsuarioService) {}

  cargarUsuario$ = createEffect(() =>
    this.actions$.pipe(
      ofType(usuariosActions.cargarUsuario),
      tap((data) => console.log('effect tap: ', data)),
      mergeMap((action) =>
        this.usuariosS.getUserById(action.id).pipe(
          map((user) =>
            usuariosActions.cargarUsuarioSuccess({ usuario: user })
          ),
          catchError((err) =>
            of(usuariosActions.cargarUsuarioError({ payload: err }))
          ),
          tap((data) => console.log('getUserById effect', data))
        )
      )
    )
  );
}
