import { UsuarioService } from './../../services/usuario.service';
import * as usuariosActions from './../actions/usuarios.actions';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UsuariosEffects {
  // 1. Inyeccion de módulos
  // 2. Constructor
  constructor(private actions$: Actions, private usuariosS: UsuarioService) {}

  cargarUsuarios$ = createEffect(() =>
    this.actions$.pipe(
      ofType(usuariosActions.cargarUsuarios),
      //tap((data) => console.log('effect tap: ', data)),
      mergeMap(() =>
        this.usuariosS.getListOfUsers().pipe(
          map((users) =>
            usuariosActions.cargarUsuariosSuccess({ usuarios: users })
          ),
          catchError((err) =>
            of(usuariosActions.cargarUsuariosError({ payload: err }))
          )
          //tap((data) => console.log('getUsuers effect', data))
        )
      )
    )
  );
}

/**
 * 
 *         () => this.actions$.pipe(
            ofType(usuariosActions.cargarUsuarios),
            tap(data => console.log('effect tap: ', data)),
            // Combina el obs anterior con el nuevo
            mergeMap(
                () => this.usuariosS.getListOfUsers()
                .pipe(
                    tap(data => console.log('getUsuers effect', data))
                )
            )
        )
 */
