import { AppState } from './../../app.reducer';
import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import Swal from 'sweetalert2';
import * as ui from '../../shared/ui.actions';
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  cargando: boolean = false;
  uiSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private authS: AuthService,
    private router: Router,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      correo: ['jorge@mail.com', [Validators.required, Validators.email]],
      password: ['123456', Validators.required],
    });

    this.uiSubscription = this.store.select('ui').subscribe((ui) => {
      this.cargando = ui.isLoading;
    });
  }

  ngOnDestroy() {
    this.uiSubscription.unsubscribe();
  }

  logearUsuario() {
    if (this.loginForm.invalid) {
      return;
    }

    // Dispatch
    this.store.dispatch(ui.isLoading());

    // Swal.fire({
    //   allowOutsideClick: false,
    //   title: 'Cargardo!',
    //   text: 'Espere por favor...',
    //   icon: 'info',
    // });
    // Swal.showLoading();

    const { correo, password } = this.loginForm.value;
    this.authS
      .loginUsuario(correo, password)
      .then((credenciales) => {
        console.log(credenciales.user);
        this.store.dispatch(ui.stopLoading());
        //Swal.close();
        this.router.navigateByUrl('/');
      })
      .catch((err) => {
        console.log(err);
        this.store.dispatch(ui.stopLoading());
        Swal.fire({
          allowOutsideClick: false,
          title: 'Error!',
          text: err.message,
          icon: 'error',
        });
      });
  }
}
