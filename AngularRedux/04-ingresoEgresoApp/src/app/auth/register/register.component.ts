import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.reducer';
import Swal from 'sweetalert2';
import { Usuario } from './../../models/usuario.model';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as ui from '../../shared/ui.actions';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  registroForm: FormGroup;
  cargando: boolean = false;
  uiSubscription: Subscription;
  constructor(
    private fb: FormBuilder,
    private authS: AuthService,
    private router: Router,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.registroForm = this.fb.group({
      nombre: ['', Validators.required],
      correo: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });

    this.uiSubscription = this.store.select('ui').subscribe((ui) => {
      this.cargando = ui.isLoading;
    });
  }

  ngOnDestroy() {
    this.uiSubscription.unsubscribe();
  }

  crearUsuario() {
    if (!this.registroForm.valid) return;
    const { nombre, correo, password } = this.registroForm.value;
    const user = new Usuario(nombre, correo, password);

    this.store.dispatch(ui.isLoading());

    this.authS
      .crearUsuario(user)
      .then((credenciales) => {
        console.log(credenciales);
        this.store.dispatch(ui.stopLoading());
        this.router.navigateByUrl('/');
      })
      .catch((err) => {
        console.log(err), this.store.dispatch(ui.stopLoading());
        Swal.fire({
          allowOutsideClick: false,
          title: 'Error!',
          text: err.message,
          icon: 'error',
        });
      });
  }
}
