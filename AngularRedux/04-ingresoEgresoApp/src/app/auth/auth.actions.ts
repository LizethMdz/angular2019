import { UsuarioFire } from './../models/userFire.model';
import { createAction, props } from '@ngrx/store';

export const setUser = createAction(
  '[Auth] setUser',
  props<{ user: UsuarioFire }>()
);

export const unSetUser = createAction('[Auth] unSetUser');
