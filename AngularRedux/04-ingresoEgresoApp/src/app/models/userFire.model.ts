export class UsuarioFire {
  static fromFirebase({ email, uid, nombre }) {
    return new UsuarioFire(uid, nombre, email);
  }

  constructor(
    public uid: string,
    public nombre: string,
    public email: string
  ) {}
}
