export class Usuario {
  public nombre: string;
  public correo: string;
  public password: string;
  public uid?: string;

  constructor(nombre: string, correo: string, password: string) {
    this.nombre = nombre;
    this.correo = correo;
    this.password = password;
  }
}
