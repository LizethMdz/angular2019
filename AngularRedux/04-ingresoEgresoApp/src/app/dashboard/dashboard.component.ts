import { IngresoEgreso } from './../models/ingreso-egreso.model';
import { IngresoEgresoService } from './../services/ingreso-egreso.service';
import { AppState } from './../app.reducer';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { setItems } from '../ingreso-egreso/ingreso-egreso.actions';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  userSubs: Subscription;
  ingresosESubs: Subscription;
  constructor(
    private store: Store<AppState>,
    private ingresoES: IngresoEgresoService
  ) {}

  ngOnInit(): void {
    this.userSubs = this.store
      .select('auth')
      .pipe(filter((auth) => auth.user !== null))
      .subscribe(({ user }) => {
        this.ingresosESubs = this.ingresoES
          .initIngresosEgresosListener(user.uid)
          .subscribe((ingresosEgresosFB: IngresoEgreso[]) => {
            console.log(ingresosEgresosFB);
            this.store.dispatch(setItems({ items: ingresosEgresosFB }));
          });
      });
  }

  ngOnDestroy(): void {
    this.ingresosESubs?.unsubscribe();
    this.userSubs?.unsubscribe();
  }
}
