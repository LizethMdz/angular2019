import * as ingesoEgesoActions from './../ingreso-egreso/ingreso-egreso.actions';
import { Store } from '@ngrx/store';
import { AppState } from './../app.reducer';
import { UsuarioFire } from './../models/userFire.model';
import { Usuario } from './../models/usuario.model';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';

import * as authActions from '../auth/auth.actions';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _user: UsuarioFire;
  userSubscription: Subscription;
  get user() {
    return this._user;
  }
  constructor(
    public auth: AngularFireAuth,
    public firestore: AngularFirestore,
    private store: Store<AppState>
  ) {}

  initAuthListener() {
    this.auth.authState.subscribe((fuser: any) => {
      if (fuser) {
        // existe
        this.userSubscription = this.firestore
          .doc(`${fuser.uid}/usuario`)
          .valueChanges()
          .subscribe((firestoreUser: any) => {
            const user = UsuarioFire.fromFirebase(firestoreUser);
            this._user = user;
            this.store.dispatch(authActions.setUser({ user }));
          });
      } else {
        // no existe
        this._user = null;
        this.userSubscription?.unsubscribe();
        this.store.dispatch(authActions.unSetUser());
        this.store.dispatch(ingesoEgesoActions.unSetItems());
      }
    });
  }

  crearUsuario(usuario: Usuario) {
    return this.auth
      .createUserWithEmailAndPassword(usuario.correo, usuario.password)
      .then(({ user }) => {
        const newUser = new UsuarioFire(user.uid, usuario.nombre, user.email);
        return this.firestore.doc(`${user.uid}/usuario`).set({ ...newUser });
      });
  }

  loginUsuario(correo: string, password: string) {
    return this.auth.signInWithEmailAndPassword(correo, password);
  }

  logout() {
    return this.auth.signOut();
  }

  isAuth() {
    return this.auth.authState.pipe(map((fbUser) => fbUser != null));
  }
}
