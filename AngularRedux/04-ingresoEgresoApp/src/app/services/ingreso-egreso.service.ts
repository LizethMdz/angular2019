import { map } from 'rxjs/operators';
import firebase from 'firebase';
import { AuthService } from './auth.service';
import { IngresoEgreso } from './../models/ingreso-egreso.model';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class IngresoEgresoService {
  constructor(
    private firestore: AngularFirestore,
    private authS: AuthService
  ) {}

  crearIngresoEgreso(
    ingresoEgreso: IngresoEgreso
  ): Promise<DocumentReference<firebase.firestore.DocumentData>> {
    const uid = this.authS.user.uid;
    delete ingresoEgreso.uid;
    return this.firestore
      .doc(`${uid}/ingreso-egreso`)
      .collection('items')
      .add({ ...ingresoEgreso });
  }

  initIngresosEgresosListener(uid: string): Observable<IngresoEgreso[]> {
    return this.firestore
      .collection(`${uid}/ingreso-egreso/items`)
      .snapshotChanges()
      .pipe(
        map((snapshot) =>
          snapshot.map((doc) => ({
            uid: doc.payload.doc.id,
            ...(doc.payload.doc.data() as IngresoEgreso),
          }))
        )
      );
  }

  borrarIngresoEgreso(uidItem: string): Promise<void> {
    const uid = this.authS.user.uid;
    return this.firestore
      .doc(`${uid}/ingresos-egresos/items/${uidItem}`)
      .delete();
  }
}
