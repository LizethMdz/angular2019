import { AppStateWithIngreso } from './../ingreso-egreso.reducer';
import Swal from 'sweetalert2';
import { IngresoEgresoService } from './../../services/ingreso-egreso.service';
import { IngresoEgreso } from './../../models/ingreso-egreso.model';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css'],
})
export class DetalleComponent implements OnInit, OnDestroy {
  ingresoESubs: Subscription;
  ingresosEgresos: IngresoEgreso[] = [];
  constructor(
    private store: Store<AppStateWithIngreso>,
    private ingresoES: IngresoEgresoService
  ) {}

  ngOnInit(): void {
    this.ingresoESubs = this.store
      .select('ingresosEgresos')
      //.pipe(map(({ items }) => items))
      .subscribe(({ items }) => (this.ingresosEgresos = items));
  }

  ngOnDestroy(): void {
    this.ingresoESubs.unsubscribe();
  }

  borrar(uidItem: string) {
    this.ingresoES
      .borrarIngresoEgreso(uidItem)
      .then(() => Swal.fire('Borrado', 'Item borrado', 'success'))
      .catch((err) => Swal.fire('Borrado', err.message, 'error'));
  }
}
