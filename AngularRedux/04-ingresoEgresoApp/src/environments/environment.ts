// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebase: {
    apiKey: 'AIzaSyDDc--heP7Tx_m7if2pAXVWI7a6Ee-58-Y',
    authDomain: 'egreso-ingresoapp-a68f7.firebaseapp.com',
    projectId: 'egreso-ingresoapp-a68f7',
    storageBucket: 'egreso-ingresoapp-a68f7.appspot.com',
    messagingSenderId: '900926653479',
    appId: '1:900926653479:web:90965d24c536667bc47a02',
    measurementId: 'G-K807Z5ZHF0',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
