import { AppState } from './app.reducer';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as actions from './contador/contador.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'redux-app';

  contador: number;

  constructor(private store: Store<AppState>) {
    // Solo observamos al contador y no a los demas valores

    this.store.select('contador').subscribe((contador) => {
      console.log(contador);
      this.contador = contador;
    });
  }

  incrementar() {
    this.store.dispatch(actions.increment());
  }

  decrementar() {
    this.store.dispatch(actions.decrement());
  }
}
