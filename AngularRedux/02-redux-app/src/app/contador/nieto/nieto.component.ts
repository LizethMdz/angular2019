import { reset } from './../contador.actions';
import { AppState } from './../../app.reducer';
import { Store } from '@ngrx/store';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-nieto',
  templateUrl: './nieto.component.html',
  styles: [],
})
export class NietoComponent implements OnInit {
  contador: number;
  // @Input() contadorFromSon: number;
  // @Output() contadorHasChangedGrandChild = new EventEmitter<number>();
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.select('contador').subscribe((cont) => (this.contador = cont));
  }

  reset() {
    this.store.dispatch(reset());
    // this.contadorFromSon = 0;
    // this.contadorHasChangedGrandChild.emit(this.contadorFromSon);
  }
}
