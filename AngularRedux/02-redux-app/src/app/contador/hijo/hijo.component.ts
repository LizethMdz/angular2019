import * as actions from './../contador.actions';
import { AppState } from './../../app.reducer';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styles: [],
})
export class HijoComponent implements OnInit {
  contador: number;
  // @Input() contadorFromPather: number;
  // @Output() contadorHasChangedSon = new EventEmitter<number>();
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store
      .select('contador')
      // .subscribe((cont) => (this.contadorFromPather = cont));
      .subscribe((cont) => (this.contador = cont));
  }

  multiplicar() {
    this.store.dispatch(actions.multiplicar({ numero: 5 }));
    // this.contadorFromPather *= 5;
    // this.contadorHasChangedSon.emit(this.contadorFromPather);
  }
  dividir() {
    this.store.dispatch(actions.dividir({ numero: 5 }));
    // this.contadorFromPather /= 2;
    // this.contadorHasChangedSon.emit(this.contadorFromPather);
  }

  resetNieto(newContador) {
    // this.contadorFromPather = newContador;
    // this.contadorHasChangedSon.emit(this.contadorFromPather);
  }
}
