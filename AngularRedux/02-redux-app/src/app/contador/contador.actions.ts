import { createAction, props } from '@ngrx/store';

export const increment = createAction('[Counter] Incrementar');
export const decrement = createAction('[Counter] Decrementar');
export const reset = createAction('[Counter] Resetear');
export const multiplicar = createAction(
  '[Counter] Multiplicar',
  props<{ numero: number }>()
);
export const dividir = createAction(
  '[Counter] Dividir',
  props<{ numero: number }>()
);
