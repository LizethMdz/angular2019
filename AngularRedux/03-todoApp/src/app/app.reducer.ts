import { filtrosValidos } from './filtro/filtro.actions';
import { ActionReducerMap } from '@ngrx/store';
import { Todo } from './todos/models/todo.model';
import { todoReducer } from './todos/todo.reducer';
import { filtroReducer } from './filtro/filtro.reducer';

/** Estructura de mi State */
export interface AppState {
  todos: Todo[];
  filtro: filtrosValidos;
}

/** Listado de Reducers */

export const appReducers: ActionReducerMap<AppState> = {
  todos: todoReducer,
  filtro: filtroReducer,
};
