import { filtrosValidos } from './../../filtro/filtro.actions';
import { AppState } from './../../app.reducer';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';

import { Todo } from './../models/todo.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css'],
})
export class TodoListComponent implements OnInit {
  todos: Todo[] = [];
  filtroSeleccionado: filtrosValidos;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    /**Si solo quisieramos manejar un state (todos) */
    // this.store.select('todos').subscribe((todos) => {
    //   this.todos = todos;
    // });
    /** Con un state global */
    // this.store.subscribe((state) => {
    //   this.todos = state.todos;
    //   this.filtroSeleccionado = state.filtro;
    // });
    /**Con un state desestructurado */
    this.store.subscribe(({ todos, filtro }) => {
      this.todos = todos;
      this.filtroSeleccionado = filtro;
    });
  }
}
