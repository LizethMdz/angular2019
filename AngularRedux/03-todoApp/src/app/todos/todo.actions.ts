import { createAction, props } from '@ngrx/store';

export const crear = createAction(
  '[TODO] Crear Note',
  props<{ texto: string }>()
);
export const toggle = createAction(
  '[TODO] Toggle Note',
  props<{ id: number }>()
);
export const editar = createAction(
  '[TODO] Editar Note',
  props<{ id: number; texto: string }>()
);
export const borrar = createAction(
  '[TODO] Borrar Note',
  props<{ id: number }>()
);

export const toggleAll = createAction(
  '[TODO] Toggle All Notes',
  props<{ completado: boolean }>()
);

export const limpiar = createAction('[TODO] Clean Completed Notes');
