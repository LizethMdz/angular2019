import { Todo } from './models/todo.model';
import {
  crear,
  toggle,
  editar,
  borrar,
  toggleAll,
  limpiar,
} from './todo.actions';
import { createReducer, on } from '@ngrx/store';

export const initialState: Todo[] = [
  new Todo('Curso de React'),
  new Todo('Curso de Python'),
  new Todo('Curso de Go Lang'),
];

const _todoReducer = createReducer(
  initialState,
  on(crear, (state, { texto }) => [...state, new Todo(texto)]),
  on(borrar, (state, { id }) => state.filter((note) => note.id !== id)),
  on(toggle, (state, { id }) => {
    return state.map((note) => {
      if (note.id === id) {
        return {
          ...note,
          completado: !note.completado,
        };
      } else {
        return note;
      }
    });
  }),
  on(editar, (state, { id, texto }) => {
    return state.map((note) => {
      if (note.id === id) {
        return {
          ...note,
          texto: texto,
        };
      } else {
        return note;
      }
    });
  }),
  on(toggleAll, (state, { completado }) => {
    return state.map((note) => {
      return {
        ...note,
        completado: completado,
      };
    });
  }),
  on(limpiar, (state) => {
    return state.filter((note) => !note.completado);
  })
);

export function todoReducer(state, action) {
  return _todoReducer(state, action);
}
