import * as actions from './../todo.actions';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.reducer';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Todo } from './../models/todo.model';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css'],
})
export class TodoItemComponent implements OnInit {
  @ViewChild('inputTxt') txtInputEditable: ElementRef;
  @Input() todo: Todo;
  formNotes: FormGroup;
  // chkCompletado: FormControl;
  // txtEditable: FormControl;

  editando: boolean = false;

  constructor(private store: Store<AppState>) {
    this.formNotes = new FormGroup({
      chkCompletado: new FormControl(),
      txtEditable: new FormControl(Validators.required),
    });
  }

  ngOnInit(): void {
    this.formNotes.setValue({
      chkCompletado: this.todo.completado,
      txtEditable: this.todo.texto,
    });
    // this.chkCompletado = new FormControl(this.todo.completado);
    // this.txtEditable = new FormControl(this.todo.texto, Validators.required);
    this.formNotes.controls['chkCompletado'].valueChanges.subscribe((val) => {
      this.store.dispatch(actions.toggle({ id: this.todo.id }));
    });
  }

  editar() {
    this.editando = true;
    this.formNotes.controls['txtEditable'].setValue(this.todo.texto);
    setTimeout(() => {
      this.txtInputEditable.nativeElement.select();
    }, 1);
  }

  terminarEdicion() {
    this.editando = false;
    if (
      this.formNotes.controls['txtEditable'].invalid ||
      this.formNotes.controls['txtEditable'].value === ''
    ) {
      return;
    }
    if (this.formNotes.controls['txtEditable'].value === this.todo.texto) {
      return;
    }

    this.store.dispatch(
      actions.editar({
        id: this.todo.id,
        texto: this.formNotes.controls['txtEditable'].value,
      })
    );
  }

  borrar() {
    this.store.dispatch(actions.borrar({ id: this.todo.id }));
  }
}
