import { CreditsResponse, Cast } from './../interfaces/credits-response';
import { tap, map, catchError } from 'rxjs/operators';
import { CarteleraResponse, Movie } from './../interfaces/cartelera-response';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MovieResponse } from '../interfaces/movie-response';

@Injectable({
  providedIn: 'root',
})
export class PeliculasService {
  private baseURL: string = 'https://api.themoviedb.org/3';
  private carteleraPage = 1;
  public cargando = false;
  constructor(private http: HttpClient) {}

  get params() {
    return {
      api_key: '918fcbe249f8d9700724ee29ea4dc7d7',
      language: 'es-ES',
      page: this.carteleraPage.toString(),
    };
  }

  resetCarteleraPage() {
    this.carteleraPage = 1;
  }

  // getMovies(): Observable<CarteleraResponse> {
  //   this.cargando = true;
  //   return this.http
  //     .get<CarteleraResponse>(`${this.baseURL}/movie/now_playing`, {
  //       params: this.params,
  //     })
  //     .pipe(
  //       tap(() => {
  //         this.carteleraPage += 1;
  //         this.cargando = false;
  //       })
  //     );
  // }

  getMovies(): Observable<Movie[]> {
    if (this.cargando) {
      return of([]);
    }
    this.cargando = true;
    return this.http
      .get<CarteleraResponse>(`${this.baseURL}/movie/now_playing`, {
        params: this.params,
      })
      .pipe(
        map((res) => res.results),
        tap(() => {
          this.carteleraPage += 1;
          this.cargando = false;
        })
      );
  }

  searchMovieByText(texto: string) {
    const params = { ...this.params, page: '1', query: texto };
    //console.log(params);
    return this.http
      .get<CarteleraResponse>(`${this.baseURL}/search/movie`, {
        params,
      })
      .pipe(map((response) => response.results));
  }

  getDetailsMovieById(id: string) {
    return this.http
      .get<MovieResponse>(`${this.baseURL}/movie/${id}`, {
        params: this.params,
      })
      .pipe(catchError((err) => of(null)));
  }

  getCreditsMovieById(id: string): Observable<Cast[]> {
    return this.http
      .get<CreditsResponse>(`${this.baseURL}/movie/${id}/credits`, {
        params: this.params,
      })
      .pipe(
        map((response) => response.cast),
        catchError((err) => of([]))
      );
  }
}
