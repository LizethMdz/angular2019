import { PeliculasService } from './../../services/peliculas.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/interfaces/cartelera-response';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  constructor(private aR: ActivatedRoute, private mS: PeliculasService) {}
  public moviesResults: Movie[] = [];
  public parametro: string = '';
  ngOnInit(): void {
    this.aR.params.subscribe((params) => {
      //console.log(params);
      // Llamar el servicio
      this.parametro = params.texto;
      this.mS
        .searchMovieByText(this.parametro)
        .subscribe((response) => (this.moviesResults = response));
    });
  }
}
