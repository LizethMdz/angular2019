import { combineLatest } from 'rxjs';
import { Cast } from './../../interfaces/credits-response';
import { MovieResponse } from './../../interfaces/movie-response';
import { PeliculasService } from './../../services/peliculas.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css'],
})
export class MovieComponent implements OnInit {
  public parametro: string = '';
  public pelicula: MovieResponse;
  public cast: Cast[] = [];
  constructor(
    private aR: ActivatedRoute,
    private mS: PeliculasService,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit(): void {
    const { id } = this.aR.snapshot.params;

    combineLatest([
      this.mS.getDetailsMovieById(id),
      this.mS.getCreditsMovieById(id),
    ]).subscribe(([pelicula, cast]) => {
      if (!pelicula) {
        this.router.navigateByUrl('/home');
        return;
      }
      this.pelicula = pelicula;

      this.cast = cast.filter((actor) => actor.profile_path !== null);
    });

    // this.mS.getDetailsMovieById(id).subscribe((movie) => {
    //   if (!movie) {
    //     this.router.navigateByUrl('/home');
    //     return;
    //   }
    //   this.pelicula = movie;
    // });

    // this.mS.getCreditsMovieById(id).subscribe((cast) => {
    //   //console.log(cast);
    //   this.cast = cast.filter((actor) => actor.profile_path !== null);
    // });
  }

  onRegresar() {
    this.location.back();
  }
}
