import { Movie } from './../../interfaces/cartelera-response';
import { Component, HostListener, OnInit, OnDestroy } from '@angular/core';
import { PeliculasService } from 'src/app/services/peliculas.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  public movies: Movie[] = [];
  public moviesSlideshow: Movie[] = [];
  @HostListener('window:scroll', ['$event'])
  onScroll() {
    const d = document;
    const pos =
      (d.documentElement.scrollTop || d.body.scrollTop) +
      d.documentElement.clientHeight;
    const max = d.documentElement.scrollHeight || d.body.scrollHeight;

    if (Math.ceil(pos) >= max) {
      if (this.mS.cargando) {
        return;
      }
      this.mS.getMovies().subscribe((res) => {
        this.movies.push(...res);
      });
    }
  }

  constructor(private mS: PeliculasService) {}

  ngOnInit(): void {
    this.mS.getMovies().subscribe((movies) => {
      //console.log(movies);
      this.movies = movies;
      this.moviesSlideshow = movies;
    });
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.mS.resetCarteleraPage();
  }
}
