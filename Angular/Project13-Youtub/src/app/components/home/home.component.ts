import { Component, OnInit } from '@angular/core';
import { YoutubeService } from '../../services/youtube.service';


declare var $:any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  _items: any[] = [];
  videoSel: any;

  constructor(private ys: YoutubeService) { 

  	// Peticion GET

  	this.ys.getVideos().subscribe(
  			(info) => {
  				console.log(info);
  				this._items = info;
  			}
  		);
  }

  ngOnInit() {
  }

  //

  ver_Video(video: any){
    this.videoSel = video;
    $('#myModal').modal();

  }

  cerrarModal(){
    this.videoSel = null;
    $('#myModal').modal('hide');
  }

  cargarMas(){
    this.ys.getVideos().subscribe(
        (newinfo) => {
          //console.log(info);
          // Agrega nuevos videos
          this._items.push.apply(this._items, newinfo);
        }
    );
  }

}
