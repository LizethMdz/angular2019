import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class YoutubeService{


	  private url = 'https://www.googleapis.com/youtube/v3/';
  	private apikey = 'AIzaSyAu5Dmb2zoF5yJgbq6XmXAafc9nF42SmmA';
  	private uploads = 'UU_aEa8K-EOJ3D6gOs7HcyNg';
    private nextPageToken = "";

  constructor(public http: HttpClient) {

  }

  getVideos(){

    // GET https://www.googleapis.com/youtube/v3/playlistItems

  	let url = `${this.url}playlistItems`;


    // params cards

    let params = new HttpParams();
    params = params.set('part', 'snippet');
    params = params.set('maxResults', '12');
    params = params.set('playlistId', this.uploads);
    params = params.set('key', this.apikey)

    
    // Lista mas elementos
    if( this.nextPageToken ){
      params = params.set('pageToken', this.nextPageToken);
    }

    console.log(params);

  	// return this.http.get(url, {params: params }).pipe(
   //    map( (resp) => {  
   //      console.log(resp);
       
   //    } ));

    return this.http.get(url,{params: params}).pipe(
      map( (resp) => {  
        console.log(resp);
        this.nextPageToken = resp['nextPageToken'];
        console.log(this.nextPageToken);

        //Items
        let Items: any[] = [];

        for (let video of resp['items']) {
          let snippet = video.snippet;
          Items.push(snippet);
        }

        return Items;

      } ));
  }

  ngOnInit() {
  }

}
