Información del proyecto

Nombre del proyecto
Youtube-Rest

ID de proyecto
youtube-rest-270219

Número del proyecto
778235229386


=======================================================

YOUTUB -API - INFORMATION


API - 10/03/20200

AIzaSyAu5Dmb2zoF5yJgbq6XmXAafc9nF42SmmA

It allows http conections from "localhost:4200"


=======================================================

Petición:

https://www.googleapis.com/youtube/v3/channels

=======================================================
Respuesta

{
  "kind": "youtube#channelListResponse",
  "etag": etag,
  "nextPageToken": string,
  "prevPageToken": string,
  "pageInfo": {
    "totalResults": integer,
    "resultsPerPage": integer
  },
  "items": [
    channel Resource
  ]
}


==========================================================
--------------------Channels: list-----------------------
==========================================================

Params:

part: contentDetails

id: UC_aEa8K-EOJ3D6gOs7HcyNg

==========================================================

Channel:

https://www.youtube.com/channel/UC_aEa8K-EOJ3D6gOs7HcyNg

=============================================================

Respuesta:

{
 "kind": "youtube#channelListResponse",
 "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/TmQRbzuuaVHXuhpfJrA2HCJ1UWA\"",
 "pageInfo": {
  "totalResults": 1,
  "resultsPerPage": 1
 },
 "items": [
  {
   "kind": "youtube#channel",
   "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/AxVkxv_SXQ6a6Bq_MewEqMXsPWg\"",
   "id": "UC_aEa8K-EOJ3D6gOs7HcyNg",
   "contentDetails": {
    "relatedPlaylists": {
     "uploads": "UU_aEa8K-EOJ3D6gOs7HcyNg",
     "watchHistory": "HL",
     "watchLater": "WL"
    }
   }
  }
 ]
}


=======================================================================================

https://www.youtube.com/watch?v=s-ZUU_cE_AQ&list=PLRBp0Fe2GpgmsW46rJyudVFlY6IYjFBIK

=======================================================================================


=============================================================
-----------------PlaylistItems: list-------------------------
=============================================================

Params:

part: snippet

playlistId: UU_aEa8K-EOJ3D6gOs7HcyNg

=============================================================

Peticion

GET https://www.googleapis.com/youtube/v3/playlistItems

=============================================================

Respuesta:

{
 "kind": "youtube#playlistItemListResponse",
 "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/c14ehx96Sm0ZysL_Mt5tVcTBMeg\"",
 "nextPageToken": "CAoQAA",
 "pageInfo": {
  "totalResults": 732,
  "resultsPerPage": 10
 },
 "items": [
  {
   "kind": "youtube#playlistItem",
   "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/yxmMEmOWlpBfxmspVzspDg0Fxvk\"",
   "id": "VVVfYUVhOEstRU9KM0Q2Z09zN0hjeU5nLkprRlVmamtuUTZR",
   "snippet": {
    "publishedAt": "2020-03-10T17:05:06.000Z",
    "channelId": "UC_aEa8K-EOJ3D6gOs7HcyNg",
    "title": "Mountkid - Jellyfish Party [NCS Release]",
    "description": "NCS: Music Without Limitations\nOur Spotify Playlist → http://spoti.fi/NCS\n\nFree Download / Stream: http://ncs.io/JellyfishParty\n\n[NCS]\n• http://soundcloud.com/NoCopyrightSounds\n• http://instagram.com/NoCopyrightSounds\n• http://facebook.com/NoCopyrightSounds\n• http://twitter.com/NCSounds\n\n[Mountkid]\n• https://facebook.com/mountkid\n• https://instagram.com/mountkid\n• https://soundcloud.com/mountkid\n\nBecome a SuperFan → http://ncs.io/SuperFan\nNCS Merchandise → http://ncs.io/StoreID\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\nWhen you are using this track, we simply ask that you put this in your description:\n\nTrack: Mountkid - Jellyfish Party [NCS Release]\nMusic provided by NoCopyrightSounds.\nWatch: https://youtu.be/JkFUfjknQ6Q\nFree Download / Stream: http://ncs.io/JellyfishParty\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\n©️ Check out our Usage Policy on how to use NCS music in your videos: http://ncs.io/UsagePolicy\n\nTo request a commercial license visit: http://ncs.io/Commercial\n\n#nocopyrightsounds #copyrightfree",
    "thumbnails": {
     "default": {
      "url": "https://i.ytimg.com/vi/JkFUfjknQ6Q/default.jpg",
      "width": 120,
      "height": 90
     },
     "medium": {
      "url": "https://i.ytimg.com/vi/JkFUfjknQ6Q/mqdefault.jpg",
      "width": 320,
      "height": 180
     },
     "high": {
      "url": "https://i.ytimg.com/vi/JkFUfjknQ6Q/hqdefault.jpg",
      "width": 480,
      "height": 360
     },
     "standard": {
      "url": "https://i.ytimg.com/vi/JkFUfjknQ6Q/sddefault.jpg",
      "width": 640,
      "height": 480
     },
     "maxres": {
      "url": "https://i.ytimg.com/vi/JkFUfjknQ6Q/maxresdefault.jpg",
      "width": 1280,
      "height": 720
     }
    },
    "channelTitle": "NoCopyrightSounds",
    "playlistId": "UU_aEa8K-EOJ3D6gOs7HcyNg",
    "position": 0,
    "resourceId": {
     "kind": "youtube#video",
     "videoId": "JkFUfjknQ6Q"
    }
   }
  },
  {
   "kind": "youtube#playlistItem",
   "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/F-pnp-yzCZDkTdCwq9G_jXMS0xw\"",
   "id": "VVVfYUVhOEstRU9KM0Q2Z09zN0hjeU5nLkhrYmpzYnd6aVpZ",
   "snippet": {
    "publishedAt": "2020-03-07T16:15:00.000Z",
    "channelId": "UC_aEa8K-EOJ3D6gOs7HcyNg",
    "title": "Dirty Palm - To The Back (feat. Purple Velvet Curtains) [NCS Release]",
    "description": "NCS: Music Without Limitations\nOur Spotify Playlist → http://spoti.fi/NCS\n\nFree Download / Stream: http://ncs.io/ToTheBack\n\n[NCS]\n• http://soundcloud.com/NoCopyrightSounds\n• http://instagram.com/NoCopyrightSounds\n• http://facebook.com/NoCopyrightSounds\n• http://twitter.com/NCSounds\n\n[Dirty Palm]\n• https://facebook.com/DirtyPalm\n• https://instagram.com/DirtyPalm\n• https://soundcloud.com/palmkillit\n\n[Purple Velvet Curtains]\n• https://facebook.com/pvcukmusic\n• https://instagram.com/pvcukmusic\n• https://soundcloud.com/pvcukmusic\n\nBecome a SuperFan → http://ncs.io/SuperFan\nNCS Merchandise → http://ncs.io/StoreID\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\nWhen you are using this track, we simply ask that you put this in your description:\n\nTrack: Dirty Palm - To The Back (feat. Purple Velvet Curtains)[NCS Release]\nMusic provided by NoCopyrightSounds.\nWatch: https://youtu.be/HkbjsbwziZY\nFree Download / Stream: http://ncs.io/ToTheBack\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\n©️ Check out our Usage Policy on how to use NCS music in your videos: http://ncs.io/UsagePolicy\n\nTo request a commercial license visit: http://ncs.io/Commercial\n\n#nocopyrightsounds #copyrightfree",
    "thumbnails": {
     "default": {
      "url": "https://i.ytimg.com/vi/HkbjsbwziZY/default.jpg",
      "width": 120,
      "height": 90
     },
     "medium": {
      "url": "https://i.ytimg.com/vi/HkbjsbwziZY/mqdefault.jpg",
      "width": 320,
      "height": 180
     },
     "high": {
      "url": "https://i.ytimg.com/vi/HkbjsbwziZY/hqdefault.jpg",
      "width": 480,
      "height": 360
     },
     "standard": {
      "url": "https://i.ytimg.com/vi/HkbjsbwziZY/sddefault.jpg",
      "width": 640,
      "height": 480
     },
     "maxres": {
      "url": "https://i.ytimg.com/vi/HkbjsbwziZY/maxresdefault.jpg",
      "width": 1280,
      "height": 720
     }
    },
    "channelTitle": "NoCopyrightSounds",
    "playlistId": "UU_aEa8K-EOJ3D6gOs7HcyNg",
    "position": 1,
    "resourceId": {
     "kind": "youtube#video",
     "videoId": "HkbjsbwziZY"
    }
   }
  },
  {
   "kind": "youtube#playlistItem",
   "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/xZpO2qd91iNv9nDpefwQpM30fWI\"",
   "id": "VVVfYUVhOEstRU9KM0Q2Z09zN0hjeU5nLnk5aHNIeV9zZW1z",
   "snippet": {
    "publishedAt": "2020-03-05T17:03:46.000Z",
    "channelId": "UC_aEa8K-EOJ3D6gOs7HcyNg",
    "title": "Mendum - You (feat. Brenton Mattheus) [NCS Release]",
    "description": "NCS: Music Without Limitations\nOur Spotify Playlist → http://spoti.fi/NCS\n\nFree Download / Stream: http://ncs.io/YouM\n\n[NCS]\n• http://soundcloud.com/NoCopyrightSounds\n• http://instagram.com/NoCopyrightSounds\n• http://facebook.com/NoCopyrightSounds\n• http://twitter.com/NCSounds\n\n[Mendum]\n• https://facebook.com/mendumofficial\n• https://instagram.com/mendumofficial\n• https://soundcloud.com/mendumofficial\n\n[Brenton Mattheus]\n• https://instagram.com/brentonmattheusmusic\n• https://facebook.com/BrentonMattheus\n\nBecome a SuperFan → http://ncs.io/SuperFan\nNCS Merchandise → http://ncs.io/StoreID\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\nWhen you are using this track, we simply ask that you put this in your description:\n\nTrack: Mendum - You (feat. Brenton Mattheus) [NCS Release]\nMusic provided by NoCopyrightSounds.\nWatch: https://youtu.be/y9hsHy_sems\nFree Download / Stream: http://ncs.io/YouM\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\n©️ Check out our Usage Policy on how to use NCS music in your videos: http://ncs.io/UsagePolicy\n\nTo request a commercial license visit: http://ncs.io/Commercial\n\n#nocopyrightsounds #copyrightfree",
    "thumbnails": {
     "default": {
      "url": "https://i.ytimg.com/vi/y9hsHy_sems/default.jpg",
      "width": 120,
      "height": 90
     },
     "medium": {
      "url": "https://i.ytimg.com/vi/y9hsHy_sems/mqdefault.jpg",
      "width": 320,
      "height": 180
     },
     "high": {
      "url": "https://i.ytimg.com/vi/y9hsHy_sems/hqdefault.jpg",
      "width": 480,
      "height": 360
     },
     "standard": {
      "url": "https://i.ytimg.com/vi/y9hsHy_sems/sddefault.jpg",
      "width": 640,
      "height": 480
     },
     "maxres": {
      "url": "https://i.ytimg.com/vi/y9hsHy_sems/maxresdefault.jpg",
      "width": 1280,
      "height": 720
     }
    },
    "channelTitle": "NoCopyrightSounds",
    "playlistId": "UU_aEa8K-EOJ3D6gOs7HcyNg",
    "position": 2,
    "resourceId": {
     "kind": "youtube#video",
     "videoId": "y9hsHy_sems"
    }
   }
  },
  {
   "kind": "youtube#playlistItem",
   "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/8YfegGj--hjdmS-EbbahqcfihrQ\"",
   "id": "VVVfYUVhOEstRU9KM0Q2Z09zN0hjeU5nLi1yQUkwTmQ5eEt3",
   "snippet": {
    "publishedAt": "2020-03-03T17:03:39.000Z",
    "channelId": "UC_aEa8K-EOJ3D6gOs7HcyNg",
    "title": "MIDNIGHT CVLT & Titus1 - Better Days [NCS Release]",
    "description": "NCS: Music Without Limitations\nOur Spotify Playlist → http://spoti.fi/NCS\n\nFree Download / Stream: http://ncs.io/BetterDays\n\n[NCS]\n• http://soundcloud.com/NoCopyrightSounds\n• http://instagram.com/NoCopyrightSounds\n• http://facebook.com/NoCopyrightSounds\n• http://twitter.com/NCSounds\n\n[MIDNIGHT CLVT]\n• https://facebook.com/midnightcvlt\n• https://instagram.com/midnightcvlt\n• https://soundcloud.com/midnightcvlt\n\n[Titus1]\n• https://facebook.com/Titus1.Official\n• https://instagram.com/dj.titus1\n• https://soundcloud.com/titus1\n\nBecome a SuperFan → http://ncs.io/SuperFan\nNCS Merchandise → http://ncs.io/StoreID\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\nWhen you are using this track, we simply ask that you put this in your description:\n\nTrack: MIDNIGHT CVLT & Titus1 - Better Days [NCS Release]\nMusic provided by NoCopyrightSounds.\nWatch: https://youtu.be/-rAI0Nd9xKw\nFree Download / Stream: http://ncs.io/BetterDays\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\n©️ Check out our Usage Policy on how to use NCS music in your videos: http://ncs.io/UsagePolicy\n\nTo request a commercial license visit: http://ncs.io/Commercial\n\n#nocopyrightsounds #copyrightfree",
    "thumbnails": {
     "default": {
      "url": "https://i.ytimg.com/vi/-rAI0Nd9xKw/default.jpg",
      "width": 120,
      "height": 90
     },
     "medium": {
      "url": "https://i.ytimg.com/vi/-rAI0Nd9xKw/mqdefault.jpg",
      "width": 320,
      "height": 180
     },
     "high": {
      "url": "https://i.ytimg.com/vi/-rAI0Nd9xKw/hqdefault.jpg",
      "width": 480,
      "height": 360
     },
     "standard": {
      "url": "https://i.ytimg.com/vi/-rAI0Nd9xKw/sddefault.jpg",
      "width": 640,
      "height": 480
     },
     "maxres": {
      "url": "https://i.ytimg.com/vi/-rAI0Nd9xKw/maxresdefault.jpg",
      "width": 1280,
      "height": 720
     }
    },
    "channelTitle": "NoCopyrightSounds",
    "playlistId": "UU_aEa8K-EOJ3D6gOs7HcyNg",
    "position": 3,
    "resourceId": {
     "kind": "youtube#video",
     "videoId": "-rAI0Nd9xKw"
    }
   }
  },
  {
   "kind": "youtube#playlistItem",
   "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/zfrSXbltCV4nWjcYvqbhNv8TK4I\"",
   "id": "VVVfYUVhOEstRU9KM0Q2Z09zN0hjeU5nLlY0dDMzbTcweExR",
   "snippet": {
    "publishedAt": "2020-02-29T16:15:00.000Z",
    "channelId": "UC_aEa8K-EOJ3D6gOs7HcyNg",
    "title": "More Plastic - Champion [NCS Release]",
    "description": "NCS: Music Without Limitations\nOur Spotify Playlist → http://spoti.fi/NCS\n\nFree Download / Stream: http://ncs.io/Champion\n\n[NCS]\n• http://soundcloud.com/NoCopyrightSounds\n• http://instagram.com/NoCopyrightSounds\n• http://facebook.com/NoCopyrightSounds\n• http://twitter.com/NCSounds\n\n[More Plastic]\n• https://instagram.com/MorePlastic\n• https://soundcloud.com/MorePlastic\n• https://facebook.com/MorePlasticOfficial\n\nBecome a SuperFan → http://ncs.io/SuperFan\nNCS Merchandise → http://ncs.io/StoreID\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\nWhen you are using this track, we simply ask that you put this in your description:\n\nTrack: More Plastic - Champion [NCS Release]\nMusic provided by NoCopyrightSounds.\nWatch: https://youtu.be/V4t33m70xLQ\nFree Download / Stream: http://ncs.io/Champion\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\n©️ Check out our Usage Policy on how to use NCS music in your videos: http://ncs.io/UsagePolicy\n\nTo request a commercial license visit: http://ncs.io/Commercial\n\n#nocopyrightsounds #copyrightfree",
    "thumbnails": {
     "default": {
      "url": "https://i.ytimg.com/vi/V4t33m70xLQ/default.jpg",
      "width": 120,
      "height": 90
     },
     "medium": {
      "url": "https://i.ytimg.com/vi/V4t33m70xLQ/mqdefault.jpg",
      "width": 320,
      "height": 180
     },
     "high": {
      "url": "https://i.ytimg.com/vi/V4t33m70xLQ/hqdefault.jpg",
      "width": 480,
      "height": 360
     },
     "standard": {
      "url": "https://i.ytimg.com/vi/V4t33m70xLQ/sddefault.jpg",
      "width": 640,
      "height": 480
     }
    },
    "channelTitle": "NoCopyrightSounds",
    "playlistId": "UU_aEa8K-EOJ3D6gOs7HcyNg",
    "position": 4,
    "resourceId": {
     "kind": "youtube#video",
     "videoId": "V4t33m70xLQ"
    }
   }
  },
  {
   "kind": "youtube#playlistItem",
   "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/eWRRu0nWvIcgLITKKlU-s7m8SLk\"",
   "id": "VVVfYUVhOEstRU9KM0Q2Z09zN0hjeU5nLmwzcXZaYkxsd0pr",
   "snippet": {
    "publishedAt": "2020-02-27T17:04:11.000Z",
    "channelId": "UC_aEa8K-EOJ3D6gOs7HcyNg",
    "title": "Fareoh - Illuminati [NCS Release]",
    "description": "NCS: Music Without Limitations\nOur Spotify Playlist → http://spoti.fi/NCS\n\nFree Download / Stream: http://ncs.io/Illuminati\n\n[NCS]\n• http://soundcloud.com/NoCopyrightSounds\n• http://instagram.com/NoCopyrightSounds\n• http://facebook.com/NoCopyrightSounds\n• http://twitter.com/NCSounds\n\n[Fareoh]\n• https://twitter.com/thefareoh\n• https://instagram.com/fareoh\n• https://soundcloud.com/fareoh\n\nBecome a SuperFan → http://ncs.io/SuperFan\nNCS Merchandise → http://ncs.io/StoreID\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\nWhen you are using this track, we simply ask that you put this in your description:\n\nTrack: Fareoh - Illuminati [NCS Release]\nMusic provided by NoCopyrightSounds.\nWatch: https://youtu.be/l3qvZbLlwJk\nFree Download / Stream: http://ncs.io/Illuminati\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\n©️ Check out our Usage Policy on how to use NCS music in your videos: http://ncs.io/UsagePolicy\n\nTo request a commercial license visit: http://ncs.io/Commercial\n\n#nocopyrightsounds #copyrightfree",
    "thumbnails": {
     "default": {
      "url": "https://i.ytimg.com/vi/l3qvZbLlwJk/default.jpg",
      "width": 120,
      "height": 90
     },
     "medium": {
      "url": "https://i.ytimg.com/vi/l3qvZbLlwJk/mqdefault.jpg",
      "width": 320,
      "height": 180
     },
     "high": {
      "url": "https://i.ytimg.com/vi/l3qvZbLlwJk/hqdefault.jpg",
      "width": 480,
      "height": 360
     },
     "standard": {
      "url": "https://i.ytimg.com/vi/l3qvZbLlwJk/sddefault.jpg",
      "width": 640,
      "height": 480
     },
     "maxres": {
      "url": "https://i.ytimg.com/vi/l3qvZbLlwJk/maxresdefault.jpg",
      "width": 1280,
      "height": 720
     }
    },
    "channelTitle": "NoCopyrightSounds",
    "playlistId": "UU_aEa8K-EOJ3D6gOs7HcyNg",
    "position": 5,
    "resourceId": {
     "kind": "youtube#video",
     "videoId": "l3qvZbLlwJk"
    }
   }
  },
  {
   "kind": "youtube#playlistItem",
   "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/Ec6MHYGxzJeUZu1CGFGQfzxAM2Y\"",
   "id": "VVVfYUVhOEstRU9KM0Q2Z09zN0hjeU5nLkQwVHFrcm5td2M4",
   "snippet": {
    "publishedAt": "2020-02-25T17:15:00.000Z",
    "channelId": "UC_aEa8K-EOJ3D6gOs7HcyNg",
    "title": "Aeden & Joellé - I Feel Crazy [NCS Release]",
    "description": "NCS: Music Without Limitations\nOur Spotify Playlist → http://spoti.fi/NCS\n\nFree Download / Stream: http://ncs.io/IFeelCrazy\n\n[NCS]\n• http://soundcloud.com/NoCopyrightSounds\n• http://instagram.com/NoCopyrightSounds\n• http://facebook.com/NoCopyrightSounds\n• http://twitter.com/NCSounds\n\n[Aeden]\n• https://facebook.com/aedenofficial\n• https://instagram.com/aedenofficial\n• https://soundcloud.com/aedenofficial\n\n[Joellé]\n• https://instagram.com/joelle.music\n• https://soundcloud.com//joelle-official\n\nBecome a SuperFan → http://ncs.io/SuperFan\nNCS Merchandise → http://ncs.io/StoreID\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\nWhen you are using this track, we simply ask that you put this in your description:\n\nTrack: Aeden & Joellé - I Feel Crazy [NCS Release]\nMusic provided by NoCopyrightSounds.\nWatch: https://youtu.be/D0Tqkrnmwc8\nFree Download / Stream: http://ncs.io/IFeelCrazy\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\n©️ Check out our Usage Policy on how to use NCS music in your videos: http://ncs.io/UsagePolicy\n\nTo request a commercial license visit: http://ncs.io/Commercial\n\n#nocopyrightsounds #copyrightfree",
    "thumbnails": {
     "default": {
      "url": "https://i.ytimg.com/vi/D0Tqkrnmwc8/default.jpg",
      "width": 120,
      "height": 90
     },
     "medium": {
      "url": "https://i.ytimg.com/vi/D0Tqkrnmwc8/mqdefault.jpg",
      "width": 320,
      "height": 180
     },
     "high": {
      "url": "https://i.ytimg.com/vi/D0Tqkrnmwc8/hqdefault.jpg",
      "width": 480,
      "height": 360
     },
     "standard": {
      "url": "https://i.ytimg.com/vi/D0Tqkrnmwc8/sddefault.jpg",
      "width": 640,
      "height": 480
     },
     "maxres": {
      "url": "https://i.ytimg.com/vi/D0Tqkrnmwc8/maxresdefault.jpg",
      "width": 1280,
      "height": 720
     }
    },
    "channelTitle": "NoCopyrightSounds",
    "playlistId": "UU_aEa8K-EOJ3D6gOs7HcyNg",
    "position": 6,
    "resourceId": {
     "kind": "youtube#video",
     "videoId": "D0Tqkrnmwc8"
    }
   }
  },
  {
   "kind": "youtube#playlistItem",
   "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/-Pj9SI4fAieUrD6YqsewmMcTpSk\"",
   "id": "VVVfYUVhOEstRU9KM0Q2Z09zN0hjeU5nLm84WV91UjZaRHhv",
   "snippet": {
    "publishedAt": "2020-02-22T16:15:01.000Z",
    "channelId": "UC_aEa8K-EOJ3D6gOs7HcyNg",
    "title": "ROY KNOX x WTCHOUT - Shadows (Feat. Svniivan) [NCS Release]",
    "description": "NCS: Music Without Limitations\nOur Spotify Playlist → http://spoti.fi/NCS\n\nFree Download / Stream: http://ncs.io/RWShadows\n\n[NCS]\n• http://soundcloud.com/NoCopyrightSounds\n• http://instagram.com/NoCopyrightSounds\n• http://facebook.com/NoCopyrightSounds\n• http://twitter.com/NCSounds\n\n[ROY KNOX]\n• https://facebook.com/RoyKnoxMusic\n• https://instagram.com/RoyKnoxMusic\n• https://soundcloud.com/RoyKnoxMusic\n\n[WTCHOUT]\n• https://facebook.com/wtchoutofficial\n• https://instagram.com/wtchoutmusic\n• https://soundcloud.com/wtchoutofficial\n\n[Svniivan]\n• https://facebook.com/Svniivan\n• https://instagram.com/svniivan\n• https://soundcloud.com/svniivan\n\nBecome a SuperFan → http://ncs.io/SuperFan\nNCS Merchandise → http://ncs.io/StoreID\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\nWhen you are using this track, we simply ask that you put this in your description:\n\nTrack: ROY KNOX x WTCHOUT - Shadows (Feat. Sviivan) [NCS Release]\nMusic provided by NoCopyrightSounds.\nWatch: https://youtu.be/o8Y_uR6ZDxo\nFree Download / Stream: http://ncs.io/RWShadows\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\n©️ Check out our Usage Policy on how to use NCS music in your videos: http://ncs.io/UsagePolicy\n\nTo request a commercial license visit: http://ncs.io/Commercial\n\n#nocopyrightsounds #copyrightfree",
    "thumbnails": {
     "default": {
      "url": "https://i.ytimg.com/vi/o8Y_uR6ZDxo/default.jpg",
      "width": 120,
      "height": 90
     },
     "medium": {
      "url": "https://i.ytimg.com/vi/o8Y_uR6ZDxo/mqdefault.jpg",
      "width": 320,
      "height": 180
     },
     "high": {
      "url": "https://i.ytimg.com/vi/o8Y_uR6ZDxo/hqdefault.jpg",
      "width": 480,
      "height": 360
     },
     "standard": {
      "url": "https://i.ytimg.com/vi/o8Y_uR6ZDxo/sddefault.jpg",
      "width": 640,
      "height": 480
     },
     "maxres": {
      "url": "https://i.ytimg.com/vi/o8Y_uR6ZDxo/maxresdefault.jpg",
      "width": 1280,
      "height": 720
     }
    },
    "channelTitle": "NoCopyrightSounds",
    "playlistId": "UU_aEa8K-EOJ3D6gOs7HcyNg",
    "position": 7,
    "resourceId": {
     "kind": "youtube#video",
     "videoId": "o8Y_uR6ZDxo"
    }
   }
  },
  {
   "kind": "youtube#playlistItem",
   "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/h2_wCnYIKzuGLF-GRPq2X9LfFJM\"",
   "id": "VVVfYUVhOEstRU9KM0Q2Z09zN0hjeU5nLlhONEMxVnM2cWdz",
   "snippet": {
    "publishedAt": "2020-02-20T17:07:32.000Z",
    "channelId": "UC_aEa8K-EOJ3D6gOs7HcyNg",
    "title": "Max Brhon - Illusion [NCS Release]",
    "description": "NCS: Music Without Limitations\nOur Spotify Playlist → http://spoti.fi/NCS\n\nFree Download / Stream: http://ncs.io/Illusion\n\n[NCS]\n• http://soundcloud.com/NoCopyrightSounds\n• http://instagram.com/NoCopyrightSounds\n• http://facebook.com/NoCopyrightSounds\n• http://twitter.com/NCSounds\n\n[Max Brhon]\n• https://facebook.com/MaxBrhon\n• https://instagram.com/Max.Brhon\n• https://soundcloud.com/music-maxb\n\nBecome a SuperFan → http://ncs.io/SuperFan\nNCS Merchandise → http://ncs.io/StoreID\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\nWhen you are using this track, we simply ask that you put this in your description:\n\nTrack: Max Brhon - Illusion [NCS Release]\nMusic provided by NoCopyrightSounds.\nWatch: https://youtu.be/XN4C1Vs6qgs\nFree Download / Stream: http://ncs.io/Ilussion\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\n©️ Check out our Usage Policy on how to use NCS music in your videos: http://ncs.io/UsagePolicy\n\nTo request a commercial license visit: http://ncs.io/Commercial\n\n#nocopyrightsounds #copyrightfree",
    "thumbnails": {
     "default": {
      "url": "https://i.ytimg.com/vi/XN4C1Vs6qgs/default.jpg",
      "width": 120,
      "height": 90
     },
     "medium": {
      "url": "https://i.ytimg.com/vi/XN4C1Vs6qgs/mqdefault.jpg",
      "width": 320,
      "height": 180
     },
     "high": {
      "url": "https://i.ytimg.com/vi/XN4C1Vs6qgs/hqdefault.jpg",
      "width": 480,
      "height": 360
     },
     "standard": {
      "url": "https://i.ytimg.com/vi/XN4C1Vs6qgs/sddefault.jpg",
      "width": 640,
      "height": 480
     },
     "maxres": {
      "url": "https://i.ytimg.com/vi/XN4C1Vs6qgs/maxresdefault.jpg",
      "width": 1280,
      "height": 720
     }
    },
    "channelTitle": "NoCopyrightSounds",
    "playlistId": "UU_aEa8K-EOJ3D6gOs7HcyNg",
    "position": 8,
    "resourceId": {
     "kind": "youtube#video",
     "videoId": "XN4C1Vs6qgs"
    }
   }
  },
  {
   "kind": "youtube#playlistItem",
   "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/lCBH-YKcVnHDTECX9FQDbsWBcxI\"",
   "id": "VVVfYUVhOEstRU9KM0Q2Z09zN0hjeU5nLnFRUTU0UzQ4MkZZ",
   "snippet": {
    "publishedAt": "2020-02-18T17:04:21.000Z",
    "channelId": "UC_aEa8K-EOJ3D6gOs7HcyNg",
    "title": "Olly Walker - Sorry (I Had To Leave) [NCS Release]",
    "description": "NCS: Music Without Limitations\nOur Spotify Playlist → http://spoti.fi/NCS\n\nFree Download / Stream: http://ncs.io/Sorry\n\n[NCS]\n• http://soundcloud.com/NoCopyrightSounds\n• http://instagram.com/NoCopyrightSounds\n• http://facebook.com/NoCopyrightSounds\n• http://twitter.com/NCSounds\n\n[Olly Walker]\n• https://facebook.com/theollywalker\n• https://instagram.com/theollywalker\n• https://soundcloud.com/theollywalker\n\nBecome a SuperFan → http://ncs.io/SuperFan\nNCS Merchandise → http://ncs.io/StoreID\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\nWhen you are using this track, we simply ask that you put this in your description:\n\nTrack: Olly Walker - Sorry (I Had To Leave) [NCS Release]\nMusic provided by NoCopyrightSounds.\nWatch: https://youtu.be/qQQ54S482FY\nFree Download / Stream: http://ncs.io/Sorry\n\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\n©️ Check out our Usage Policy on how to use NCS music in your videos: http://ncs.io/UsagePolicy\n\nTo request a commercial license visit: http://ncs.io/Commercial\n\n#nocopyrightsounds #copyrightfree",
    "thumbnails": {
     "default": {
      "url": "https://i.ytimg.com/vi/qQQ54S482FY/default.jpg",
      "width": 120,
      "height": 90
     },
     "medium": {
      "url": "https://i.ytimg.com/vi/qQQ54S482FY/mqdefault.jpg",
      "width": 320,
      "height": 180
     },
     "high": {
      "url": "https://i.ytimg.com/vi/qQQ54S482FY/hqdefault.jpg",
      "width": 480,
      "height": 360
     },
     "standard": {
      "url": "https://i.ytimg.com/vi/qQQ54S482FY/sddefault.jpg",
      "width": 640,
      "height": 480
     }
    },
    "channelTitle": "NoCopyrightSounds",
    "playlistId": "UU_aEa8K-EOJ3D6gOs7HcyNg",
    "position": 9,
    "resourceId": {
     "kind": "youtube#video",
     "videoId": "qQQ54S482FY"
    }
   }
  }
 ]
}


=================================================================================================================================