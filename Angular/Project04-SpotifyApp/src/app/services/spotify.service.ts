import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) {
    console.log('Servicio SpotifyApp Listo');
  }

  // RECARGAR EL TOKEN DE SPOTIFY EN POSTMAN
  getQuery(query: string) {
    // DE ESTA MANERA QUEDA CENTRALIZADO EL TEMA DE LOS QUERIES
    const url = `https://api.spotify.com/v1/${query}`;
    const token = 'BQAwA4N7fhrC7fzr_wlqL4W10EQ4RumekhWm8GrxwT853A63oQ0nqdM4P0sspB5WLFg-v3AXvtR1Kb2PxvE';
    const headers =  new HttpHeaders({

      'Authorization': `Bearer ${token}`,
    });

    return this.http.get(url, { headers });
  }

  getNewReleases() {

    // Generacion de credenciales para poder usar la API
    // const headers =  new HttpHeaders({
    //   'Authorization': 'Bearer BQA_uebrmc4omLu7IkFoKLQkQ9eFgyMTdugmlv6sJ3_OGERO2F3_U2wdGL_92jH4SCPiamoxDr2nLRMkcRA',
    // })
    // NOTE Peticion a la API de SpotifyService
    return this.getQuery('browse/new-releases')
      .pipe(map(data => data['albums'].items ));

    // NOTE Si quisieramos poder obtener la data del servicio
    // .subscribe(data => {
    //   console.log(data);
    // })
  }
  getArtist(termino: string) {
    // const headers =  new HttpHeaders({
    //   'Authorization': 'Bearer BQA_uebrmc4omLu7IkFoKLQkQ9eFgyMTdugmlv6sJ3_OGERO2F3_U2wdGL_92jH4SCPiamoxDr2nLRMkcRA',
    // });
    // NOTE Peticion a la API de SpotifyService
    // ANCHOR Se empleará una funcionalidad .map para filtrar unicamente
    // lo que se necesita.
    return this.getQuery(`search?query=${termino}&type=artist&offset=0&limit=15`)
    .pipe(map(data => data['artists'].items));
  }

  getDetailArtist(id: string){
    return this.getQuery(`artists/${id}`);
  }

  getTopTracks(id: string){
    return this.getQuery(`artists/${id}/top-tracks?country=us`)
      .pipe(map(data => data['tracks']));
  }
}
