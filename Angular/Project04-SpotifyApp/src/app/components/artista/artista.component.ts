import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: []
})
export class ArtistaComponent implements OnInit {

  detailArtist: any = {};
  tracksTop: any[] = [];
  loading: boolean;

  constructor(private router: ActivatedRoute, private spotify: SpotifyService ) {
    this.router.params.subscribe(params => {
     this.getArtistaDetalle(params['id']);
     this.getTopCanciones(params['id']);
    })
  }

  getArtistaDetalle(id: string){
      this.loading = true;
      this.spotify.getDetailArtist(id)
      .subscribe( (artista: any) => {
        // LA DATA ES ASIGNADA A "detailArtist"
        this.detailArtist = artista;
        this.loading = false;
        // console.log(this.detailArtist);

      })
  }

  getTopCanciones(id: string){
    this.spotify.getTopTracks(id)
    .subscribe( (canciones: any) => {
      this.tracksTop = canciones;
      console.log(this.tracksTop);
    } )

  }

  ngOnInit() {
  }

}
