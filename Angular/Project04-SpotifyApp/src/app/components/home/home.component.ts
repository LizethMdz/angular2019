import { Component } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';
// NOTE Import para hacer peticiones HTTP
// import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent {

  // NOTE - ELABORADO PARA ENTENDER COMO EXTRAER DATA DE UNA URL
  // paises: any[] = [];
  // constructor(private http: HttpClient) {
  //   this.http.get('https://restcountries.eu/rest/v2/lang/es')
  //     .subscribe((data: any ) => {
  //       this.paises = data;
  //       console.log(data);
  //   })
  //  }
  loading: boolean;
  nuevasCanciones: any[] = [];
  error: boolean;
  msmerror: string;


  constructor(private spotify: SpotifyService) {
    this.loading = true;
    this.error = false;

    this.spotify.getNewReleases()
    .subscribe((data: any) => {
      console.log(data);
      this.nuevasCanciones = data;
      this.loading = false;
    }, (errorService) => {
      this.error = true;
      this.loading = false;
      this.msmerror = errorService.error.error.message;
      console.log(errorService);
    })
  }

  ngOnInit() {
  }

}
