import { Component } from '@angular/core';
// import { NgForm } from '@angular/forms';
import {FormGroup, FormControl, Validators, FormArray} from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styles: []
})
export class DataComponent{
  formData: FormGroup;

  usuario: object = {
    nombrecompleto: {
      nombre: "yuuki",
      apellido: "haruno"
    },
    correo: 'yharuno@gmail.com',
pasatiempos: ['correr', 'mirar tv', 'leer']
  }

  constructor() {


    this.formData = new FormGroup({
      // Valor por defecto
      // Se agrega la propieedad Validators para hacer las validaciones

      // Agregar el objeto manualmente
      // colocar this.usuario['nombrecompleto'].nombre
      'nombrecompleto': new FormGroup(
        {
          //Elementos internos
          'nombre': new FormControl( '', [
            Validators.required,
            Validators.minLength(3)
          ]),
          'apellido': new FormControl('', [Validators.required,
                                          this.noApellido]),
        }
      ),

      'correo': new FormControl('', [
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      'pasatiempos' : new FormArray([
          new FormControl('correr', Validators.required)
      ]),
      'password1' : new FormControl('', Validators.required),
      'password2' : new FormControl('', Validators.required),
      'username' : new FormControl('', Validators.required, this.existeUsuario)

    });
    // NOTE De esta manera tambien podemos reemplazar los datos
    // cargados a traves de un objeeto con la misma estructura.
    // Sin embargo vuelve a recargar la data una vez enviado

    // this.formData.setValue(this.usuario);

    // console.log(this.formData);
    // console.log(this.formData.controls.pasatiempos);
    // bind() - establecemos que "this" sea "this.formData"
    // tslint:disable-next-line: no-string-literal
    this.formData.controls['password2'].setValidators([
      Validators.required,
      this.noIgual.bind(this.formData)
    ]);

    // De esta manera imprimimos cada cambio en los inputs
    /** this.formData.valueChanges.subscribe(
      (data) => {2
        console.log(data);
      }
    )**/

    // Aqui se especifica en que "controls" debera observar
    this.formData.controls.username.valueChanges.subscribe(
      (data) => {
        console.log(data);
      }
    )

    // Tambien se puede acceder al statusChanges, este solo mustra
    // si el valido
    this.formData.controls['username'].statusChanges.subscribe(
      (data) => {
        console.log(data);
      }
    )
  }

  save() {
    console.log(this.formData.value);
    console.log(this.formData);

    // Con esta funcion limpiamos la data precargada y enviada
    // this.formData.reset({
    //   nombrecompleto: {
    //     nombre: "",
    //     apellido: ""
    //   },
    //   correo: "",
    //   password1: "",
    //   password2: "",
    //   username: ""
    // });


  }



  agregarPasatiempo(){
    console.log('Agregar pasatiempo');
    // De esta manera le decimos que es un arreglo, por lo tanto
    // podremos acceder a las propiedades de un arreglo
    // Agrega un elemento ddentro del arreglo "pasatiempos"
    (<FormArray>this.formData.controls['pasatiempos']).push(
      new FormControl('Dormir', Validators.required)
    )
  }


  noApellido(controlForm: FormControl): { [s:string]: boolean } {
    if (controlForm.value === 'example') {
      // Si regresa true es que hay algo mal
      return {
        noApellido: true
      }
    }
    // Si no es que esta bien
    console.log('no fallo');
    return null;

  }

  noIgual(controlForm: FormControl): { [s:string]: boolean } {
    // console.log(this);
    let formLocal: any = this;

    if (controlForm.value === formLocal.controls['password2'].value) {
      // Si regresa true es que hay algo mal
      return {
        noIgual: true
      }
    }
    // Si no es que esta bien
    return null;

  }

  // function que se ejecuta cuando se valiaa si
  // el usuario es valido
  existeUsuario(controlForm: FormControl): Promise<any>|Observable<any> {
    let promesa = new Promise(
      (resolve, reject) => {
        setTimeout( () => {
          if(controlForm.value === 'till'){
            resolve( {existe: true});
          } else {
            resolve(null);
          }
        }, 3000);
      }
    );
    return promesa;
  }


}
