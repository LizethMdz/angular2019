import { PaisService } from './../../services/pais.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styles: [`
      .ng-invalid.ng-touched:not(form)
      {
        border: 1px solid red;
      }

      .ng-valid:not(form)
      {
        border: 1px solid green;
      }
    `]
})
export class TemplateComponent implements OnInit {

  paises = [{
      codigo: 'mex',
      pais: 'mexico'
    },
    {
      codigo: 'col',
      pais: 'colombia'
    }
  ];

  places = ['Niagara Waterfall', 'Egipt Pirams', 'Alphest', 'Everest', 'North Pole'];

  colors = ['Red', 'Yellow', 'Orange', 'Blue', 'Black', 'Pink', 'green'];

  // tslint:disable-next-line: ban-types
  usuario: object = {
    nombre: null,
    apellido: null,
    email: null,
    pais: '',
    lugares: 'Alphes',
    genero: '',
    colores: 'Blue',
    acepta: false
  };
 /** Nuevas actualizaciones */
  countries: any[] = [];

  constructor(private pService: PaisService) {
  }

  ngOnInit() {
    this.pService.getPaises().subscribe(
      paises => {
        this.countries = paises;
        /** Para que mantenga un elemento seleccionado */
        this.countries.unshift({
          nombre: 'SELECCIONE PAIS',
          codigo: ''
        });
        console.log(this.countries);
      }
    );
  }

  guardar(form: NgForm) {
    /** Otra forma de validar, pero no se ejecuta poruqe exite otra validacion
     *  que es la que habilita el boton submit.
     */
    // if (form.invalid) {
    //   Object.values(form.controls).forEach( control => {
    //     control.markAsTouched();
    //   });
    //   return;
    // }
    // console.log(form);
    // console.log(form.value);

    console.log('Guardado!');
    console.log(form);
    console.log(form.value);
    console.log(this.usuario);

  }


}
