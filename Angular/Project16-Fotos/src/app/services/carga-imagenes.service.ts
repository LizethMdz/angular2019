import { Injectable } from '@angular/core';
/** NOTE FIREBASE */
import { AngularFirestore } from 'angularfire2/firestore';
import { FileItem } from '../models/file-item.model';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class CargaImagenesService {
  private CARPETA_IMAGENES = 'img';
  urlFinal: string;

  constructor(private db: AngularFirestore) {
   }

   loadImgFirebase(imagenes: FileItem[]) {
     //console.log(imagenes);

     const storageRef = firebase.storage().ref();

     for (const item of imagenes ) {
       item.subiendo = true;
       if ( item.progreso >= 100 ) {
         continue;
       }
        // firebase.storage.UploadTask
       const uploadTask: firebase.storage.UploadTask = storageRef.child(`${this.CARPETA_IMAGENES }/${item.nombreArchivo}`)
       .put(item.archivo);

       uploadTask.on( firebase.storage.TaskEvent.STATE_CHANGED,
        // (snapshot: firebase.storage.UploadTaskSnapshot) => item.progreso = (snapshot.bytesTransferred / snapshot.totalBytes) * 100,
        (snapshot: firebase.storage.UploadTaskSnapshot) => item.progreso = (snapshot.bytesTransferred / snapshot.totalBytes) * 100,
        (error) => console.log('Error al subir', error),
        () => {
          console.log('Imagen cargada correctamente');
          // item.url = uploadTask.snapshot.downloadURL;
          uploadTask.snapshot.ref.getDownloadURL()
          .then( (downloadURL) => {
            console.log('File available at', downloadURL);
            item.url = downloadURL;

            this.saveImg({
              nombre: item.nombreArchivo,
              url: item.url
            });
          });

          item.subiendo = false;
        });
     }
   }


   private saveImg( imagen: { nombre: string, url: string }) {
    this.db.collection(`/${this.CARPETA_IMAGENES}`)
    .add(imagen);
  }
}
