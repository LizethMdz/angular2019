import { Component, OnInit } from '@angular/core';
/** NOTE LIBRERIAS */
import { FileItem } from 'src/app/models/file-item.model';
import { CargaImagenesService } from 'src/app/services/carga-imagenes.service';

@Component({
  selector: 'app-carga',
  templateUrl: './carga.component.html',
  styles: []
})
export class CargaComponent implements OnInit {
  onDrop = false;
  files: FileItem[] = [];
  constructor(private ciS: CargaImagenesService) { }

  ngOnInit() {
  }

  /**
   * Carga las imagenes provenientes del Service
   * o DataBase
   * @memberof CargaComponent
   */
  loadImg() {
    this.ciS.loadImgFirebase(this.files);
  }

  /**
   * Imprime true al hacer drop en la caja de
   * carga
   * @param {*} event
   * @memberof CargaComponent
   */
  onElement(event) {
    console.log( event);
  }

  cleanImg() {
    this.files = [];
  }

}
