import { Directive, EventEmitter, ElementRef,
HostListener, Input, Output } from '@angular/core';
import { FileItem } from '../models/file-item.model';

@Directive({
  selector: '[appNgDropFiles]'
})
export class NgDropFilesDirective {
  /** NOTE PARA INTERACTUAR CON EL PADRE */
  @Input() archivos: FileItem[] = [];
  @Output() mouseSobre: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  @HostListener('dragover', ['$event'])
  public onDragEnter(event: any) {
    this.mouseSobre.emit(true);
    this._preventStop(event);
  }

  @HostListener('dragleave', ['$event'])
  public onDragLeave(event: any) {
    this.mouseSobre.emit(false);
  }

  @HostListener('drop', ['$event'])
  public onDrop(event: any) {

    const tranferencia = this._getTransferencia(event);
    if (!tranferencia ) {
      return;
    }

    this._extractFiles (tranferencia.files);
    this._preventStop(event);
    this.mouseSobre.emit(false);
  }

  private _getTransferencia( event: any) {
    return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTranfer;
  }

  private _extractFiles(archivosLista: FileList ) {
    // console.log(archivosLista);

    // NOTE Object.getOwnPropertyNames(objeto) Permite tomar las
    // propiedades de un objeto y convertirlo en un arreglo
    for (const propiedad in Object.getOwnPropertyNames(archivosLista)) {
      const archivoTemp = archivosLista[propiedad];
      // console.log(archivoTemp);

      if (this._fileCanBeLoaded (archivoTemp )) {
        const nuevoArchivo = new FileItem(archivoTemp);
        this.archivos.push(nuevoArchivo);
      }
    }

    console.log(this.archivos);
  }

  // Validaciones

  /**
   * Valida que el archivo a recibir cumpla
   * con los metodos _DroppedFileYet() y _isImage()
   * para dar por hecho que un archivo válido
   * @private
   * @param {File} archivo
   * @returns {boolean}
   * @memberof NgDropFilesDirective
   */
  private _fileCanBeLoaded(archivo: File): boolean {
    if(!this._DroppedFileYet(archivo.name) && this._isImage(archivo.type)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Evita que las imagenes se abran
   * automaticamente al hacer el "Drop"
   * @private
   * @param {*} event
   * @memberof NgDropFilesDirective
   */
  private _preventStop(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  /**
   * Verifica si una imagen ya fue
   * cargada en nuestro arreglo
   * @private
   * @param {string} nombreArchivo
   * @returns {boolean}
   * @memberof NgDropFilesDirective
   */
  private _DroppedFileYet( nombreArchivo: string ): boolean {
    for ( const archivo of this.archivos ) {
      if (archivo.nombreArchivo === nombreArchivo) {
        console.log('El archivo ' + nombreArchivo + ' ya está agregado');
        return true;
      }
    }

    return false;
  }

  /**
   * Valida que una imagen sea un imagen
   *
   * @private
   * @param {string} tipoArchivo
   * @returns {boolean}
   * @memberof NgDropFilesDirective
   */
  private _isImage( tipoArchivo: string ): boolean {
    return (tipoArchivo === '' || tipoArchivo === undefined) ? false : tipoArchivo.startsWith('image');
  }
}
