// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDlxmEfrkf2raNJnHtcnbo4UFsJS3UWdvw',
    authDomain: 'fir-fotos-b812e.firebaseapp.com',
    databaseURL: 'https://fir-fotos-b812e.firebaseio.com',
    projectId: 'fir-fotos-b812e',
    storageBucket: 'fir-fotos-b812e.appspot.com',
    messagingSenderId: '344394468166',
    appId: '1:344394468166:web:4864fae5af28d4ffeab347',
    measurementId: 'G-CFFTLM8JWG'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
