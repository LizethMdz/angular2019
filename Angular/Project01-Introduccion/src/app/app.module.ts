// NOTE LE DECIMOS QUE SERVIOS Y MODUOS TIENE

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
// NOTE IMPORTACIÓN DE MI COMPONENTE, PERO DEBEMOS
// DE ASEGURARNOS QUE NUESTRA CLASE TENGA LA PALABRA "EXPORT"
import { HeaderComponent } from './components/header/header.component';
import { BodyComponent } from './components/body/body.component';
import { FooterComponent } from './components/footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    // NOTE AQUI SE DECLARAN NUESTROS COMPONENTES IMPORTADOS
    HeaderComponent, 
    BodyComponent, FooterComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
