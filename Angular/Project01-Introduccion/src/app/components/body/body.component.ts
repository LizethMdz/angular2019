import { Component } from '@angular/core';

@Component ({
    selector : 'app-body',
    templateUrl : './body.component.html'
})

export class BodyComponent{

    mostrar = true;

    frase: any = {
        mensaje : 'If you can dream it, you can make it',
        autor : 'John Dream'
    }

    personajes: string[] = ['Lucas Till', 'Liam Hemworth', 'Barry Allen'];
}