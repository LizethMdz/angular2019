import { Component, OnInit } from '@angular/core';
import { HeroeModel } from '../../models/heroe/heroe.model';
import { NgForm } from '@angular/forms';
import { HeroesService } from '../../services/heroes.service';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

  heroe: HeroeModel = new HeroeModel();
  constructor(private heroesService: HeroesService,
              private route: ActivatedRoute) {
              }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    console.log(id);

    if ( id !== 'nuevo' ) {
      this.heroesService.obtenerHeroe(id).
      subscribe ( (resp: HeroeModel) => {
        this.heroe = resp;
        this.heroe.id = id;
      });
    }
  }

  guardar(form: NgForm) {
    if ( form.invalid ) {
      console.log('Formulario no válido');
      return;
    }

    Swal.fire({
      allowOutsideClick: false,
      title: 'Espere!',
      text: 'Guardando información...',
      icon: 'info'
    });
    Swal.showLoading();
    let peticion: Observable<any>;
    let bandera;

    if (this.heroe.id) {
      peticion = this.heroesService.editarHeroe(this.heroe);
      bandera = 'Se actualizó correctamente';

    } else {
      peticion = this.heroesService.crearHeroe(this.heroe);
      bandera = 'Se creo correctamente';

    } // end else

    peticion.subscribe(
      (resp) => {
        Swal.fire({
          allowOutsideClick: false,
          title: this.heroe.nombre,
          text: bandera,
          icon: 'success'
      });

      }); // end subscribe

  } // end guardar()

} // end class
