import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../../services/heroes.service';
import { HeroeModel } from '../../models/heroe/heroe.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes: HeroeModel[] = [];
  cargando = false;

  constructor(private heroeService: HeroesService) {
  }

  ngOnInit() {
    this.cargando = true;
    this.heroeService.listarHeroes()
    .subscribe( resp => {
      this.cargando = false;
      this.heroes = resp;
    });
  }

  _listarheroes() {
    this.heroeService.listarHeroes()
    .subscribe( data => {
      console.log(data);
      this.heroes = data;
    });
  }

  _borrarheroe(heroe: HeroeModel, i: number) {

    Swal.fire({
      title: '¿Esta seguro?',
      text: `Desea borrar a ${heroe.nombre} `,
      showConfirmButton: true,
      showCancelButton: true
    }).then(resp => {
      if (resp.value ) {
        this.heroes.splice(i, 1);
        this.heroeService.borrarHeroe(heroe.id)
            .subscribe();
      }
    });

  }

}
