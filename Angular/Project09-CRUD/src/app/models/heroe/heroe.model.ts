export class HeroeModel {
    id: string;
    nombre: string;
    estado: boolean;
    properties: string;

    constructor() {
        this.estado = true;
    }
}
