import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeroeModel } from '../models/heroe/heroe.model';
import { map, delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  url =  'https://login-angular-e8637.firebaseio.com';
  constructor(private http: HttpClient) {

  }

  crearHeroe(heroe: HeroeModel) {
    // post(url, body)
    return this.http.post(`${this.url}/heroes.json`, heroe)
    .pipe(
      map( (resp: any) => {
        // "name" es la propiedad del json que regresa FIREBASE
        // name { id: "gskdhgasd" }
        heroe.id = resp.name;
        return heroe;
      })
    );
  }

  editarHeroe(heroe: HeroeModel) {
    const heroeTemp = {
      ...heroe
    };

    delete heroeTemp.id;
    // Editar un elemento

    return this.http.put(`${this.url}/heroes/${heroe.id}.json`, heroeTemp);
  }

  listarHeroes() {
    return this.http.get(`${this.url}/heroes.json`).
    pipe(
      //  map( resp => {
      //      this.crearArreglo(resp);
      //  })

      // No es lo mismo hacerlo como la sentencia anterior
      map (this.crearArreglo)
    );
  }

  private crearArreglo( heroesObj: object) {
    const heroes: HeroeModel[] = [];
    console.log(heroesObj);

    // Por si no hay elementos en la base de datos
    if (heroesObj === null) {
      return [];
    }

    // Reemplazamos los id como la key de cada elemento
    // y despues los agregamos al arreglo de elementos heroes: heroeModel[]
    Object.keys(heroesObj).forEach( key => {
      const heroe: HeroeModel = heroesObj[key];
      heroe.id = key;
      heroes.push(heroe);
    });
    // console.log(heroes);
    return heroes;
  }

  obtenerHeroe(id: string) {
    return this.http.get(`${this.url}/heroes/${ id }.json`);
  }

  borrarHeroe(id: string) {
    // Accion de borrar
    return this.http.delete(`${this.url}/heroes/${id}.json`);
  }


}
