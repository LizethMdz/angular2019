import { Component, OnInit } from '@angular/core';
import { usuarioModel } from '../../../models/usuario.model';
import { NgForm } from '@angular/forms';
import { AuthloginService } from '../../services/authlogin.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: usuarioModel;
  recordarme = false;

  constructor(private authlogin: AuthloginService,
              private router: Router) { }

  ngOnInit() {
    this.usuario = new usuarioModel();

    if ( localStorage.getItem('email')) {
      this.usuario.email = localStorage.getItem('email');
      this.recordarme = true;
    }
  }

  login(form: NgForm) {
    if (form.invalid) { return; }

    Swal.fire({
      allowOutsideClick: false,
      title: 'Cargardo!',
      text: 'Espere por favor...',
      icon: 'info'
    });
    Swal.showLoading();

    this.authlogin.loging(this.usuario).subscribe(
      respuesta => {
        console.log(respuesta);
        Swal.close();
        this.router.navigateByUrl('/home');

        if (this.recordarme) {
          localStorage.setItem('email', this.usuario.email);
        }

      }, (err) => {
        console.log(err.error.error.message);
        Swal.fire({
          allowOutsideClick: false,
          title: 'Error!',
          text: err.error.error.message,
          icon: 'error'
        });
      }
    );
  }

}
