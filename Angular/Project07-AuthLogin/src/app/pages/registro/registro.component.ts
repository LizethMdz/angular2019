import { Component, OnInit } from '@angular/core';
import { usuarioModel } from '../../../models/usuario.model';
import { NgForm } from '@angular/forms';
import { AuthloginService } from '../../services/authlogin.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  usuario: usuarioModel;
  recordarme = false;
  constructor(private authlogin: AuthloginService, private router: Router) { }

  ngOnInit() {
    this.usuario = new usuarioModel();
  }


  onSubmit(form: NgForm) {
    if ( form.invalid) { return; }
    Swal.fire({
      allowOutsideClick: false,
      title: 'Registrando!',
      text: 'Espere por favor...',
      icon: 'info'
    });
    Swal.showLoading();

    this.authlogin.nuevousuario(this.usuario)
    .subscribe(
      (respuesta) => {
        console.log(respuesta);
        Swal.close();
        if (this.recordarme) {
          localStorage.setItem('email', this.usuario.email);
        }
        this.router.navigateByUrl('/home');

      }, (err) => {
        Swal.fire({
          allowOutsideClick: false,
          title: 'Error!',
          text: err.error.error.message,
          icon: 'error'
        });
       });
  }
}
