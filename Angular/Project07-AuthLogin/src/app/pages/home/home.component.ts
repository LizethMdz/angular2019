import { Component, OnInit } from '@angular/core';
import { AuthloginService } from '../../services/authlogin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private auth: AuthloginService,
              private router: Router) { }
  ngOnInit() {
  }

  log_out() {
    this.auth.logout();
    this.router.navigateByUrl('/login');
  }

}
