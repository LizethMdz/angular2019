import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { usuarioModel } from '../../models/usuario.model';

import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthloginService {

  private url = 'https://identitytoolkit.googleapis.com/v1/accounts';
  private apiKey = 'AIzaSyAF5bDnmY0c-kmrR2PefP0c8vA1bt0LW-E';
  userToken: string;
  constructor(private http: HttpClient) {
    this.readToken();
  }

  // NOTE: CREAR NUEVO USUARIO
  // https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]

  // NOTE: LOGEAR UN USUARIO
  // https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]

  nuevousuario(usuario: usuarioModel) {
    // Se declara que el usuario sera nuestra
    // data a enviar por el post
    // const authData = {
    //   email: usuario.email,
    //   password: usuario.password,
    //   returnSureToken: true
    // }

    const authData = {
      ...usuario,
      returnSureToken: true
    };
    console.log(this.url);
    console.log(this.apiKey);
    console.log(authData);

    return this.http.post(
      `${ this.url }:signUp?key=${ this.apiKey }`,
      authData
    ).pipe(
      // Pasa por un filtro antes de regresar la respuesta
      map( resp => {
        this.saveToken(resp['idToken']);
        return resp;
      })
    );

  }

  logout() {
    localStorage.removeItem('token');
  }

  loging(usuario: usuarioModel) {
    const authData = {
      ...usuario,
      returnSureToken: true
    };
    console.log(this.url, this.apiKey);
    console.log(authData);

    return this.http.post(
      `${ this.url }:signInWithPassword?key=${ this.apiKey }`,
      authData
    ).pipe(
      map( resp => {
        this.saveToken(resp['idToken']);
        return resp;
      })
    );
  }

  private saveToken(idToken: string) {
    this.userToken = idToken;
    localStorage.setItem('token', idToken);

    let hoy = new Date();
    hoy.setSeconds(3600);

    localStorage.setItem('expira', hoy.getTime().toString());
    console.log(hoy.getTime().toString);
  }

  private readToken() {
    if (localStorage.getItem('token')) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }

    return this.userToken;
  }

  isAuthenticated(): boolean {
    // NOTE Limite de tiempo
    // if (this.userToken) {
    //   return this.userToken.length > 2;
    // }
    if (this.userToken.length < 2) {
      return false;
    }
    // NOTE Obtine el dato de expiracion en el localStorage
    const expira = Number(localStorage.getItem('expira'));
    const DateExpira = new Date();
    DateExpira.setTime(expira);

    if (DateExpira > new Date()) {
      return true;
    } else {
      return false;
    }
  }
}


