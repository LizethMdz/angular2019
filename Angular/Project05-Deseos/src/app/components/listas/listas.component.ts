import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DeseosService } from 'src/app/services/deseos.service';
import { Lista } from 'src/app/models/lista.model';
import { Router } from '@angular/router';
import { AlertController, IonList } from '@ionic/angular';


@Component({
  selector: 'app-listas',
  templateUrl: './listas.component.html',
  styleUrls: ['./listas.component.scss'],
})
export class ListasComponent implements OnInit {
  @ViewChild(IonList, {static: false }) lista: IonList;
  @Input() terminados = true;

  constructor(public deseosService: DeseosService,
              private router: Router,
              private alertC: AlertController) {

  }

  ngOnInit() {}



  listaSeleccionada(lista: Lista) {
    // console.log(lista);
    const idListaSelected = lista.id;
    if ( this.terminados) {
      console.log(lista.terminada);
      this.router.navigateByUrl(`/tabs/tab2/agregar/${idListaSelected}`);

    } else {
      this.router.navigateByUrl(`/tabs/tab1/agregar/${idListaSelected}`);
    }
  }

  borrarLista(lista: Lista) {
    this.deseosService.borrarLista(lista);
  }

  async editarLista(lista: Lista) {

    const alert = await this.alertC.create({
      header: 'Editar Nombre de la Lista',
      inputs: [{
        name: 'titulo',
        type: 'text',
        value: lista.titulo
      }],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelar');
            this.lista.closeSlidingItems();
          }
        },
        {
          text: 'Cambiar',
          handler: (data ) => {
              console.log(data);

              if (data.titulo.length === 0 ) {
                return;
              }

              lista.titulo = data.titulo;
              this.deseosService.guardarStorage();
              this.lista.closeSlidingItems();
          }
        }
      ]
    });
    alert.present();

  }
}
