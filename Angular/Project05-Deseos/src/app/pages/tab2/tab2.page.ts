import { Component } from '@angular/core';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  // SE DECLARA NUESTRO SERVICIO PARA QUE SEA EMPLEADO
  constructor(public deseosService: DeseosService) {
    // console.log(deseosService);
    for(let i = 0; i < deseosService.listas.length; i++){
      if(deseosService.listas[i].terminada === true ){
        console.log("Lista interior Tab2Page",deseosService.listas[i]);
      }
    }
  }

}
