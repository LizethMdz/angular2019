import { Component } from '@angular/core';
import { DeseosService } from 'src/app/services/deseos.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';



@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  // SE DECLARA NUESTRO SERVICIO PARA QUE SEA EMPLEADO
  // SE PROGRAMA LA LOGICA PARA LA CENTRALIZACION DE LA FUNCIONALIDAD

  // _listas: Lista[];
  constructor(public deseosService: DeseosService,
    private router: Router, private alertC: AlertController) {
    // this._listas = this.deseosService.listas;
    // console.log(this._listas);
  }

  async agregarLista() {
    //this.router.navigateByUrl('/tabs/tab1/agregar');
    //async presentAlert() {
      const alert = await this.alertC.create({
        header: 'Nueva Lista',
        inputs: [{
          name: 'titulo',
          type: 'text',
          placeholder: 'Nombre de la Lista'
        }],
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('Cancelar');
            }
          },
          {
            text: 'Crear',
            handler: (data ) => {
                // console.log(data);
                if(data.titulo.length === 0 ){
                  return;
                }

                //CREAR LA LISTA
                const idListaSent = this.deseosService.crearLista(data.titulo);
                this.router.navigateByUrl(`/tabs/tab1/agregar/${idListaSent}`);
            }
          }
        ]
      });
      alert.present();
    //}
  }



}
