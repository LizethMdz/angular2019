import { Component, OnInit } from '@angular/core';
import { DeseosService } from 'src/app/services/deseos.service';
import { ActivatedRoute } from '@angular/router';
import { Lista } from 'src/app/models/lista.model';
import { ListaItem } from 'src/app/models/lista-item.model';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {

  _lista: Lista;
  nombreItem: string;
  constructor(private deseosService: DeseosService, private route: ActivatedRoute) {
    const idFromLista = this.route.snapshot.paramMap.get('listaId');
    this._lista = this.deseosService.obtenerLista(idFromLista);
    console.log(this._lista);
  }

  ngOnInit() {
  }

  agregarItem() {
    if (this.nombreItem.length === 0) {
      return;
    }
    const newItem = new ListaItem(this.nombreItem);
    this._lista.items.push(newItem);
    this.nombreItem = '';
    this.deseosService.guardarStorage();
  }

  chambioCheck(item: ListaItem) {
    // console.log(item);
    const pendientes = this._lista.items.filter(itemData => !itemData.completado)
    .length;
    console.log({pendientes});

    if (pendientes === 0) {
      this._lista.terminadaEn = new Date();
      this._lista.terminada = true;
    } else {
      this._lista.terminadaEn = null;
      this._lista.terminada = false;
    }

    this.deseosService.guardarStorage();
    console.log(this.deseosService.listas);
  }

  borrarItem(i: number) {
    this._lista.items.splice(i, 1);
    this.deseosService.guardarStorage();
  }

}
