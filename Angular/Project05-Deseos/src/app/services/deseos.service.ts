import { Injectable } from '@angular/core';
import { Lista } from '../models/lista.model';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root'
})
export class DeseosService {

  listas: Lista[] = [];

  constructor() {
    this.cargarStorage();
  }

  crearLista(titulo: string) {
    const newlista = new Lista(titulo);
    this.listas.push(newlista);
    this.guardarStorage();

    return newlista.id;
  }

  // NOTE CADA LISTA TIENE UN ID, POR TANTO ES NECESARIO PARA ACCEDER A UN
  // TIPO DE LISTA
  obtenerLista(id: string | number) {
    id = Number(id);
    return this.listas.find( listaData => listaData.id === id);
  }

  guardarStorage() {
    localStorage.setItem('data', JSON.stringify(this.listas));
  }

  cargarStorage() {
    if (localStorage.getItem('data')) {
      this.listas = JSON.parse(localStorage.getItem('data'));
    } else {
      this.listas = [];
    }
  }

  borrarLista(lista: Lista) {
    this.listas = this.listas.filter(data => data.id !== lista.id );
    this.guardarStorage();
  }

}
