import { Pipe, PipeTransform } from '@angular/core';
import { Lista } from '../models/lista.model';


 /** NOTE La propiedad pure=fasle permite que los cambios que se hagan en la
  * aplicacion se guarden automaticamente.
  */

@Pipe({
  name: 'filtroCompletado',
  pure: false
})
export class FiltroCompletadoPipe implements PipeTransform {

  transform(listas: Lista[], completado: boolean = true): Lista[] {
    return listas.filter( lista => {
      return lista.terminada === completado;
    });
  }

}
