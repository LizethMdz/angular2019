Habilitar el Functions en Firebase

Instalar 

Instalar las herramientas de Firebase:

CODE ACCEPTED - $ npm install -g firebase-tools

Despues ejecutar

CODE ACCEPTED - firebase login

Debemos esperar a que hagamos todas la autenticacion...

===================================================================

PS D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend> CODE ACCEPTED - firebase init

     ######## #### ########  ######## ########     ###     ######  ########     
     ##        ##  ##     ## ##       ##     ##  ##   ##  ##       ##
     ######    ##  ########  ######   ########  #########  ######  ######
     ##        ##  ##    ##  ##       ##     ## ##     ##       ## ##
     ##       #### ##     ## ######## ########  ##     ##  ######  ########        

You're about to initialize a Firebase project in this directory:

  D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend

? Are you ready to proceed? CODE ACCEPTED - Yes
? Which Firebase CLI features do you want to set up for this folder? Press Space to select features, then Enter to confirm your choices. 
CODE ACCEPTED - Functions: Configure and deploy Cloud Functions

=== Project Setup

First, let's associate this project directory with a Firebase project.
You can create multiple project aliases by running firebase use --add,
but for now we'll just set up a default project.

? Please select an option: CODE ACCEPTED - Use an existing project
? Select a default Firebase project for this directory: CODE ACCEPTED - graficas-online (Graficas-Online)
i  Using project graficas-online (Graficas-Online)

=== Functions Setup

A functions directory will be created in your project with a Node.js
package pre-configured. Functions can be deployed with firebase deploy.

? What language would you like to use to write Cloud Functions? CODE ACCEPTED - TypeScript
? Do you want to use TSLint to catch probable bugs and enforce style? CODE ACCEPTED -Yes
+  Wrote functions/package.json
+  Wrote functions/tslint.json
+  Wrote functions/tsconfig.json
+  Wrote functions/src/index.ts
+  Wrote functions/.gitignore
? Do you want to install dependencies with npm now? CODE ACCEPTED -Yes

> protobufjs@6.9.0 postinstall D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend\functions\node_modules\protobufjs
> node scripts/postinstall

npm notice created a lockfile as package-lock.json. You should commit this file.
added 292 packages from 221 contributors and audited 292 packages in 26.496s

32 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities


i  Writing configuration info to firebase.json...
i  Writing project information to .firebaserc...
i  Writing gitignore file to .gitignore...

+  Firebase initialization complete!
PS D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend>

=========================================================================================================================================
NOTE PRODUCCION
CODE ACCEPTED - - firebase deploy
=== Deploying to 'graficas-online'...

i  deploying functions
Running command: npm --prefix "$RESOURCE_DIR" run lint

> functions@ lint D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend\functions
> tslint --project tsconfig.json

Running command: npm --prefix "$RESOURCE_DIR" run build

> functions@ build D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend\functions
> tsc

+  functions: Finished running predeploy script.i  functions: ensuring required API cloudfunctions.googleapis.com is enabled...
+  functions: required API cloudfunctions.googleapis.com is enabled
i  functions: preparing functions directory for 
uploading...
i  functions: packaged functions (34.61 KB) for 
uploading
+  functions: functions folder uploaded successfully
i  functions: creating Node.js 8 function helloWorld(us-central1)...
+  functions[helloWorld(us-central1)]: Successful create operation.
Function URL (helloWorld): https://us-central1-graficas-online.cloudfunctions.net/helloWorld    

+  Deploy complete!

Function URL (helloWorld): https://us-central1-graficas-online.cloudfunctions.net/helloWorld

============================================================================
NOTE DESARROLLO
PS D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend> CODE ACCEPTED firebase serve        

=== Serving from 'D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend'...     

!  Your requested "node" version "8" doesn't match your global version "12"
+  functions: functions emulator started at http://localhost:5000
i  functions: Watching "D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend\functions" for Cloud Functions...
+  functions[helloWorld]: http function initialized (http://localhost:5000/graficas-online/us-central1/helloWorld).

=====================================================================

COMMENT - PARA COMPILAR LOS NUEVOS CAMBIOS EN EL SERVIDOR ES NECESARIO COMPILAR
COMMENT - PARA ELLO SE JECUTA LO SIGUINETE

Project Console: https://console.firebase.google.com/project/graficas-online/overview
PS D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend> cd .\functions\       
PS D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend\functions> CODE ACCEPTED npm run build

> functions@ build D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend\functions
> tsc

PS D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend\functions>

========================================================================

COMMENT - LA OPTIMIZACION PARA ESTE PASO ES LO SIGUIENTE:

PS D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend\functions> CODE ACCEPTED tsc --watch 
[18:18:05] Starting compilation in watch mode...
[18:18:08] Found 0 errors. Watching for file changes.

=======================================================================================================================

COMMENT - Instalar dependencies

npm install firebase-admin

==============================================================

COMMENT - DESCARGAR ARCHIVO CON CLAVE GENERADA, DESDE 
-> CONFIGURACION -> CUENTAS DE SERVICIO

NOTE: PARA Node.js

var admin = require("firebase-admin");

var serviceAccount = require("path/to/serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://graficas-online.firebaseio.com"
});

====================================================================

COMMENT INSTALAR EXPRESS Y CORS
PS D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend\functions>
npm install express cors

COMMENT EXPRESS

npm install @types/express --save-dev
PS D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend\functions> npm install @types/express --save-dev

COMMENT CORS

npm install @types/cors --save-dev
PS D:\lizet\Documents\ANGULAR\angular2019\Angular\Project20\goty-backend\functions> npm install @types/cors --save-dev

====================================================================