import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
/** Importar Express */
import * as express from 'express';
import * as cors from 'cors';

/** NOTE Configuracion para conectar a Firebase de manera Local */
// URL https://console.firebase.google.com/u/0/project/graficas-online/settings/serviceaccounts/adminsdk
const serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://graficas-online.firebaseio.com"
});

/** NOTE Referencia a la base de datos de firebase */

const db = admin.firestore();

// End Configuration
// export const helloWorld = functions.https.onRequest((request, response) => {
//     response.json({
//         mensaje: "Hello from Function Firebase!"
//     });
// });

export const getGOTY = functions.https.onRequest( async(request, response) => {
    // const nombre = request.query.nombre || 'Sin nombre';
    // response.json({
    //     nombre
    // });

    /** NOTE Referencia a la coleccion de mi Base de datos */
    const gotyRef = db.collection('goty');
    // COMMENT el await permite regresar lo del metodo de get()
    const docsSnap = await gotyRef.get();
    const juegos = docsSnap.docs.map( doc => doc.data());
    // NOTE Retorno de todos los registros
    response.json( juegos);

});

// Express
const app = express();
app.use(cors({origin: true}));

app.get('/goty', async(req, res) => {

    const gotyRef = db.collection('goty');
    const docsSnap = await gotyRef.get();
    const juegos = docsSnap.docs.map( doc => doc.data());
    res.json( juegos);
})

// POST
app.post('/goty/:id', async(req, res) => {
    const id = req.params.id;
    const gameRef = db.collection('goty').doc(id);
    const gameSnap = await gameRef.get();

    if( !gameSnap.exists){
        res.status(404).json({
            ok: false,
            mensaje : 'No exite un juego con ese id' + id
        });
    }else {
        //res.json('Juego existe')
        const antes = gameSnap.data() || { votos: 0};
        await gameRef.update({
            votos: antes.votos + 1
        });

        res.json({
            ok:true,
            mensaje: `Gracias por tu voto ${antes.name}`
        });
    }



});


// Firebase, un servidor de express corriendo

export const api = functions.https.onRequest(app);
