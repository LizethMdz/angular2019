import { GoogleAnalyticsService } from './services/google-analytics.service';
import { environment } from './../environments/environment';
import { Component, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

declare var gtag;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'goty';
  @ViewChild('head') head: ElementRef;
  constructor(private router: Router, private render2: Renderer2, private googleAnalytics: GoogleAnalyticsService) {
    const head = document.querySelector('head') as HTMLHeadElement;
    const scriptGoogleAnalytics = this.render2.createElement('script');
    this.render2.setAttribute(scriptGoogleAnalytics, 'async', 'true');
    this.render2.setAttribute(scriptGoogleAnalytics, 'src', environment.analyticsUrl);
    this.render2.appendChild(head, scriptGoogleAnalytics);
    // const navEndEvents$ = this.router.events.pipe(
    //   filter(event => event instanceof NavigationEnd)
    // );
    // navEndEvents$.subscribe((event: NavigationEnd) => {
    //   gtag('config', environment.idMedicion, {  
    //     'page_path': event.urlAfterRedirects
    //   });
    // });
    const navEndEvents$ = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    );
    this.googleAnalytics.sendCurrentUser("LAMT");
    navEndEvents$.subscribe((event: NavigationEnd) => {
      console.log('NavigationEnd', event.urlAfterRedirects, event.url);
      this.googleAnalytics.sendConfig(event);
      this.googleAnalytics.sendEventGA("screen_view", {
        app_name: environment.firebase.authDomain,
        screen_name: event.url
      })
    });
  }


}