import { TestBed } from '@angular/core/testing';

import { GoogleAnalyticsInitService } from './google-analytics-init.service';

describe('GoogleAnalyticsInitService', () => {
  let service: GoogleAnalyticsInitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoogleAnalyticsInitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
