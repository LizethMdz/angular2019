import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Game } from '../interfaces/interfaces';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private http: HttpClient) {}

  /** Funciones para consultar nuestro servicio  */
  getNominados() {
    return this.http.get<Game[]>(`${environment.url}/api/goty`);
  }

  postVotos(id: string){
    return this.http.post(`${environment.url}/api/goty/${id}`, {})
      .pipe(
        catchError( error => {
          console.log('Error en la peticion');
          console.log(error);
          return of(error.error);
        })
      );
  }
}
