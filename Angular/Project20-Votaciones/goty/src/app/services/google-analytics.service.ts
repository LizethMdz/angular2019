import { Injectable } from '@angular/core';
import { NavigationEnd } from '@angular/router';
import { environment } from 'src/environments/environment';

declare const gtag: (...params : any[]) => void; 

export enum TypeGoogleAnalytics {
  Event = "event",
  Config = "config",
  Set = "set"
}

@Injectable({
  providedIn: 'root'
})
export class GoogleAnalyticsService {

  constructor() { }

  public sendCustomEvent(eventName : string, eventCategory : string, eventLabel : string, eventValue : any) {
    gtag(TypeGoogleAnalytics.Event, eventName, {
      event_category : eventCategory,
      event_label : eventLabel,
      value : eventValue
    });
  }

  public sendEventGA(eventName : string, eventParameters : any) {
    gtag(TypeGoogleAnalytics.Event, eventName, eventParameters);
  }

  public sendConfig(event : NavigationEnd) {
    gtag(TypeGoogleAnalytics.Config, environment.idMedicion,
      {
        page_path : event.urlAfterRedirects,
        send_page_view : true
      }
    );
  }

  public sendCurrentUser(userId: string) {
    gtag(TypeGoogleAnalytics.Set,
      {
        user_id : userId
      }
    );
  }
}
