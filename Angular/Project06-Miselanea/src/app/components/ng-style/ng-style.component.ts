import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  template: `

    <strong>NG-STYLE</strong>
    <p [ngStyle]="{'font-size': tamano + '%'}">
      Test ngStyle="'property': expression"
    </p>

    <p [style.fontSize.px]="tamano">
      Test ngStyle="'property': expression"
    </p>

    <p [style.fontSize]="'20px'">
      Test ngStyle="'property': expression"
    </p>

    <hr>

    <button class="btn btn-primary" (click)="tamano = tamano + 5">
      <i class="fas fa-plus"></i>
    </button>

    <button class="btn btn-primary" (click)="tamano = tamano - 5">
      <i class="fas fa-minus"></i>
    </button>

    <hr>
  `,
  styles: [
  ]
})
export class NgStyleComponent implements OnInit {

  // TAMAÑO DEL FONT
  tamano: number = 10;
  constructor() { }

  ngOnInit() {
  }

}
