import { Component, OnInit,
OnChanges, DoCheck, AfterContentInit,
AfterContentChecked, AfterViewInit,
AfterViewChecked, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
  <!-- NG-STYLE -->
  <app-ng-style></app-ng-style>
  <!-- NG-CSS -->
  <app-css></app-css>
  <!-- NG-CLASS -->
  <app-ng-class></app-ng-class>

  <!-- USO DE DIRECTIVAS- APLICAN UN NUEVO STYLE AL ELEMENTO  -->
  <!-- SIN MANDAR PARAMETROS -->
  <strong>DIRECTIVAS</strong>
  <p appResaltado>Estilo de un Componente</p>
  <!-- MANDAR PARAMETROS -->
  <p [appResaltado]="'red'">Estilo de un Componente</p>

  <!-- NG-SWITCH -->
  <app-ng-switch></app-ng-switch>
  `,
  styles: []
})

export class HomeComponent implements OnInit, OnChanges, DoCheck, AfterContentInit,
AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {

  constructor() {
    console.log("Constructor");
  }

  // CICLO DE VIDA DE UN COMPONENTE
  ngOnInit() {
    console.log("ngOnInit");
  }
  ngOnChanges(){
    console.log("ngOnChanges");
  }
  ngDoCheck(){
    console.log("ngDoCheck");
  }
  ngAfterContentInit(){
    console.log("ngAfterContentInit");
  }
  ngAfterContentChecked(){
    console.log("ngAfterContentChecked");
  }
  ngAfterViewInit(){
    console.log("ngAfterViewInit");
  }
  ngAfterViewChecked(){
    console.log("ngAfterViewChecked");
  }
  ngOnDestroy(){
    console.log("ngOnDestroy");
  }

}
