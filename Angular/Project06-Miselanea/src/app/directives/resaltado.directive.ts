import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  constructor(private el:ElementRef) {
    console.log("Directiva llamada");
    // el.nativeElement.style.background = "yellow";
   }
   // VARIALBE DE VIENE DE AFUERA - PERO HACIENDO REFERENCIA A LA DIRECTIVA
   @Input ("appResaltado") nuevoColor: string;

   @HostListener('mouseenter') mouseEntro () {
     console.log(this.nuevoColor);
     this.resaltar(this.nuevoColor || 'yellow');
   }

   @HostListener('mouseleave') mouseSalio () {
     this.resaltar(null);
     // this.el.nativeElement.style.background = null;
   }

   private resaltar(color: string){
     this.el.nativeElement.style.background = color;
   }


}
