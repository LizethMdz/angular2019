import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-precios',
  templateUrl: './precios.component.html',
  styles: []
})
export class PreciosComponent implements OnInit {

  constructor(private auth: AuthService) {

   }

  ngOnInit() {

    console.log(this.auth.loggedIn);
  }

}
