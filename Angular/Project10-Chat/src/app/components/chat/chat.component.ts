import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../providers/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styles: []
})
export class ChatComponent implements OnInit {

  mensaje: string;
  elemento: any;
  claseActivada: false;

  constructor(private chatService: ChatService) {
    /** Solo se subscribe de esta manera, ya que la otra logica estrá en el service */
    this.chatService.cargarMensajes()
    .subscribe(
      () => {
        /** Se coloco esta instruccion para que el ultimo mensaje sea mostrado */
        setTimeout( () => {
          this.elemento.scrollTop = this.elemento.scrollHeight;
        }, 20);
      }
     );
   }

  ngOnInit() {
    /** Se obtuvo el id del "div" que contiene el scroll */
    this.elemento = document.getElementById('app-msm');
  }

/**
 * Una cadena de elementos
 * son insertados en la coleccion
 * @returns Promise
 */
_enviarMensaje() {
    console.log(this.mensaje);
    if (this.mensaje.length === 0) {
      return;
    }
    this.chatService.agregarMensaje(this.mensaje)
    .then( () => this.mensaje = '')
    .catch( (err) => console.error('Error al enviar', err));
  }

  volverUp() {
    console.log('Ir arriba!');

    /** Funcion que regresa el scroll hasta arriba */
    console.log('Volvio arriba');
    setTimeout(() => {
       this.elemento.scrollTop = 0;
    }, 200);

  }

}
