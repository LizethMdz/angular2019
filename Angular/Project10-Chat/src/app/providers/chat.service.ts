import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { map } from 'rxjs/operators';
import { Mensaje } from '../interfaces/mensaje.interface';

/** Librerias para hacer la autenticacion con Google */
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public chats: Mensaje[] = [];
  public usuario: any = {};


  /** Elementos any => Mensaje */
  private itemsCollection: AngularFirestoreCollection<Mensaje>;

  constructor(private afs: AngularFirestore,
              public afAuth: AngularFireAuth) {
                this.afAuth.authState.subscribe(user => {
                  console.log('Estado del usuario...', user);

                  if (!user) {
                    /** No regresa nada */
                    return;
                  }

                  /** Regresa las variables de sesion */

                  this.usuario.nombre = user.displayName; // regresa un objeto
                  this.usuario.uid = user.uid;

                });
  }

  /** Metodo para hacer el logeo */

  login(proveedor: string) {

    if (proveedor === 'google') {
    // Despues del auth. Salen todos los demas servicios de autenticacion
      this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).catch(
        error => {
          console.log(error);
        }
      );
    } else {
      // Registro de apps en Twitter
      this.afAuth.auth.signInWithPopup(new firebase.auth.TwitterAuthProvider()).catch(
        error => {
          console.log(error);
        }
      );
    }
  }

  /** Metodo para hacer el logout */

  logout() {
    this.usuario = {};
    console.log('Cerró sesión');
    this.afAuth.auth.signOut();
  }

  cargarMensajes() {
    // Regresa cada elemento de la colección "chats"
    this.itemsCollection = this.afs.collection<Mensaje>('chats', ref => ref.orderBy('fecha', 'desc').limit(5));
    return this.itemsCollection.valueChanges()
    .pipe(
      map(
        (mensajes: Mensaje[]) => {
          console.log(mensajes);
          // this.chats =   mensajes;
          this.chats = [];

          for (let mensaje of mensajes) {
            /** Los elementos se insertan al comienzo del array */
            /** [n] => [ 0 1 2 3 ] */
            this.chats.unshift(mensaje);
          }

          return this.chats;
        }
      )
    );
  }

  agregarMensaje(msm: string) {
    // NOTE Falta el UID
    let mensajeEnviado: Mensaje = {
      nombre: this.usuario.nombre,
      mensaje: msm,
      fecha: new Date().getTime(),
      uid: this.usuario.uid
    };

    // Regresa una promesa
    return this.itemsCollection.add(mensajeEnviado);

  }



}
