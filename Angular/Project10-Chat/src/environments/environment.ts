// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// CONFIGURACIÓN POR DEFECTO
// export const environment = {
//   production: false
// };

export const environment = {
  production: false,
  firebase: {
    // apiKey: '<your-key>',
    // authDomain: '<your-project-authdomain>',
    // databaseURL: '<your-database-URL>',
    // projectId: '<your-project-id>',
    // storageBucket: '<your-storage-bucket>',
    // messagingSenderId: '<your-messaging-sender-id>'
    apiKey: 'AIzaSyARAh5Wo-aMxFTKQYhUjlW4MUyJVg3Y1hQ',
    authDomain: 'chat-live-angular.firebaseapp.com',
    databaseURL: 'https://chat-live-angular.firebaseio.com',
    projectId: 'chat-live-angular',
    storageBucket: 'chat-live-angular.appspot.com',
    messagingSenderId: '604165372573',
    appId: '1:604165372573:web:fad667fca985c30e3b3274',
    measurementId: 'G-XRNEPQ37LW'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
