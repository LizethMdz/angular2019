import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, PipeTransform } from '@angular/core';
// NOTE import del PIPE creado manualmente
import { CapitalizadoPipe } from './pipes/capitalizado.pipe';
import { AppComponent } from './app.component';

// NOTE IMPORTS PARA EL CAMBIO DE FORMATO PARA LA FECHA
import localeEsMX from '@angular/common/locales/es-MX';
import localeFr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';
// NOTE PIPE IMPORTADO AUTOMATICAMENTE
import { DomseguroPipe } from './pipes/domseguro.pipe';
import { ContrasenaPipe } from './pipes/contrasena.pipe';

/** NOTE Nuevas actualizaciones */
registerLocaleData(localeEsMX);
registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    CapitalizadoPipe,
    DomseguroPipe,
    ContrasenaPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [ {
    provide: LOCALE_ID, useValue : 'es-MX'
  } ],
  bootstrap: [AppComponent]
})
export class AppModule { }
