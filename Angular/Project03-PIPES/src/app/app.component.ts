import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nombre = '¡¡ If you can dream it, you can do it!!';
  arreglo: number[] = [1, 2, 4, 5, 6, 5];
  PI = Math.PI;
  a = 0.2342;
  salario = 1234.5;

  ninja = {
    name : 'Kakashi',
    aldea : 'Konoha',
    habilidad : 'Ninjutsu',
    edad : 30,
    jutsus : {
      j1 : 'Taijutsu',
      j2 : 'Ninjutsu',
      j3 : 'Genjutsu',

    }
  }

  valorPromesa = new Promise( (resolve, reject) => {
    setTimeout( () => resolve('Llegó la data'), 3000);
  })

  fecha = new Date();

  frase = 'iF yOu Can DREam iT, you caN DO iT :)';

  videoenlace = 'fyeKqqVgSRs';

  activar: boolean = true;

  palabraClave = 'Secreto';

  /** NOTE Nuevas actualizaciones */
  idioma: string = 'es-MX';

}
