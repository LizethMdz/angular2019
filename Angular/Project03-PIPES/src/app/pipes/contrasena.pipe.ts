import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contrasena'
})
export class ContrasenaPipe implements PipeTransform {


  transform(value: string, ocultar: boolean = true): string {

    if ( ocultar ) {
      let salida: string = '';
      const temp: string[] = ['*'];
      for ( let i = 1; i < value.length; i++ ) {
        // salida = salida + '*';
        temp.push('*');
        salida += temp[i];
      }
      return salida;
    } else {
      return value;
    }

  }

}
