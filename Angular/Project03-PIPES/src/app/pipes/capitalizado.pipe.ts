import { Pipe, PipeTransform } from '@angular/core';
/**
 * PIPE personalizado
 * que regresa una cadena 'Live it'
 * @export
 * @class CapitalizadoPipe
 * @implements {PipeTransform}
 */
@Pipe({name: 'capitalizado'})

    // NOTE Una vez creado el PIPE se debe
    // importar en el app.module
    export class CapitalizadoPipe implements PipeTransform {
        transform(value: string, todas: boolean = true): string {
            // NOTE Para la parte de los argumentos es necesario
            // que si se define de esta manera, se antepongan ...args:any[]
            // Si no, se deben declarar separadamente args1, args2, etc.
            value = value.toLowerCase();
            let palabras = value.split(' ');
            if ( todas ) {
                for ( let i in palabras ){
                    // NOTE substring() extrae caracteres desde indiceA hasta indiceB sin incluirlo.
                    palabras[i] = palabras[i][0].toUpperCase() + palabras[i].substr(1);
                }
            } else {
                palabras[0] = palabras[0][0].toUpperCase() + palabras[0].substr(1);
            }

            return palabras.join(' ');
        }
    }
