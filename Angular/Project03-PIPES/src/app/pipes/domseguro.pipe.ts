import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';


/**
 * Pipe para mandar un parametro a una
 * url segura en este caso de YOUTUBE
 * @export
 * @class DomseguroPipe
 * @implements {PipeTransform}
 */
@Pipe({
  name: 'domseguro'
})
export class DomseguroPipe implements PipeTransform {

  constructor(private domSanitizer: DomSanitizer ) { }
  transform(value: string, url: string): any {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url + value);
  }

}
