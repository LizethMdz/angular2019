import { Component, OnInit, Inject} from '@angular/core';
// Dialog
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-mapa-editar',
  templateUrl: './mapa-editar.component.html',
  styleUrls: ['./mapa-editar.component.css']
})
export class MapaEditarComponent implements OnInit {

  //  Crear una referencia a FormGroup
  form: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    // NOTE De esta manera recibimos la data mandada de MapaComponent
    public dialogRef: MatDialogRef<MapaEditarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log(data);
      // Construcción del Form
      this.form = formBuilder.group({
        'titulo': data.titulo,
        'desc' : data.desc
      });
    }

  ngOnInit() {
  }

  saveChanges() {
    console.log(this.form.value);
    this.dialogRef.close(this.form.value);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
