import { Component, OnInit } from '@angular/core';
import { Marcador } from 'src/app/classes/marcador.class';
// Se importó para poder utilizar el SnackBar
import { MatSnackBar } from '@angular/material/snack-bar';
// Dialog
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { MapaEditarComponent } from './mapa-editar.component';

// TODO https://angular-maps.com/api-docs/agm-core/components/agmmap#mapClick

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  // Para crear este compoenente fue necesario especiicar de donde
  // importara las librerias
  // ng g c components/mapa --module=app.module --spec=false


  marcadores: Marcador[] = [];

  lat = 51.678418;
  lng = 7.809007;

  constructor(public snackBar: MatSnackBar,
              public dialog: MatDialog ) {
    // const marcador = new Marcador(51.678418, 7.809007);
    // this. marcadores.push( marcador);

    if (localStorage.getItem('marcadores')) {
       this.marcadores = JSON.parse(localStorage.getItem('marcadores'));
    }
  }

  ngOnInit() {
  }

  addMark(evento) {
    const coords: { lat: number, lng: number } = evento.coords;
    const marcador = new Marcador(coords.lat, coords.lng);
    this. marcadores.push( marcador);
    console.log(evento);
    this.saveStorage();
    // Simple message with an action.
    this.snackBar.open('Marcador agregado', 'Cerrar', {duration: 3000});
  }

  saveStorage() {
    localStorage.setItem('marcadores', JSON.stringify(this.marcadores));
  }

  deleteMark(index: number) {
    console.log(index);
    this.marcadores.splice(index, 1);
    this.saveStorage();
    this.snackBar.open('Marcador borrado', 'Cerrar', {duration: 3000});
  }

  editMark( mark: Marcador) {
      // NOTE: MapaEditarComponent Es inyectado
      const dialogRef = this.dialog.open( MapaEditarComponent, {
        width: '250px',
        data: {titulo: mark.titulo, desc: mark.desc }
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log(result);

        if ( !result) {
          return;
        }

        mark.titulo = result.titulo;
        mark.desc = result.desc;
        this.saveStorage();
        this.snackBar.open('Marcador editado', 'Cerrar', {duration: 3000});
      });
  }

}
