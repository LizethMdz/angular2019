import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

// Module
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
// Components
import { NavComponent } from './nav/nav.component';
import { MapaComponent } from './components/mapa/mapa.component';
import { MapaEditarComponent } from './components/mapa/mapa-editar.component';
// Libreria para Google Maps
import { AgmCoreModule } from '@agm/core';
// Formularios
import { ReactiveFormsModule } from '@angular/forms';


// Declarations Ngmodule //

@NgModule({
  entryComponents: [
    // Para declarar que el componente puede ser utilizado
    // como argumento o entrada.
    MapaEditarComponent
  ],
  declarations: [
    AppComponent,
    NavComponent,
    MapaComponent,
    MapaEditarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAcA0qbudek6EAMPBB0I-jeq96P3XYE5YM'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


