import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html',
  styles: []
})
export class HeroeTarjetaComponent implements OnInit {

  /**
   * Decorador de mi variable
   * heroe: any = {};
   * Indica que puede venir de fuera
   */
  @Input() heroe: any = {};
  @Input() index: number;
  
  /** NOTE SE DECLARA EL EVENTO JUNTO CON SU SALIDA
   * Indica que irá de este componente a otro
   */
  @Output() heroeSeleccionado: EventEmitter<number>;

  constructor(private router:Router) {
    this.heroeSeleccionado = new EventEmitter();
   }

  ngOnInit() {
  }

  verHeroe(){
    this.router.navigate(['/heroe', this.index]);
    // this.heroeSeleccionado.emit(this.index);
    }
}
