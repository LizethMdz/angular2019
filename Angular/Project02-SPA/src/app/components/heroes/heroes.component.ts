import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from '../../services/heroes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styles: []
})
export class HeroesComponent implements OnInit {

  heroes:Heroe[] = [];
  constructor(private _heroesService : HeroesService,
              private router:Router) { 
    // console.log("Constructor");
  }
  /** NOTE OBTIENE LOS HEROES DECLARADOS EN EL SERVICIO */
  ngOnInit() {
    this.heroes = this._heroesService.getHeores();
    // console.log(this.heroes);
  }

  /**
   * Funcion para redirecionar una ruta especifica
   * @param id number
   */

   /**
    * Funcion que sera llamada desde un hijo (HeroeTarjeta)
    */
  verHeroe(id: number){
    this.router.navigate(['/heroe', id]);
  }


}
