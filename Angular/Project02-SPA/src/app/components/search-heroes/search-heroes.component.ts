import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../services/heroes.service';

@Component({
  selector: 'app-search-heroes',
  templateUrl: './search-heroes.component.html',
  styles: []
})
export class SearchHeroesComponent implements OnInit {

  heroeBuscado: any[] = [];
  termino: string;
  constructor(private activatedRoute: ActivatedRoute,
              private heroesService: HeroesService) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {
      // console.log(params['miheroe']);
      this.termino = params['miheroe'];
      this.heroeBuscado = this.heroesService.buscarHeroes(params['miheroe']);
       console.log(this.heroeBuscado);
    });
  }

}
