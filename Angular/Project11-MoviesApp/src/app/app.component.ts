import { Component } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private _movieS: MoviesService ){
  	// this._getMovie();
  }

  // _getMovie(){
  // 	return this._movieS.getPopulars().subscribe(
  // 		resp =>
  // 			{
  // 				console.log(resp);
  // 			}
  // 		);
  // }
}
