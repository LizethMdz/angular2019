import { Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { SearchComponent } from './pages/search/search.component';
import { MovieComponent } from './pages/movie/movie.component';
import { TvshowsComponent } from './pages/tvshows/tvshows.component';
import { TvshowComponent } from './pages/tvshow/tvshow.component';

export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'search', component:  SearchComponent},
    { path: 'search/:term', component:  SearchComponent},
    { path: 'search/:term/:page', component:  SearchComponent},
    { path: 'movie/:id', component:  MovieComponent},
    { path: 'movie/:id/:page/:term', component:  MovieComponent},
    { path: 'tvshows', component: TvshowsComponent },
    { path: 'tvshow/:id', component: TvshowComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home'}

];
