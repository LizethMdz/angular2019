import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
/** Modulos http para el uso del servicio */
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';

/** Components */

import { HomeComponent } from './pages/home/home.component';
import { NavbarComponent } from './pages/navbar/navbar.component';
import { MovieComponent } from './pages/movie/movie.component';
import { LoadingComponent } from './pages/loading/loading.component';
import { SearchComponent } from './pages/search/search.component';
import { CardsComponent } from './pages/cards/cards.component';
import { TvshowsComponent } from './pages/tvshows/tvshows.component';


// NOTE Importación de RouterModule
import { RouterModule } from '@angular/router';
// NOTE Importación de rutas
import { ROUTES } from './app.routes';
import { TvshowComponent } from './pages/tvshow/tvshow.component';
/** PIPE IMG */
import { PeliculasImgPipe } from './pipes/peliculas-img.pipe';

import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    MovieComponent,
    LoadingComponent,
    SearchComponent,
    CardsComponent,
    TvshowsComponent,
    TvshowComponent,
    PeliculasImgPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientJsonpModule,
    RouterModule.forRoot( ROUTES, { useHash: true } ),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
