import { Component, OnInit} from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  termino: string;
  regresarAHome =  'home';
  regresarASearch = 'search';



  // Para saber naveegar en la ruta que se esta
  regresarA = '';

  constructor(private _movieS: MoviesService,
              private router: Router,
              private route: ActivatedRoute ) {

                // console.log(this.regresarA);
               }

  ngOnInit() {
    console.log('OnInit');
  }

  _buscar(termino: string) {
    if (termino.length === 0) {
      return;
    }
    console.log(termino);
    this.termino = termino;
    //if ( this.regresarA.length > 3){
      //console.log(this.regresarA);
      //this.router.navigate(['/search', termino, this.regresarA]);
    //}else {
      this.router.navigate(['/search', termino, this.regresarAHome]);

    //}
  }

}
