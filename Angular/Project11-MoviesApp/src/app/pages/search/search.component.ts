import { Component, OnInit} from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { Router } from '@angular/router';


/** Obtener el parametro de la url */
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit{

  loading: boolean;
  __movies: any[] = [];
  // Input()
  busqueda  = 'busqueda';
  // URL PARAMS
  regresarA = '';
  idBuscado: string;

  // Almacena el parametro 'id' de entrada
  terminoBuscado: string;

  constructor(private _moviesS: MoviesService,
              private route: ActivatedRoute,
              private router: Router
            ) {
                  this.route.params.subscribe(
                      (params) => {
                         // Despliegue de los Parametros por URL
                         if(params['page']){
                           this.regresarA = params['page'];
                         }
                         console.log('PARAMS EN SEARCH', params);
                         if ( params ) {
                           this.idBuscado = params['term'];
                           // ID MANDADO POR URL
                           this.buscar(this.idBuscado);
                         }
                      }
                    )
               }

  ngOnInit() {
  }

  buscar(term: string) {
    this.terminoBuscado = term;
    this.loading = true;

    if ( this.terminoBuscado === "" ){
      this.loading = false;
    }else {
        this.loading = true;
        this._moviesS.searchMovie(this.terminoBuscado).subscribe( (data: any) => {
        // console.log(data);
        this.__movies = data;
        this.loading = false;
        console.log(this.__movies);
        this.router.navigate(['/search', term, this.regresarA]);
      });
    }
    console.log(this.idBuscado);
  }

}
