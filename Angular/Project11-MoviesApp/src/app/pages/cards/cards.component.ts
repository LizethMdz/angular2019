import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  /** Depende de lo que venga como parametro de referencia [moviesPop]="name of the array" */
  @Input() elementPop: any[] = [];
  @Input() entrada: string;

  regresarA = '';
  term = '';
  urlImg: string = 'https://image.tmdb.org/t/p/w500/';

  constructor(private router: Router,
              private aR: ActivatedRoute) {
                this.aR.params.subscribe(
                  params => {
                    console.log('PARAMS DE CARDS', params);
                    this.regresarA = params['page'];
                    this.term = params['term'];
                  }
                )
               }

  ngOnInit() {
  }

  _seeDetails(elementPopular: any) {
    let idParam: any;
    // console.log(elementPopular);
    if(elementPopular) {
      console.log(elementPopular.id);
      idParam = elementPopular.id;
    }

    if(this.entrada === 'peliculas') {
      this.router.navigate(['/movie', idParam]);
    } else{
      this.router.navigate(['/tvshow', idParam]);
    }
    if ( this.entrada === 'busqueda') {
      this.router.navigate(['/movie', idParam, this.regresarA, this.term]);
    }

  }



}
