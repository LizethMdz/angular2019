import { Component, OnInit } from '@angular/core';
import { TvshowsService } from '../../services/tvshows.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-tvshow',
  templateUrl: './tvshow.component.html',
  styleUrls: ['./tvshow.component.css']
})
export class TvshowComponent implements OnInit {
  detailShow: any = {};
  loading: boolean;
  urlImg = 'https://image.tmdb.org/t/p/w500/';

  constructor(private _tvS: TvshowsService,
              private _route: ActivatedRoute) 
    {
      this._route.params
      .subscribe( params => {
        this._getDetails(params['id']);
      });

    }

  ngOnInit() {
  }

  _getDetails(id: string) {
    this.loading = true;
    console.log(id);
    this._tvS.getDetailTv(id).subscribe(
      (data: any) => {
        this.detailShow = data;
        this.loading = false;
        console.log(this.detailShow);
      }
    );
  }

}
