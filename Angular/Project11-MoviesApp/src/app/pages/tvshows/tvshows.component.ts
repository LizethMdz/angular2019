import { Component, OnInit } from '@angular/core';
import { TvshowsService } from '../../services/tvshows.service';


@Component({
  selector: 'app-tvshows',
  templateUrl: './tvshows.component.html',
  styleUrls: ['./tvshows.component.css']
})
export class TvshowsComponent implements OnInit {
  _shows: any[] = [];
  loading: boolean;
  tvshows = 'tvshows';

  constructor(private _tvS: TvshowsService) {
    this._listShows();
  }

  ngOnInit() {
  }

  _listShows() {
    this.loading = true;
    this._tvS.getTvDiscover().subscribe(
      (resp: any) => {
        this._shows = resp;
        this.loading = false;
        console.log(this._shows);
      }
    );
  }

}
