import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  loading: boolean;
  _movies: any[] = [];
  _upcoming: any[] = [];
  _trending: any[] = [];


  peliculas  = 'peliculas';
  error: boolean;
  msmerror: string;

   urlImg: string = 'https://image.tmdb.org/t/p/w500/';

    constructor(private _moviesS: MoviesService) {
      this.loading = true;
      this.error = false;

      this._moviesS.getPopulars()
      .subscribe((data: any) => {
        // console.log(data);
        this._movies = data;
        this.loading = false;
      }, (errorService) => {
        this.error = true;
        this.loading = false;
        this.msmerror = errorService.error.status_message;

      });

    this._moviesS.getUpcoming()
    .subscribe( (data: any ) => {
      this._upcoming = data;
      console.log(this._upcoming.slice(0, 6));
    });

    this._moviesS.getMovieTrending()
    .subscribe( (data: any) => {
      this._trending = data;
       console.log(data)
       // Trending
       console.log(this._trending);
    })

  }


  ngOnInit() {
  }

}
