import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

  detailMovie: any = {};
  loading: boolean;
  urlImg = 'https://image.tmdb.org/t/p/w500/';

  // URL PARAMS
  idBuscado: string;
  goback = '';
  word = '';

  constructor(private _moviesS: MoviesService,
              private _route: ActivatedRoute) {
                this._route.params
                .subscribe( params => {

                  if (params['term']) {
                    console.log(params['term']);
                    this.word = params['term'];
                  }
                  // get params from url
                  console.log("PARAMS MOVIE", params);
                  this._getDetails(params['id']);
                  this.goback = params['page'];
                });

               }

  ngOnInit() {
  }

  _getDetails(id: string) {
    this.idBuscado = id;
    this.loading = true;
    // console.log(id);
    this._moviesS.getDetailMovie(id).subscribe(
      (data: any) => {
        this.detailMovie = data;
        this.loading = false;
        console.log(this.detailMovie);
      }
    );
  }

}
