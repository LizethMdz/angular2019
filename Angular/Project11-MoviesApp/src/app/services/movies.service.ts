import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import 'rxjs';

// import { Jsonp } from "@angular/common/http";
// import 'rxjs/Rx'; // Map

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private apikey: string = '918fcbe249f8d9700724ee29ea4dc7d7';
  private urlMovieDB: string = 'https://api.themoviedb.org/3/';

  constructor(private http: HttpClient) {}

  /** https://api.themoviedb.org/3/discover/movie?api_key=<<api_key>>&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1 */

  getPopulars() {

    let url = `${this.urlMovieDB}discover/movie?api_key=${this.apikey}&language=en-US&sort_by=popularity.desc&include_adult=false`;

    return this.http.get(url).pipe(map(
      (resp: any) => resp.results
    ));

  }

  getUpcoming() {
	  
	// https://api.themoviedb.org/3/movie/upcoming?api_key=918fcbe249f8d9700724ee29ea4dc7d7&language=en-US&page=1

    let url = `${this.urlMovieDB}movie/upcoming?api_key=${this.apikey}&language=en-US&sort_by=popularity.desc&include_adult=false`;

    return this.http.get(url).pipe(map(
      (resp: any) => resp.results
    ));

  }

  searchMovie(termino: string) {

    // https://api.themoviedb.org/3/search/movie?api_key={api_key}&query=Jack+Reacher
    let url = `${ this.urlMovieDB }search/movie?api_key=${ this.apikey }&query=${ termino }`;
    console.log(url);

    return this.http.get(url)
      .pipe(
		map((resp: any) => resp.results
	));

  }


  getDetailMovie(id: string) {
    // https://api.themoviedb.org/3/movie/412?api_key=918fcbe249f8d9700724ee29ea4dc7d7&language=en-US
    let url = `${ this.urlMovieDB }movie/${id}?api_key=918fcbe249f8d9700724ee29ea4dc7d7&language=en-US`;

    return this.http.get(url);

  }


  getMovieTrending(){
    // https://api.themoviedb.org/3/trending/movie/day?api_key=918fcbe249f8d9700724ee29ea4dc7d7
    let url = `${ this.urlMovieDB }trending/movie/day?api_key=${ this.apikey }`;
    return this.http.get(url).pipe(
      map( (resp: any) => resp.results ));
  }

}
