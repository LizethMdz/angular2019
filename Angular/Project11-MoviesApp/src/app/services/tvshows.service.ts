import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TvshowsService {

  private apikey = '918fcbe249f8d9700724ee29ea4dc7d7';
  private urlMovieDB = 'https://api.themoviedb.org/3/';
  constructor(private http: HttpClient) { }

  getTvDiscover() {
    // https://api.themoviedb.org/3/discover/tv?api_key=918fcbe249f8d9700724ee29ea4dc7d7&language=en-US&sort_by=popularity.desc&page=1&timezone=America%2FNew_York&include_null_first_air_dates=false
    let url = `${this.urlMovieDB}discover/tv?api_key=${ this.apikey }&language=en-US&page=1`;
    return this.http.get( url).pipe(
      map ( (resp: any) => resp.results ));
  }

  getTvPopular() {
    // https://api.themoviedb.org/3/tv/popular?api_key=918fcbe249f8d9700724ee29ea4dc7d7&language=en-US&page=1
    let url = `${this.urlMovieDB}tv/popular?api_key=${this.apikey}&language=en-US&page=1`;
    return this.http.jsonp( url, 'callback' ).pipe(
      map ( (resp: any ) => resp.results )
    );
  }

  getDetailTv(id: string){
    // https://api.themoviedb.org/3/tv/93533?api_key=918fcbe249f8d9700724ee29ea4dc7d7&language=en-US

    let url = `${ this.urlMovieDB }tv/${id}?api_key=${this.apikey}&language=en-US`;

    return this.http.get(url);
  }

  searchTv(termino: string) {

    // https://api.themoviedb.org/3/search/tv?api_key=918fcbe249f8d9700724ee29ea4dc7d7&query=The+Simpsons
    let url = `${ this.urlMovieDB }search/tv?api_key=${ this.apikey }&query=${ termino }`;
    console.log(url);

    return this.http.get(url)
      .pipe(
        map((resp: any) => resp.results));

  }

}
