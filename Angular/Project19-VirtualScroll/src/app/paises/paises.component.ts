import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.css']
})
export class PaisesComponent implements OnInit {

  paises: any = [];
  constructor(private paisesS: HttpClient) { }

  ngOnInit(): void {

    // COMMENT Obtenencion de datos:
    // URL https://restcountries.eu/rest/v2/lang/es
    this.paisesS.get('https://restcountries.eu/rest/v2/lang/es')
    .subscribe( paises => {
        this.paises = paises;
    });
  }

  drop(event: CdkDragDrop<any> ) {
    console.log('OK', event);
    moveItemInArray(this.paises, event.previousIndex, event.currentIndex);
  }

  // NOTE Mas Informacion
  // Virtual Scroll
  // URL https://material.angular.io/cdk/scrolling/overview

  // Drag and Drop
  // URL https://material.angular.io/cdk/drag-drop/overview


}
