import { TestBed, ComponentFixture } from '@angular/core/testing';
import { IncrementadorComponent } from './incrementador.component';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

describe('Incremendator Component', () => {
  let component: IncrementadorComponent;
  let fixture: ComponentFixture<IncrementadorComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [IncrementadorComponent],
      imports: [FormsModule],
    });

    fixture = TestBed.createComponent(IncrementadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Carga de IncrementadorComponent', () => {
    expect(component).toBeTruthy();
  });
  it('Debe cargar la leyenda', () => {
    component.leyenda = 'Progreso';
    fixture.detectChanges(); //disparar la deteccion de cambios
    //Hacer referencia al elemento de la leyenda pero en el HTML
    const ele: HTMLElement = fixture.debugElement.query(By.css('h3'))
      .nativeElement;
    expect(ele.innerHTML).toContain('Progreso');
  });
  //   it('Debe de mostrar el valor de progreso en el input 1', async () => {
  //     component.cambiarValor(5);
  //     fixture.detectChanges();

  //     //Con esta funcionaria, pero no fue así
  //     fixture.whenStable().then(() => {
  //       const input = fixture.debugElement.query(By.css('input'));
  //       const element: HTMLInputElement = input.nativeElement;
  //       expect(element.valueAsNumber).toBe(55);
  //     });
  //   });
  it('Debe de mostrar el valor de progreso en el input 2', (done) => {
    component.cambiarValor(5);
    fixture.detectChanges();

    //Con esta funcionaria, pero no fue así
    fixture.whenStable().then(() => {
      const input = fixture.debugElement.query(By.css('input'));
      const element: HTMLInputElement = input.nativeElement;
      //element.dispatchEvent(new Event('input'));
      expect(element.valueAsNumber).toBe(55);
      done();
    });
  });

  it('Debe de incrementar/Decrementar en 5, con un click', () => {
    const botones = fixture.debugElement.queryAll(By.css('.btn-primary'));
    botones[0].triggerEventHandler('click', null);
    expect(component.progreso).toBe(45);
    botones[1].triggerEventHandler('click', null);
    expect(component.progreso).toBe(50);
  });

  it('Debe de mostrar el valor de progreso al aumentar o disminuir en 5 si se presionó en el click', () => {
    const botones = fixture.debugElement.queryAll(By.css('.btn-primary'));
    botones[0].triggerEventHandler('click', null);
    fixture.detectChanges(); //disparar la deteccion de cambios
    //Hacer referencia a los elementos en h3
    const ele: HTMLElement = fixture.debugElement.query(By.css('h3'))
      .nativeElement;

    expect(ele.innerHTML).toContain('45');
  });
});
