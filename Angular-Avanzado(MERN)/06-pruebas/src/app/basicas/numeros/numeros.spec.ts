import { incrementar } from './numeros';

describe('Pruebas de numeros', () => {
  it('Debe de retornar 100, si el numero ingresado es mayor a 100', () => {
    const num = incrementar(300);
    expect(num).toBe(100);
  });
  it('Debe de retornar el número mas 1, si el numero ingresado no es mayor a 100', () => {
    const num = incrementar(50);
    expect(num).toBe(51);
  });
});
