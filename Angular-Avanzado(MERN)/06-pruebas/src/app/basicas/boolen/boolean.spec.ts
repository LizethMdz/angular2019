import { usuarioIngresado } from './boolean';

describe('Pruebas de Booleanos', () => {
  it('Debe de regresar un true', () => {
    const res = usuarioIngresado();

    //expect(res).toBe(true);
    expect(res).toBeTruthy(true);
  });
});
