import { obtenerAnimes } from './arreglos';

describe('Prueba de arreglos', () => {
  it('Debe de regresar 5 animes', () => {
    const arr = obtenerAnimes();

    expect(arr.length).toBeGreaterThanOrEqual(3);
  });

  it('Debe existir Black Clover y Memories', () => {
    const arr = obtenerAnimes();

    expect(arr).toContain('Black Clover');
    expect(arr).toContain('Memories');
  });
});
