import { Jugador2 } from './jugador2';
describe('Jugador 2 Emit', () => {
  let jugador: Jugador2;

  beforeEach(() => (jugador = new Jugador2()));

  it('Debe emitir un evento cuando recibe daño', () => {
    let nHP = 0;
    jugador.hpCambia.subscribe((hp) => {
      nHP = hp;
    });

    jugador.recibirDanio(1000);

    expect(nHP).toBe(0);
  });
  it('Debe emitir un evento cuando recibe daño y sobrevivir si es menos de 100', () => {
    let nHP = 0;
    jugador.hpCambia.subscribe((hp) => {
      nHP = hp;
    });

    jugador.recibirDanio(50);

    expect(nHP).toBe(50);
  });
});
