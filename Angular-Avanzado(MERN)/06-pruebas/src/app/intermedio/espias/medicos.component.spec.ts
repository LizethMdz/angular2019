import { empty, from, throwError } from 'rxjs';
import { MedicosComponent } from './medicos.component';
import { MedicosService } from './medicos.service';

describe('MedicosComponent', () => {
  let componente: MedicosComponent;
  const servicio = new MedicosService(null);

  beforeEach(() => {
    componente = new MedicosComponent(servicio);
  });

  it('Init: Debe de cargar los médicos', () => {
    //Hacer peticiones falsas
    const medicos = ['medico1', 'medico2'];
    spyOn(servicio, 'getMedicos').and.callFake(() => {
      return from(medicos);
    });
    componente.ngOnInit();
    expect(componente.medicos.length).toBeGreaterThan(0);
  });

  it('Debe de llamar al servidor para agregar un médico', () => {
    //Hacer peticiones falsas

    const espia = spyOn(servicio, 'agregarMedico').and.callFake(() => {
      return empty();
    });
    componente.agregarMedico();
    expect(espia).toHaveBeenCalled();
  });
  it('Debe de agregar un nuevo médico al arreglo de médicos', () => {
    //Respuesta de la base de datos
    const medico = { id: 1, nombre: 'juan' };
    spyOn(servicio, 'agregarMedico').and.returnValue(from([medico]));

    componente.agregarMedico();
    expect(componente.medicos.indexOf(medico)).toBeGreaterThanOrEqual(0);
  });
  it('Si falla la adicion, la propiedad mensajeError, debe ser igual al error del  del servicio', () => {
    const miError = 'No se agrego el médico';

    spyOn(servicio, 'agregarMedico').and.returnValue(throwError(miError));

    componente.agregarMedico();

    expect(componente.mensajeError).toBe(miError);
  });
  it('Debe de llamar al servidor para borrar un médico', () => {
    // Simula el alert de confirmacion en true
    spyOn(window, 'confirm').and.returnValue(true);
    const espia = spyOn(servicio, 'borrarMedico').and.returnValue(empty());
    componente.borrarMedico('1');
    expect(espia).toHaveBeenCalledWith('1');
  });
  it('No debe de llamar al servidor para borrar un médico', () => {
    // Simula el alert de confirmacion en false
    spyOn(window, 'confirm').and.returnValue(false);
    const espia = spyOn(servicio, 'borrarMedico').and.returnValue(empty());
    componente.borrarMedico('1');
    expect(espia).not.toHaveBeenCalledWith('1');
  });
});
