import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterMedicoComponent } from './router-medico.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, of, empty, Subject } from 'rxjs';

class FakeRouter {
  navigate(params) {}
}

// No usable en angular 9
class FakeActivatedRoute {
  //params: Observable<any> = empty();
  //Insertar  valores a un observable
  //private subject = new Subject<any>();
  //Agregar un valor al subject
  // pushValor(valor) {
  //   this.subject.next(valor);
  // }

  // get params() {
  //   return this.subject.asObservable();
  // }

  params: Observable<any> = of({});
}

describe('RouterMedicoComponent', () => {
  let component: RouterMedicoComponent;
  let fixture: ComponentFixture<RouterMedicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RouterMedicoComponent],
      providers: [
        { provide: Router, useClass: FakeRouter },
        //Version antigua - Implica la creacion una funcion Fake, pero ya no
        { provide: ActivatedRoute, useClass: FakeActivatedRoute },
        // {
        //   provide: ActivatedRoute,
        //   useValue: {
        //     snapshot: {
        //       paramMap: {
        //         get(): string {
        //           return 'nuevo';
        //         },
        //       },
        //     },
        //   },
        // },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterMedicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  //CODE ERROR Validacion de navegación
  xit('Debe de redireccionar a medico cuando se guarde', () => {
    const router = TestBed.inject(Router);
    const spy = spyOn(router, 'navigate');

    component.guardarMedico();

    expect(spy).toHaveBeenCalledWith(['medico', '123']);
  });

  //CODE ERROR No sirve tiene errores
  xit('Debe de colocar el id = nuevo', () => {
    component = fixture.componentInstance;
    const activatedRouter = TestBed.inject(ActivatedRoute);
    expect(component.id).toBe('nuevo');
  });
});
