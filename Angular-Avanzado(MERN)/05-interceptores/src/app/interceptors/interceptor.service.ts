import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class InterceptorService implements HttpInterceptor {
  constructor() {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const headers = new HttpHeaders({
      'token-usuario': 'dsadsdsdwqesadsad1213',
    });

    const reqClone = req.clone({
      headers,
    });
    return next.handle(reqClone).pipe(catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    console.log('Sucedió un error');
    return throwError('Error personalizado');
  }
}
