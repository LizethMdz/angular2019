import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UsuariosService {
  constructor(private http: HttpClient) {}

  getUsers() {
    let params = new HttpParams().append('page', '1');
    params = params.append('nombre', 'hola');

    // const headers = new HttpHeaders({
    //   'token-usuario': 'dsadsdsdwqesadsad1213',
    // });

    return this.http
      .get('https://reqres.in/api/user/hgf', {
        params,
      })
      .pipe(
        map((resp) => {
          return resp['data'];
        })
        // catchError((err) => {
        //   console.log('Sucedió un error');
        //   return throwError('Error personalizado');
        // })
        //catchError(this.handleError)
      );
  }

  handleError(error: HttpErrorResponse) {
    console.log('Sucedió un error');
    return throwError('Error personalizado');
  }
}
