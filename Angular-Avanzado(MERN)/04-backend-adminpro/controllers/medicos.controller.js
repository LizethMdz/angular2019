const { response } = require('express');

const Medico = require('../models/medico.model');

const getMedicos = async (req, res = response) => {

    const medicos = await Medico.find()
        .populate('usuario', 'nombre img')
        .populate('hospital', 'nombre img')

    res.json({
        ok: true,
        medicos
    });
}

const getMedicoById = async (req, res = response) => {
  // Id del médico a actualizar
  const id = req.params.id;
  try {

    const medico = await Medico.findById(id)
    .populate('usuario', 'nombre img')
    .populate('hospital', 'nombre img')

    res.json({
      ok: true,
      medico
    });
  } catch (error) {
    console.log(error);
    res.json({
      ok: false,
      msg: "Hable con el administrador"
    });
  }

}

const createMedico = async (req, res = response) => {
    // Obtener el id del usuario quien esta dando de alta un
    // médico
    const uid = req.uid;
    const medico = new Medico({ usuario: uid, ...req.body });
    try {

        //Guardar el médico
        const medicoDB = await medico.save();

        res.status(200).json({
            ok: true,
            medico: medicoDB
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Ha ocurrido un fallo'
        });

    }

}

const updateMedico = async (req, res = response) => {
    // Id Usuario
    const uid = req.uid;
    // Id del médico a actualizar
    const id = req.params.id;

    try {

        const medico = await Medico.findById(id);
        // validar que el médico exista
        if (!medico) {
            return res.status(404).json({
                ok: true,
                msg: 'Medico no encontrado por id',
            });
        }
        // Si existe, guardamos los cambios
        const cambiosMedico = {
            ...req.body,
            usuario: uid
        }

        const medicoDB = await Medico.findByIdAndUpdate(id, cambiosMedico, { new: true });

        res.json({
            ok: true,
            medico: medicoDB
        })

    } catch (error) {

        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })
    }



}

const deleteMedico = async (req, res = response) => {

    // Id del médico a actualizar
    const id = req.params.id;

    try {

        const medico = await Medico.findById(id);
        // validar que el médico exista
        if (!medico) {
            return res.status(404).json({
                ok: true,
                msg: 'Médico no encontrado por id',
            });
        }


        await Medico.findByIdAndDelete(id);

        res.json({
            ok: true,
            msg: 'Médico eliminado'
        })

    } catch (error) {

        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })
    }
}


module.exports = {
    getMedicos,
    getMedicoById,
    createMedico,
    updateMedico,
    deleteMedico
}
