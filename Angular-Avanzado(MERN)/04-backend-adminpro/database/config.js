const mongoose = require('mongoose');
//const URL_DB = 'mongodb+srv://mean_user:RCMynujnqr5zblwG@cluster0.kvbns.mongodb.net/hospitaldb';


const dbConnection = async () => {

    try {
        await mongoose.connect(process.env.DB_CONNECTION, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        });

        console.log('DB Online...');

    } catch (e) {
        console.log(e);
        throw new Error('Error al conectar a la BD ver logs...')
    }

}

module.exports = {
    dbConnection
}