/*
    Ruta: /api/login
*/
const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

//Controladores
const { login, googleSignIn, renewToken } = require('../controllers/auth.controller');

const router = Router();

router.post('/', [
    check('email', 'El email es obligatorio').isEmail(),
    check('password', 'El password es obligatorio').not().isEmpty(),
    validarCampos
], login);

router.post('/google', [
    check('token', 'El token es necesario').not().isEmpty(),
    validarCampos
], googleSignIn);

router.get('/renew',
    validarJWT,
    renewToken
)

module.exports = router;