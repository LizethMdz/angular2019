import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  private listTheme = document.querySelector('#theme');

  constructor() {
    const url =
      localStorage.getItem('theme') || `./assets/css/colors/default-dark.css`;
    this.listTheme.setAttribute('href', url);
  }

  changeTheme(theme: string) {
    const url = `./assets/css/colors/${theme}.css`;
    this.listTheme.setAttribute('href', url);
    localStorage.setItem('theme', url);

    this.checkCurrentTheme();
  }

  checkCurrentTheme() {
    const links = document.querySelectorAll('.selector');

    links.forEach((elem) => {
      elem.classList.remove('working');
      const dataTheme = elem.getAttribute('data-theme');
      const dataUrlTheme = `./assets/css/colors/${dataTheme}.css`;
      const currentTheme = this.listTheme.getAttribute('href');
      //console.log(dataUrlTheme, currentTheme);
      if (dataUrlTheme === currentTheme) {
        elem.classList.add('working');
      }
    });
  }
}
