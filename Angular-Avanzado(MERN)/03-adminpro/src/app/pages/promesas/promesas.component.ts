import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promesas',
  templateUrl: './promesas.component.html',
  styles: [],
})
export class PromesasComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    // const promesas = new Promise((resolve, reject) => {
    //   if (false) {
    //     resolve('hola mundo');
    //   } else {
    //     reject('rechazada');
    //   }
    // });
    // promesas
    //   .then((msm) => {
    //     console.log(msm);
    //   })
    //   .catch((e) => console.log('error', e));
    this.getUsuarios().then((u) => console.log(u));
  }
  getUsuarios() {
    const promesa = new Promise((resolve) => {
      fetch('https://reqres.in/api/users?page=2')
        .then((res) => res.json())
        .then((body) => resolve(body.data))
        .catch((err) => {
          console.log(err);
        });
    });

    return promesa;
  }
}
