import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, interval, Subscription } from 'rxjs';
import { retry, take, map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: [],
})
export class RxjsComponent implements OnInit, OnDestroy {
  // Se declaró para unsubscribe de un observable cuando el componente es destruido
  intervaloSubs: Subscription;

  constructor() {}

  ngOnDestroy(): void {
    this.intervaloSubs.unsubscribe();
  }

  ngOnInit(): void {
    // this.retornaObservable()
    //   .pipe(retry(1))
    //   .subscribe(
    //     (valor) => console.log('subs', valor),
    //     (err) => console.warn('error', err),
    //     () => console.info('Obs terminado')
    //  );
    this.intervaloSubs = this.retornaIntervalo().subscribe((valor) =>
      console.log(valor)
    );
    // De esta forma todos los parametros estan dispuestos a ser impresos mediante la consola
    //this.retornaIntervalo().subscribe(console.log);
  }

  retornaIntervalo(): Observable<number> {
    return interval(500).pipe(
      // Manipula la salida retornada por el observable
      map((valor) => {
        return valor + 1;
      }),
      // Filtra informacion segun la salida del observable
      filter((valor) => valor % 2 === 0),
      // Indica cuantos valores tomar del intervalo
      take(10)
    );

    //return interval$;
  }

  retornaObservable(): Observable<number> {
    let i = -1;

    return new Observable<number>((observer) => {
      const intervalo = setInterval(() => {
        i++;
        observer.next(i);

        if (i === 4) {
          clearInterval(intervalo);
          observer.complete();
        }
        if (i === 2) {
          observer.error('Lego al valor de dos');
        }
      }, 1000);
    });

    //return obs$;
  }
}
