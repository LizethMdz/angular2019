import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label, Color } from 'ng2-charts';

@Component({
  selector: 'app-grafica1',
  templateUrl: './grafica1.component.html',
  styles: [],
})
export class Grafica1Component implements OnInit {
  // Doughnut
  public doughnutChartLabels: Label[] = [
    'Download Sales',
    'In-Store Sales',
    'Mail-Order Sales',
  ];
  public doughnutChartData = [[300, 45, 10]];
  // Doughnut
  public doughnutChartLabels2: Label[] = [
    'Download Products',
    'In-Store Products',
    'Mail-Order Products',
  ];
  public doughnutChartData2 = [[380, 10, 70]];
  // Doughnut
  public doughnutChartLabels3: Label[] = [
    'Download Shopping',
    'In-Store Shopping',
    'Mail-Order Shopping',
  ];
  public doughnutChartData3 = [[30, 30, 30]];
  // Doughnut
  // public doughnutChartLabels4: Label[] = [
  //   'Download Offers',
  //   'In-Store Offers',
  //   'Mail-Order Offers',
  // ];
  // public doughnutChartData4 = [[80, 90, 100]];

  ngOnInit(): void {}
}
