import { Component, Input } from '@angular/core';
import { MultiDataSet, Label, Color } from 'ng2-charts';

@Component({
  selector: 'app-dona',
  templateUrl: './dona.component.html',
})
export class DonaComponent {
  @Input() title: string;
  // Doughnut
  @Input('labels') public doughnutChartLabels: Label[] = [
    'Label1',
    'Label2',
    'Label3',
  ];
  @Input('data') public doughnutChartData = [[10, 30, 50]];
  // public doughnutChartType: ChartType = 'doughnut';
  public colors: Color[] = [
    { backgroundColor: ['#9E120E', '#FF5800', '#FFB414'] },
  ];
}
