function saludar(nombre) {
    console.log(" Hola " + nombre.toUpperCase());
}
var wolverine = {
    nombre: " Logan "
};
saludar(wolverine.nombre);
/* NOTE CODIGO JAVA SCRIPT - DIFERENCIA */
/* ENTRE VAR Y ANCHOR LET */
let mensaje = "Hola";
if (true) {
    let mensaje = "adios";
}
console.log(mensaje);
/* NOTE  USO DE CONSTANTES

/* DEBEN SER INICIALIZADAS DESDE EL PRICIPIO
const OPCIONES = "todas";
SI SE DECLARA EN OTRO SCOPE PASA LO MISMO QUE ANCHOR  */
if (true) {
    const OPCIONES = "ninguna";
}
/* ANCHOR EJEMPLO 1:
/* AL DECLARAR SE ESPECIFICA EL TIPO DE DATO */
let nombre = "Peter";
let numero = 123;
let booleano = true;
let hoy = new Date();
let cualquiera;
cualquiera = nombre;
cualquiera = numero;
cualquiera = booleano;
cualquiera = hoy;
let spiderman = {
    nombre: "Peter",
    edad: 20
};
/* NOTE SI SE CREA OTRO OBJETO PERO CON MAS
 CARACTERISTICAS, NOTIFICARA UN ERROR */
/* NOTE Manejo de STRINGS PARA LA CONCATENACION
 CREA UN TEXTO MULTILINEAS SIN AGREGAR SALTOS DE LINEA, ETC. */
let nombre_ = "Peter";
let apellido = "Parker";
let edad = 22;
let cadena = `Hola, ${nombre} ${apellido} ${edad}`;
/*NOTE DENTRO DE ` ${ SE PUEDEN DECLARAR CODIGO JAVASCRIPT } ` */
console.log(cadena);
/* NOTE EJEMPLO 2:

/*PARAMETRO OBLIGATORIO */
function activar(quien) {
    let mensaje;
    mensaje = `${quien} activo la alarma`;
    console.log(mensaje);
}
activar("Juan");
/*PARAMETRO NO OBLIGATORIO, PERO LO DEMAS SI LO ES */
function activar_(quien, objeto, momento) {
    let mensaje;
    mensaje = `${quien} activo la ${objeto}`;
    if (momento) {
        mensaje = `${quien} activo la ${objeto} en la ${momento}`;
    }
    else {
        mensaje = `${quien} activo la ${objeto}`;
    }
    console.log(mensaje);
}
activar_("Juan", "Bomba");
/* ANCHOR EJEMPLO 3:

/*FUNCIONES DE FLECHA */
/* NOTE OPCION 1 */
let miFuncion = function (a) {
    return a;
};
/* NOTE OPCION 2*/
let miFuncionF = a => a;
/* NOTE CADA UNA IMPRIME LO MISMO DADO QUE
 LA FUNCION ES LA MISMA*/
console.log(miFuncion("Normal"));
console.log(miFuncionF("De flecha"));
/*ANCHOR OPCION 1*/
let suma = function (a, b) {
    return a + b;
};
/*ANCHOR OPCION 2*/
let sumaF = (a, b) => a + b;
console.log(suma(4, 5));
console.log(sumaF(7, 9));
/* STUB OPCION 1*/
let toUpperCase = function (nombre) {
    nombre = nombre.toUpperCase();
    return nombre;
};
/* STUB OPCION 2*/
let toUpperCaseF = (nombre) => {
    nombre = nombre.toUpperCase();
    return nombre;
};
/* NOTE UN EJEMPLO DE UNA FUNCION DENTRO DE UN OBJETO*/
let hulk = {
    nombreA: "Hulk",
    smash() {
        setTimeout(() => console.log(this.nombreA + " smash "), 1500);
    }
};
hulk.smash();
/* ANCHOR DESTRUCTURACION DE OBJETOS*/
let avenger = {
    nombre_Ave: "Tom",
    clave: "Superman",
    poder: "Strength"
};
/*NOTE  DESTRUCTURACION CON UNA LINEA
/* NO IMPORTA EL ORDEN
/* DESPUES DE clave: -> es un alias, más no el tipo de dato */
let { nombre_Ave, clave, poder } = avenger;
/* NOTE Destructuración de objeto con varias lineas
/*let nombre_Ave = avenger.nombre;
/*let clave = avenger.clave;
/*let poder = avenger.poder;*/
console.log(nombre_Ave, clave, poder);
/* ANCHOR DESTRUCTURACION DE ARREGLOS*/
let avengers = ["Thor", "CP", "Ironman"];
/*NO LE IMPORTA QUE NO SE LLAMEN IGUAL,
/*SABE QUE EL VALOR SERA ASIGNADO SEGUN LA POSICION
/*EN LAS VARIABLES*/
let [av1, av2, av3] = avengers;
console.log(av1, av2, av3);
/*SI SOLE QUEREMOS UN VALOR DE LA POSICION
/*LOS ESPACIOS SE LLENAN CON COMAS*/
let [, , ironman] = avengers;
console.log(ironman);
/* ANCHOR  PROMESA
/* PARA PROCESOS ASINCRONOS, DETERMINAN QUE PASA CUANDO
/* UNA FUNCION SALE BIEN O SALE MAL*/
//  let prom1 = new Promise( function (resolve, reject){
//      setTimeout(()=> {
//          console.log("Termino la promesa");
//         /*TERMINA BIEN*/
//          resolve();
//         /*TERMINA MAL
//         /*reject();*/
//      }, 1500)
//  })
/*LAMAR PROMESA, ADEMÁS DE DECLARAR QUE PASA SEGUN LO ESTABLECIDO*/
// prom1.then( function(){
//     console.log("Ejecución porque se termino bien");
// } , function(){
//     console.error("Termino mal");
// })
/**ANCHOR EJEMPLO MAS ILUSTRATIVO DE PROMESAS */
(() => {
    const retirarDinero = (montoRetirar) => {
        let dineroActual = 1000;
        return new Promise((resolve, reject) => {
            if (montoRetirar > dineroActual) {
                reject('No hay sufientes fondos');
            }
            else {
                dineroActual -= montoRetirar;
                resolve(dineroActual);
            }
        });
    };
    retirarDinero(1509).then(montoActual => console.log(`Me queda ${montoActual}`))
        .catch(err => console.warn(err));
})();
/* ANCHOR INTERFASES DE TYPESCRIPT */
function enviarMision(xmen) {
    console.log("Enviando a: " + xmen.nombre);
}
let bluedemon = {
    nombre: "Lucas",
    poder: "X-Ray"
};
/*NOTE DE ESTA MANERA SI SE IMPRIME EL VALOR DEL NOMBRE
// PERO, SI SE CAMBIA EL ATRIBUTO NOMBRE A NOMBREXMEN, MARCARIA
// COMO UNDIFINED */
// enviarMision(bluedemon);
// /**NOTE PARA ESTE CASO ES MEJOR DECIRLE ESPECIFICAMENTE 
// QUE RECIBIRAR UN OBJETO CON CIERTOS TIPOS DE ATRIBUTOS */
function enviarMision2(xmen) {
    console.log("Enviando al espacio : " + xmen.nombre);
}
let thor = {
    nombre: "Thor",
    poder: "Lightning"
};
enviarMision2(thor);
/**NOTE se le declara que es un tipo XMEN haciendo referencia
a la interfaz */
function enviarMision3(xmen) {
    console.log("Enviando al cuartel : " + xmen.nombre);
}
/**NOTE AHORA EL OBJETO COMPARTE EL ESQUELETO DE UN XMEN
RESPETANDO LAS PROPIEDADES DECLARADAS EN LA INTERFAZ */
let CP = {
    nombre: "Captain America",
    poder: "Fighting"
};
enviarMision3(CP);
/**ANCHOR CLASES POO, CONSTRUCTORES*/
class Vengadores {
    constructor(nombre, equipo, nombreReal) {
        this.nombre = nombre;
        this.equipo = equipo;
        this.nombreReal = nombreReal;
    }
}
/**NOTE CONSTRUCTOR PERMITE INICIALIZAR VARIABLES  */
let antman = new Vengadores("Ironman", "Iron", "Tony");
console.log(antman);
/**ANCHOR  MODULOS - PERMITEN SEGMENTAR NUESTRO CODIGO
 EN VARIOS ARCHIVOS:
 SE UTILIZA UN MODULO "SYSTEMJS" PARA LA DEMOSTRACION DE
 LOS MODULOS

 PARA SUS IMPORTACION SE EMPLEA

 import { } from "./classes/xmen.class" (EN EL ARCHIVO A IMPORTAR)

 SE ESPECIFICA QUE ELEMENTO U OBJETO SE EMPLEA

 import { Xmen } from "./classes/xmen.class" (EN EL ARCHIVO A IMPORTAR)

 import { Villanos } from "./classes/villanos.class" (EN EL ARCHIVO A IMPORTAR)

PARA UTILIZARLO SERIA;

let wolverine = new Xmen("Logan", "Wolverine");

NOTE PUEDO HACER UNA IMPORTACION DE MI CLASE, PERO
DEBO AGREGAR LA PALABRA EXPORT, EJEMPLO:

export class Xmen {
    contenido.....
}

NOTE SE PUEDE REDUCIR EL NUMERO DE LINEAS POR IMPORTACION,
YA QUE SE PUEDE REDUCIR EN UN SOLO ARCHIVO QUE CONTENGA
TODAS LAS QUE OCUPAS. QUEDANDO DE LA SIGUENTE MANERA:

import {Xmen. Villanos} from "./classes/index"
import { element } from 'protractor';

*/
let elemento = [1, 1, 6, 7, 9, 3, 2, 6];
for (const iterator of elemento) {
    console.log(iterator);
}
for (const key in elemento) {
    if (elemento.hasOwnProperty(key)) {
        const element = elemento[key];
    }
}
for (let i of elemento) {
    console.log(i);
}
