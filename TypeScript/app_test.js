// Uso de Let y Const
let nombre__ = "Ricardo Tapia";
let edad_ = 23;
const PERSONAJE = {
    nombre: nombre__,
    edad: edad_
};
const t = "hola";
// Cree una interfaz que sirva para validar el siguiente objeto
let batman = {
    nombre__: "Bruno Díaz",
    artesMarciales: ["Karate", "Aikido", "Wing Chun", "Jiu-Jitsu"]
};
function habilidades(batman) {
    console.log(batman.nombre__);
    for (var i = 0; i < batman.artesMarciales.length; i++) {
        console.log("Habilidad [" + i + "] : " + batman.artesMarciales[i]);
    }
}
habilidades(batman);
// Convertir esta funcion a una funcion de flecha
function resultadoDoble(a, b) {
    return (a + b) * 2;
}
let rDoble_F = (a, b) => (a + b) * 2;
// Función con parametros obligatorios, opcionales y por defecto
// donde NOMBRE = obligatorio
//       PODER  = opcional
//       ARMA   = por defecto = "arco"
function getAvenger(nombre__, poder, arma = "arma") {
    var mensaje;
    if (poder) {
        mensaje = nombre__ + " tiene el poder de: " + poder + " y un arma: " + arma;
    }
    else {
        mensaje = nombre__ + " tiene un " + poder;
    }
}
;
function getAvger(nombre__, arma = "arco", poder) {
    var mensaje;
    if (poder) {
        mensaje = nombre__ + " tiene el poder de: " + poder + " y un arma: " + arma;
    }
    else {
        mensaje = nombre__ + " tiene un " + poder;
    }
    console.log(mensaje);
}
getAvger("Robin", "arco", "velocidad");
// Cree una clase que permita manejar la siguiente estructura
// La clase se debe de llamar rectangulo,
// debe de tener dos propiedades:
//   * base
//   * altura
// También un método que calcule el área  =  base * altura,
// ese método debe de retornar un numero.
class rectangulo {
    constructor() { }
    area(base, altura) {
        return base * altura;
    }
}
let figura = new rectangulo();
console.log(figura.area(5, 6));
/**
 * ANCHOR DESTRUCTURACIÓN DE OBJETOS
 */
let avengerss = ["Thor", "CP", "Ironman"];
/*NO LE IMPORTA QUE NO SE LLAMEN IGUAL,
/*SABE QUE EL VALOR SERA ASIGNADO SEGUN LA POSICION
/*EN LAS VARIABLES*/
let [av1s, av2s, av3s] = avengerss;
console.log(av1s, av2s, av3s);
/*SI SOLE QUEREMOS UN VALOR DE LA POSICION
/*LOS ESPACIOS SE LLENAN CON COMAS*/
let [, , ironmans] = avengerss;
console.log(ironmans);
/**
 * ANCHOR EJEMPLO DE UNA TUPLA
 */
var employee = [1, "Steve"];
var person = [1, "Steve", true];
var variables = [1, 2];
class Student {
    constructor(firstName, lastName, subject, age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.first_name = firstName;
        this.last_name = lastName;
        this.subject = subject;
        this.age = age;
    }
}
let student1 = new Student("Lucas", "Till", "Math", 23);
let person1 = {
    firstName: "Tom",
    lastName: "Welling"
};
function greeter(person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}
console.log(greeter(student1));
/**NOTE Type Assertions
 * Compiler knows what is doing
 * A type assertion is like a type cast
 * in other languages, but performs no special
 * checking or restructuring of data
 *
*/
let Frase = "This is an string";
let tamFrase = Frase.length;
let tamaFrase = Frase.length;
/**
 * NOTE ENUM (TYPE OF DATA)
 */
var Colors;
(function (Colors) {
    Colors[Colors["red"] = 4] = "red";
    Colors[Colors["black"] = 9] = "black";
    Colors[Colors["white"] = 8] = "white";
})(Colors || (Colors = {}));
;
console.log(Colors[4]);
/**
 * NOTE: ITERATORS
 */
let set_numbers = [1, 1, 6, 7, 9, 3, 2, 6];
for (let iterator of set_numbers) {
    console.log(iterator);
}
for (let key in set_numbers) {
    if (set_numbers.hasOwnProperty(key)) {
        const element = set_numbers[key];
        console.log(set_numbers[key]);
    }
}
for (let i in set_numbers) {
    console.log(i);
}
/**
 * NOTE OBJETOS
 */
let superhuman = {
    power: "speed",
    color: "red",
    name: "Lucas",
    getName() {
        return this.name;
    }
};
