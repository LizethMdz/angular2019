// Uso de Let y Const
let nombre__:string = "Ricardo Tapia";
let edad_:number = 23;


const PERSONAJE = {
  nombre: nombre__,
  edad: edad_

};
 

const t = "hola";
 
// Cree una interfaz que sirva para validar el siguiente objeto

interface Batman {
  nombre__ : string,
  artesMarciales : string [];
}

// Nueva implementacion
let batman_:Batman = {
  nombre__: "Bruno Díaz",
  artesMarciales: ["Karate","Aikido","Wing Chun","Jiu-Jitsu"]
}

//Ejemplo:

let batman:Heroe = {
  nombre__: "Bruno Díaz",
  artesMarciales: ["Karate","Aikido","Wing Chun","Jiu-Jitsu"]
}


    interface Heroe {
        nombre__ : string,
        artesMarciales : string [];
    }


    function habilidades (batman:Heroe){
        console.log(batman.nombre__);
        
        for(var i=0; i < batman.artesMarciales.length; i++ ){
            console.log( "Habilidad [" + i + "] : " + batman.artesMarciales[i]);
        }
    
    }

    habilidades(batman);
 
 
// Convertir esta funcion a una funcion de flecha
function resultadoDoble( a, b ){
  return (a + b) * 2
}

let rDoble_F = (a:number,b: number) => (a + b) * 2;

//Nueva implementacion

const resultadoD = (a:number, b:number): number => (a+b) * 2; 




 
 
// Función con parametros obligatorios, opcionales y por defecto
// donde NOMBRE = obligatorio
//       PODER  = opcional
//       ARMA   = por defecto = "arco"
function getAvenger( nombre__:string , poder?:string, arma:string = "arma" ){
  var mensaje;
  if( poder ){
     mensaje = nombre__ + " tiene el poder de: " + poder + " y un arma: " + arma;
  }else{
     mensaje = nombre__ + " tiene un " + poder
  }
};

function getAvger(nombre__:string, arma:string = "arco", poder?:string){
  let  mensaje;
  if( poder ){
    mensaje = nombre__ + " tiene el poder de: " + poder + " y un arma: " + arma ;
  }else{
    mensaje = nombre__ + " tiene un " + poder
  }

  console.log(mensaje);
}

getAvger("Robin","arco" ,"velocidad");

 
 
// Cree una clase que permita manejar la siguiente estructura
// La clase se debe de llamar rectangulo,
// debe de tener dos propiedades:
//   * base
//   * altura
// También un método que calcule el área  =  base * altura,
// ese método debe de retornar un numero.



class rectangulo {
    base:number;
    altura:number;

    constructor (){}

    area(base, altura):number {
        return base * altura;
    }

    //Nueva implementacion
    area2 = ():number => {
      return this.base * this.altura; 
    }
}

let figura = new rectangulo();

console.log(figura.area(5,6));

/**======================================================================= */
/**===================FIN DEL EXAMEN ===================================== */
/**======================================================================= */

/**
 * ANCHOR DESTRUCTURACIÓN DE OBJETOS
 */

let avengerss:string[]= ["Thor", "CP", "Ironman"];

/*NO LE IMPORTA QUE NO SE LLAMEN IGUAL, 
/*SABE QUE EL VALOR SERA ASIGNADO SEGUN LA POSICION 
/*EN LAS VARIABLES*/
let [av1s, av2s, av3s] = avengerss;
console.log(av1s, av2s, av3s);

/*SI SOLE QUEREMOS UN VALOR DE LA POSICION
/*LOS ESPACIOS SE LLENAN CON COMAS*/

let [, , ironmans] = avengerss;
console.log(ironmans);

/**
 * ANCHOR EJEMPLO DE UNA TUPLA
 */

var employee: [number, string] = [1, "Steve"];
var person: [number, string, boolean] = [1, "Steve", true];

var variables: [number, number] = [1, 2] 


/**
 * NOTE Interfases accedidas desde sus variables publicas
 */

interface Person {
  firstName: string;
  lastName: string;
}

class Student {
  first_name:string
  last_name : string
  subject : string
  age : number

  constructor (public firstName : string, public lastName : string, subject : string, age : number){
    this.first_name = firstName;
    this.last_name = lastName;
    this.subject = subject;
    this.age = age;
  }

  
}


let student1 = new Student("Lucas", "Till", "Math", 23);

let person1:Person =  {
  firstName: "Tom",
  lastName:"Welling"
}

function greeter(person:Person){
  return "Hello, " + person.firstName + " " + person.lastName;
}

console.log(greeter(student1));

/**NOTE Type Assertions 
 * Compiler knows what is doing
 * A type assertion is like a type cast 
 * in other languages, but performs no special 
 * checking or restructuring of data
 * 
*/

let Frase : string = "This is an string";

let tamFrase : number = (<string>Frase).length;

let tamaFrase : number = (Frase as string).length;

/**
 * NOTE ENUM (TYPE OF DATA)
 */


enum Colors {red = 4, black=9, white = 8};

console.log(Colors[4]);

/**
 * NOTE: ITERATORS
 */

let set_numbers : Array<number>  = [1,1,6,7,9,3,2,6];

for (let iterator of set_numbers) {
    console.log(iterator);
}

for (let key in set_numbers) {
    if (set_numbers.hasOwnProperty(key)) {
        const element = set_numbers[key];
        console.log(set_numbers[key]);
        
    }
}

for (let i in set_numbers){
    console.log(i);
}

/**
 * NOTE OBJETOS
 */

let superhuman :Hero = {
   power : "speed",
   color : "red",
  name : "Lucas",
   getName(){
     return this.name;
   }
}

type Hero = {
  name:string, 
  power:string, 
  color:string, 
  getName:() =>string
}

/** =============================================== */
/**=============NUEVAS ACTUALIZACIONES============= */
/** =============================================== */

/**ANCHOR Tipado del retorno de una funcion  */
(() => {
  const Sumar = (a:number, b:number): number => {
    return a + b;
  }

  const nombre = (): string => 'Hola Lizz';

  const obtenerSaldo = (): Promise<string> => {
    return new Promise((resolve, reject) => {
      resolve('Tienes saldo');
    });

  }

  obtenerSaldo().then( a => console.log(a.toUpperCase));



})();











